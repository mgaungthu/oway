<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>




    <div class="box search-bar">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
            </div><!-- /. tools -->
            <i class="fa fa-search"></i>
            <h3 class="box-title">
                Search
            </h3>
        </div>

        <div class="box-body">



            <?=form_open("admin/search-cv",'  ')?>
            <section class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?=form_label('NRC No')?>
                    <?=form_input("nrc_no",'',"placeholder='Applicant No'  class='form-control'")?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Job Category</label>
                    <select name="job_cate" data-placeholder="Choose one" class="chosen-select" tabindex="2" >
                        <option value=""></option>
                        <?php
                        $this->db->order_by('cat_code','ASC');
                        $jc=$this->db->get('job_category_tbl')->result_array();
                        foreach ($jc as $key => $val) :
                            ?>
                            <option value="<?=$val['cat_code']?>"><?=$val['cat_code']?> - <?=$val['description']?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <?=form_label('Expected Salary')?>
                    <select  name='salary_exp' id='salary_exp' class='form-control'>
                        <option  value="">Select Expected Salary</option>
                        <?php for ($i = 1; $i < 7; $i++) :?>
                            <option value='<?=$i?>'  >  <?=$this->main_model->salary_exp($i)?> </option>
                        <?php endfor;?>

                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <?=form_label('Job Type')?>
                    <select id="job_type" name="job_type" data-placeholder="Choose one" class="chosen-select" tabindex="2" >
                        <option value=""></option>
                        <?php
                        $this->db->order_by('t_id','ASC');
                        $jobtype=$this->db->get('job_type_tbl')->result_array();
                        foreach ($jobtype as $key => $val) :
                            ?>
                            <option value="<?=$val['t_id']?>"><?=$val['job_type']?> </option>
                        <?php endforeach;?>
                    </select>

                </div>
            </div>
                </section>
            <section class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?=form_label('Experience ')?>
                    <?php
                    $options = array(
                        ''       => 'Choose One',
                        '1'      => 'Non Experience ( 0 - 1 Year) ',
                        '2'      => 'Experience (2 - 3 Year)',
                        '3'      => 'Manager (3 - 5 Year)',
                        '4'      => 'Executive (5 - 8 Year) ',
                        '5'      => 'Senior Executive ',

                    );
                    ?>
                    <?=form_dropdown('exp_skill', $options,'','class="form-control"  ')?>
                </div>

            </div>


            <div class="col-md-3">
                <div class="form-group">
                    <label>Experience Job Title</label>

                    <select id="job_title" name="job_title" data-placeholder="Choose one" class="chosen-select" tabindex="2" >
                        <option value=""></option>
                        <?php
                        $expjt=$this->db->query("SELECT DISTINCT job_title from working_exp_tbl ORDER BY job_title DESC")->result_array();
                        foreach ($expjt as $key => $jt) :
                            ?>
                            <option value="<?=$jt['job_title']?>"><?=$jt['job_title']?> </option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>


                <div class="col-md-3">
                <div class="form-group">
                    <label>Experience Job Industry</label>
                    <select name="exp_industry" data-placeholder="Choose one" class="chosen-select" tabindex="2" >
                        <option value=""></option>
                        <?php
                        $this->db->order_by('description','DESC');
                        $ind=$this->db->get('industry_tbl')->result_array();
                        foreach ($ind as $key => $val) :
                            ?>
                            <option value="<?=$val['t_id']?>"><?=$val['description']?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                </div>

            </section>
            <section class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?=form_label('Apply Position')?>
                    <select id="apply_post" name="apply_post" data-placeholder="Choose one" class="chosen-select" tabindex="2" >
                        <option value=""></option>
                        <?php
                        $app=$this->db->query("SELECT DISTINCT apply_post from app_pos_tbl ORDER BY apply_post DESC")->result_array();
                        foreach ($app as $key => $val) :
                            ?>
                            <option value="<?=$val['apply_post']?>"><?=$val['apply_post']?> </option>
                        <?php endforeach;?>
                    </select>

                </div>
            </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Apply Job Industry</label>
                        <select name="app_industry" data-placeholder="Choose one" class="chosen-select" tabindex="2" >
                            <option value=""></option>
                            <?php
                            $this->db->order_by('description','DESC');
                            $ind=$this->db->get('industry_tbl')->result_array();
                            foreach ($ind as $key => $val) :
                                ?>
                                <option value="<?=$val['t_id']?>"><?=$val['description']?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </section>



    </div>
        <div class="box-footer text-right smallpadding">
           <a href="admin" class="btn btn-danger">Reset</a> <?=form_submit("Search","Search","class='btn btn-primary'     ")?>
        </div>
    </div>




<?=form_close()?>

<div class="box">
<div  class="table-responsive smallsidepadding smallpadding">
       <table id="cv_table" class="table table-bordered table-hover" >
        <thead>
          <tr>
            <th>
              No
            </th>
            <th>
              Application-Id
            </th>
              <th>
                  Full Name
              </th>

              <th>
                  Email
              </th>
              <th>
                  Phone Number
              </th>
            <th>
              Job Category
            </th>
              <th>
                  Salary Expectations
              </th>
              <th>
                  Experience Job Title
              </th>
            <th>
              Who Created
            </th>
            <th>
              Created Date
            </th>
              <th>
                  CV Strength
              </th>
            <th>
              Action
            </th>
            
          </tr>
        </thead>
        <tbody>
        <?php 
            $id=1;
            foreach ($query as $key => $row) :
        ?>
        <tr>
            <td>
              <?=$id++?>
            </td>
            <td>
              <?=$row['cv_id']?>
            </td>
            <td>
                <?=$row['first_name']?> <?=$row['last_name']?>
            </td>
           <td>
               <?=$row['email']?>
           </td>
            <td>
                <?=$row['c_number']?>
            </td>
            <td>
              <?=$row['description']?>
            </td>
            <td class="cur">

                <?=$row['salary_exp']?>

            </td>
            <td>
                <?=$row['job_title']?>
            </td>
            <td>
              <?=$row['creator_name']?>
            </td>
            <td>
              <?=date_time($row['creat_date'])?>
            </td>
            <td>
                <div class="progress-group">
                    <span class="progress-text text-red">weak</span>
                    <span class="progress-number text-blue">strong</span>
                    <div class="progress sm">
                        <div class="progress-bar progress-bar-aqua" style="width:<?=$this->main_model->cv_strenght($row['cv_id'])?>%"></div>
                    </div>
                </div>
            </td>
            <td>
                <a data-toggle="tooltip" title="More Info" href="admin/edit-cv/<?=urlencode(encryptor('encrypt',$row['t_id']))?>">
                    <i class="fa fa-file-text-o icon"></i>
                </a>
                <a data-toggle="tooltip" title="Print" href="admin/print-setting/<?=urlencode(encryptor('encrypt',$row['t_id']))?>">
                    <i class="fa fa-print icon"></i>
                    </a>
                <a data-toggle="tooltip" title="Interview" href="#">
                    <i class="fa fa-users icon"></i>
                </a>
            </td>
           
        </tr>
        <?php endforeach;?>
        </tbody>       
      </table>
  </div>
    <div class="box-footer clearfix">
        <a href="admin/creat-cv" class="btn btn-sm btn-info btn-flat pull-left">Creat New Cv</a>

    </div>
</div>
