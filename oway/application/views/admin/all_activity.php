<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


<div class="box">
<div  class="table-responsive smallsidepadding smallpadding">
       <table id="cv_table" class="table table-bordered table-hover" >
        <thead>
          <tr>
            <th>
              No
            </th>
            <th>
                Full Name
            </th>
              <th>
                  Action
              </th>

              <th>
                  Date
              </th>

            
          </tr>
        </thead>
        <tbody>
        <?php 
            $id=1;
            foreach ($query as $key => $row) :
        ?>
        <tr>
            <td>
              <?=$id++?>
            </td>
            <td>
                <?=$row['real_name']?>
            </td>
           <td>
               <p><?=$row['description']?> <?=$row['About']?>  </p>
           </td>
            <td>
                <?=date_time($row['date_time'])?>
            </td>

        </tr>
        <?php endforeach;?>
        </tbody>       
      </table>
  </div>

</div>
