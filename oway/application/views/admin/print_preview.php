<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$error= $this->uri->segment(4);
if(!empty($error)):
    if($error=='complete'):
        ?>

        <div class=" email-error text-center smallpadding">
            <b>Email is already has been sent!</b>
        </div>
        <?php
    elseif($error=='failed'):
        ?>
        <div class=" email-error text-center smallpadding">
            <b>Email is Sending Error!</b>
        </div>
        <?php
    endif;
endif;
?>
<div class="box">
    <div class="box-body">
        <div class="col-md-3 smalltopmargin">
            <?=form_open("admin/print-cv/".$this->uri->segment(3),'')?>

            <label>Contact Infomation</label>
            <div class="form-group smalltoppadding">
                <div class="radio-inline">
                    <label>
                        <input name="c_info" value="1"  type="radio"> Yes
                    </label>
                </div>

                <div class="radio-inline">
                    <label>
                        <input name="c_info" value="2"  type="radio"> No
                    </label>
                </div>
            </div>


            <label>Email Attachment</label>
            <div class="form-group smalltoppadding">
                <div class="radio-inline">
                    <label>
                        <input id="email_sit" name="email_sit" value="1"  type="radio"> Yes
                    </label>
                </div>

                <div class="radio-inline">
                    <label>
                        <input id="email_sit" name="email_sit" value="2"  type="radio"> No
                    </label>
                </div>
            </div>


            <label>Logo</label>
            <div class="form-group smalltoppadding">
                <div class="radio-inline">
                    <label>
                        <input id="logo" name="target_location" value="1" <?php if($target_location==1){echo 'checked';} ?>  type="radio"> Yes
                    </label>
                </div>

                <div class="radio-inline">
                    <label>
                        <input id="logo" name="target_location" value="2" <?php if($target_location==2){echo 'checked';} ?> type="radio"> No
                    </label>
                </div>
            </div>
            <!---->
            <!--            <label>File Type</label>-->
            <!---->
            <!--            <div class="form-group smalltoppadding">-->
            <!--                <div class="radio-inline">-->
            <!--                    <label>-->
            <!--                        <input name="email_sit" value="1"  type="radio"> Docs-->
            <!--                    </label>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="radio-inline">-->
            <!--                    <label>-->
            <!--                        <input name="email_sit" value="2"  type="radio"> PDF-->
            <!--                    </label>-->
            <!--                </div>-->
            <!--            </div>-->


            <div class="form-group">
                <?=form_label('Apply Position ')?>
                <select id="apply_post" name="apply_post"  class="form-control" required="" >
                    <option value="">Choose One</option>
                    <?php
                    $appos=$this->db->query("SELECT DISTINCT apply_post from app_pos_tbl WHERE cv_id = '$cv_id' ORDER BY apply_post DESC")->result_array();
                    foreach ($appos as $key => $val) :
                        ?>
                        <option value="<?=$val['apply_post']?>"><?=$val['apply_post']?> </option>
                    <?php endforeach;?>
                </select>
            </div>


            <div>
                <style>
                    .print-pp p {
                        font-size: 15px;
                    }

                    h3 {
                        font-size: 16px;
                        color: #7dc24b;
                    }

                    h5 {
                        font-weight: bold;
                        font-size: 14px;
                    }


                    div.line {
                        border-bottom: 2px solid #7dc24b;
                    }



                    .list-symbol-bullet li {
                        line-height: 30px;
                        list-style-type: none;
                    }


                    .address {
                        margin-top:20px;
                    }




                </style>
                <a data-target=".printPreview" data-toggle="modal" class="btn btn-primary">Print Preview</a>

                <div class="modal fade printPreview" >
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body text-body print-pp">


                                <div class="row bigsidepadding">
                                    <div class="col-md-4">
                                        <img src="images/Oway-Logo.png" width="200px"/>
                                    </div>

                                    <div class="col-md-5 pull-right smallpadding">
                                        <p>49/B, 1st Floor, Moe Sandar Street, Ward (1), Kamayut, Yangon, Myanmar</p>
                                        <span class="text-muted">Telephone:</span> 01 525011, 01 503196  <br>
                                        <span class="text-muted">Email:</span> <a href="mailto:cvs@owayhrsolutions.com">cvs@owayhrsolutions.com.mm<br></a>
                                    </div>
                                </div>
                                <?php
                                $this->db->where('cv_id',$cv_id);
                                $per=$this->db->get('c_information_tbl')->row_array();
                                ?>
                                <div  style="margin-bottom:10px;padding: 1px;border-bottom: 2px solid #7dc24b;"></div>

                                <div class="row bigsidepadding">
                                    <div class="col-md-4">
                                        <h3>
                                            <?=$per['first_name']?> <?=$per['last_name']?>
                                        </h3>
                                        <p class="text-body text-sm text-muted space-top-none">
                                            Ref: <?=$per['cv_id']?>
                                        </p>
                                        <div class="address">
                                            <span class="text-muted">Mobile:</span>
                                            <?=$per['c_number']?><?php if($per['sec_contact_no']){echo ", ".$per['sec_contact_no'];}?><br>
                                            <span class="text-muted">Email:</span>
                                            <a href="mailto:a.baby.of.blackgang@gmail.com"><?=$per['email']?><br></a>
                                            <?php
                                            $appos=$this->db->query("SELECT DISTINCT apply_post from app_pos_tbl WHERE cv_id = '$cv_id' ORDER BY apply_post DESC")->row_array();
                                            ?>
                                        </div>
                                        Apply Position: <?=$appos['apply_post']?>
                                                    <span>


                                                    </span><br>
                                        Salary Expectations:
                                                    <span class="cur">
                                                        <?=$per['salary_exp'];?>

                                                    </span>
                                        <?php if($per['not_period']):?>
                                        <br>
                                        Noticed Period:
                                                    <span class="cur">
                                                        <?=$per['not_period'];?>
                                                    </span>
                                        <?php endif?>


                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">
                                        <?php if($per['cv_pic']):?>
                                            <img src="uploads/profile/<?=$per['cv_pic']?>" width="220px" class="img-responsive" >
                                        <?php endif?>
                                    </div>
                                </div>


                                <h3>Summary</h3>
                                <div class="line"></div>
                                <div class="row bigsidepadding smallpadding">
                                    <div class="col-md-8 col-md-offset-2">
                                        <ul class="list-symbol-bullet space-bottom-none">
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Profile:</div>
                                                    <div class="col-md-6">
                                                        <?=gender($per['gender'])?>,
                                                        <?=age($per['date_of_birth'])?>,
                                                        <?=marital($per['marital'])?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Date of birth:</div>
                                                    <div class="col-md-6">
                                                        <?=age($per['date_of_birth'])?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Father Name:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['dad_name']?>
                                                       </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">N.R.C No:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['nrc_no'];?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Nationality:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['nationality'];?>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                            if($per['height']):
                                            ?>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Height:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['height'];?>
                                                    </div>
                                                </div>
                                            </li>
                                                <?php
                                            endif
                                            ?>
                                            <?php
                                            if($per['weight']):
                                            ?>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Weight:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['weight'];?>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                            endif
                                            ?>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Current Location:</div>
                                                    <div class="col-md-6">
                                                        <?php
                                                        if($per['cur_location']):
                                                            ?>
                                                            <?=$this->main_model->country($per['cur_location'])?>,
                                                            <?php
                                                        endif;
                                                        ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Address:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['address']?></p>

                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div><p>

                                    <div class="row" style="margin-bottom:3px;padding: 1px;border-bottom: 1px solid #000;"></div>


                                    <div class="col-md-8 col-md-offset-2">
                                        <ul class="list-symbol-bullet space-bottom-none">
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Current Position:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['cur_post'];?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-6">Company:</div>
                                                    <div class="col-md-6">
                                                        <?=$per['cur_company'];?>
                                                    </div>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>




                                <?php
                                if($per['c_objective']):
                                ?>
                                <h3>Career Objective</h3>
                                <div class="line"></div>
                                <div class="row bigsidepadding smallpadding">
                                    <?=$per['c_objective']?>
                                </div>
                                <?php
                                endif;
                                ?>


                                <?php
                                $count=$this->db->query("SELECT COUNT(cv_id) as count FROM ref_person_tbl where cv_id = '$cv_id'  ")->row_array();
                                if($count['count']!=0):
                                    ?>

                                    <h3>Reference Person</h3>
                                    <div class="line"></div>
                                    <?php
                                    $i=0;
                                    $this->db->order_by('t_id','DESC');
                                    $this->db->where('cv_id',$cv_id);
                                    $query=$this->db->get('ref_person_tbl')->result_array();
                                    foreach ($query as $key => $ref) :
                                        $i++;
                                        ?>
                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-8 col-md-offset-2">

                                                <ul class="list-symbol-bullet space-bottom-none">
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-6"> Full Name :</div>
                                                            <div class="col-md-6">
                                                                <?=$ref['ref_name']?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-6"> Occupation :</div>
                                                            <div class="col-md-6">
                                                                <?=$ref['ref_occupation']?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-6"> Employer :</div>
                                                            <div class="col-md-6">
                                                                <?=$ref['ref_employer']?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    if($ref['ref_ph_no']):
                                                    ?>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-6"> Mobile No :</div>
                                                            <div class="col-md-6">
                                                                <?=$ref['ref_ph_no']?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                        <?php
                                                        endif
                                                        ?>
                                                    <?php
                                                    if($ref['ref_email']):
                                                    ?>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-6"> Email :</div>
                                                            <div class="col-md-6">
                                                                <?=$ref['ref_email']?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                        <?php
                                                    endif
                                                    ?>

                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                        if($count['count']!=$i):
                                            ?>
                                            <div style="border: 1px solid rgba(0, 0, 0, 0.18);"></div>
                                            <?php
                                        endif;
                                        ?>
                                        <?php
                                    endforeach;
                                endif;
                                ?>


                                <?php
                                $count=$this->db->query("SELECT COUNT(cv_id) as count FROM education_tbl where cv_id = '$cv_id'  ")->row_array();
                                if($count['count']!=0):
                                    ?>

                                    <h3>Educational Background</h3>
                                    <div class="line"></div>

                                    <?php
                                    $this->db->order_by('grad_start_year','DESC');
                                    $this->db->where('cv_id',$cv_id);
                                    $query=$this->db->get('education_tbl')->result_array();
                                    foreach ($query as $key => $edu) :
                                        ?>
                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-4">
                                                <?php if($edu['grad_start_month']!=0):?>
                                                    <?=name_of_month($edu['grad_start_month'])?>,
                                                <?php endif ; ?>
                                                <?php if($edu['grad_start_year']!=0):?>
                                                    <?=$edu['grad_start_year']?>
                                                <?php endif;?>

                                                <?php
                                                if ($edu['grad_date']==1) {
                                                    echo "- present";
                                                }
                                                else
                                                {
                                                    if($edu['grad_month']!=0 || $edu['grad_date']!=0 )
                                                    {
                                                        echo '- ' ;
                                                    }
                                                    if($edu['grad_month']!=0) {
                                                        echo name_of_month($edu['grad_month']).', ';
                                                    }
                                                    if($edu['grad_date']!=0)
                                                    {
                                                        echo $edu['grad_date'];
                                                    }

                                                }

                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <h5 class="nopadding"><?=$edu['university']?></h5>
                                            </div>
                                            <div class="col-md-4 text-right">
                                                <h5 class="nopadding">  <?php if($edu['degree_lvl']!=30){echo $this->main_model->degree_lvl($edu['degree_lvl']);}else{echo $edu['otherdegree_lvl'];} ?>, <?=$edu['subject_m']?> </h5>
                                            </div>
                                        </div>

                                        <div class="row bigsidepadding ">
                                            <div class="col-md-8 col-md-offset-4">
                                                <?=$this->main_model->country($edu['country'])?>
                                            </div>
                                        </div>

                                        <?php
                                    endforeach;
                                endif;
                                ?>


                                <?php
                                $count=$this->db->query("SELECT COUNT(cv_id) as count FROM certi_tbl where cv_id = '$cv_id'  ")->row_array();
                                if($count['count']!=0):
                                    ?>

                                    <h3>Other Qualifications</h3>
                                    <div class="line"></div>

                                    <?php
                                    $this->db->order_by('certi_year','DESC');
                                    $this->db->where('cv_id',$cv_id);
                                    $query=$this->db->get('certi_tbl')->result_array();
                                    foreach ($query as $key => $certi) :
                                        ?>
                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-4">
                                                <?php if($certi['certi_month']!=0):?>
                                                    <?=name_of_month($certi['certi_month'])?>,
                                                <?php endif ; ?>
                                                <?php if($certi['certi_year']!=0):?>
                                                    <?=$certi['certi_year']?>
                                                <?php endif;?>
                                            </div>
                                            <div class="col-md-4">
                                               <h5 class="nopadding"><?=$certi['t_center']?></h5>
                                                <?=$this->main_model->country($certi['certi_country'])?>
                                            </div>
                                            <div class="col-md-4 text-right">
                                                <h5 class="nopadding"><?=$certi['certi']?></h5>
                                            </div>
                                        </div>

                                        <?php
                                    endforeach;
                                endif;
                                ?>

                                <?php
                                $count=$this->db->query("SELECT COUNT(cv_id) as count FROM it_skill_tbl where cv_id = '$cv_id'  ")->row_array();
                                if($count['count']!=0):
                                    ?>

                                    <h3>IT Skills</h3>
                                    <div class="line"></div>
                                    <?php
                                     $this->db->order_by('t_id','DESC');
                                    $this->db->where('cv_id',$cv_id);
                                    $query=$this->db->get('it_skill_tbl')->result_array();
                                    foreach ($query as $key => $it) :
                                        ?>
                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-8 col-md-offset-2">
                                                <h5> <?=$it['topic_app']?></h5>
                                                <ul class="list-symbol-bullet space-bottom-none">
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-8"> <?=it_type($it['it_type'])?>:</div>
                                                            <div class="col-md-4">
                                                                <?=it_skill_lvl($it['it_skill_lvl'])?>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                <?php
                                $count=$this->db->query("SELECT COUNT(cv_id) as count FROM lang_tbl where cv_id = '$cv_id'  ")->row_array();
                                if($count['count']!=0):
                                    ?>

                                    <h3>Languages</h3>
                                    <div class="line"></div>
                                    <?php
                                     $this->db->order_by('t_id','DESC');
                                    $this->db->where('cv_id',$cv_id);
                                    $query=$this->db->get('lang_tbl')->result_array();
                                    foreach ($query as $key => $lang) :
                                        ?>
                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-8 col-md-offset-2">

                                                <ul class="list-symbol-bullet space-bottom-none">
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-6"> <?=$this->main_model->lang($lang['lang'])?>:</div>
                                                            <div class="col-md-6">
                                                                <?=lang_skill($lang['lang_skill'])?>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>


                                <?php
                                $count=$this->db->query("SELECT COUNT(cv_id) as count FROM working_exp_tbl where cv_id = '$cv_id'  ")->row_array();
                                if($count['count']!=0):
                                    ?>

                                    <h3>Working Experience</h3>
                                    <div class="line"></div>

                                    <?php
                                    $i=0;
                                    $this->db->order_by("start_year","DESC");
                                    $this->db->where('cv_id',$cv_id);
                                    $query=$this->db->get('working_exp_tbl')->result_array();
                                    foreach ($query as $key => $exp) :
                                        $i++;
                                        ?>




                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-4">
                                                <?php if($exp['start_month']!=0):?>
                                                    <?=name_of_month($exp['start_month'])?>,
                                                <?php endif ; ?>
                                                <?php if($exp['start_year']!=0):?>
                                                    <?=$exp['start_year']?>
                                                <?php endif;?> <?php
                                                if ($exp['end_year']==1) {
                                                    echo "- present";
                                                }
                                                else
                                                {
                                                    if($exp['end_month']!=0 || $exp['end_year']!=0 )
                                                    {
                                                        echo '- ' ;
                                                    }
                                                    if($exp['end_month']!=0) {
                                                        echo name_of_month($exp['end_month']).', ';
                                                    }
                                                    if($exp['end_year']!=0)
                                                    {
                                                        echo $exp['end_year'];
                                                    }

                                                }
                                                ?><br>


                                            </div>
                                            <div class="col-md-4">
                                                <h5 class="nopadding"><?=$exp['job_title']?></h5>
                                            </div>

                                            <div class="col-md-4 text-right">
                                                <h5 class="nopadding"><?=$exp['company']?></h5>
                                            </div>
                                        </div>
                                        <div class="row bigsidepadding">
                                            <div class="col-md-8">
                                                <span ><b>Industry:</b></span>
                                            <?=$this->main_model->job_industry($exp['exp_industry'])?>
                                            </div>

                                            <div class="col-md-4">
                                                <p class="nopadding"><?=$exp['city']?>, <?=$this->main_model->country($exp['country'])?></p>
                                            </div>
                                        </div>

                                        <div class="row bigsidepadding smallpadding">
                                            <div class="col-md-4">
                                                <?php
                                                if($exp['description']):
                                                    ?>
                                                    <h5 class="nopadding">Job Description</h5>
                                                <?php endif;?>
                                            </div>
                                            <div class="col-md-8 ">
                                                <p><?=$exp['description']?>  </p>
                                            </div>
                                        </div>
                                        <?php
                                        if($count['count']!=$i):
                                            ?>
                                            <div style="border: 1px solid rgba(0, 0, 0, 0.18);"></div>
                                            <?php
                                        endif;
                                        ?>
                                        <?php
                                    endforeach;
                                endif;
                                ?>





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">


            <label>Compose New Message</label>

            <div class="form-group">
                <input name="to_mail" class="form-control" placeholder="To:">
            </div>
            <div class="form-group">
                <input name="subject" class="form-control" placeholder="Subject:" >
            </div>
            <div class="form-group">
                    <textarea name="body" id="compose-textarea" class="form-control" style="height: 300px">

                    </textarea>
            </div>



            <div class="pull-right">

                <?=form_submit("Print","Export PDF","class='btn btn-primary' id='printTrigger' ")?>
            </div>
            <?=form_close();?>


        </div>
    </div>

</div>
