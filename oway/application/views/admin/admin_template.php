<!DOCTYPE html>

<html>
<base href="<?=base_url()?>"/>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Oway`s Online Database Management System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="css/dataTables.bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.css">
        <link href="css/bootstrap-chosen.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/bootstrap3-wysihtml5.css" rel="stylesheet">
    <link href="css/jquery.datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="css/cropper.css">
    <link rel="stylesheet" href="css/cropper-support.css">
    <!-- Ionicons -->
<!--     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 -->    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <link rel="stylesheet" href="css/AdminLTE.css">
    <link rel="stylesheet" href="css/skins/skin-blue.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="admin" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Oway</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Oway</b> </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-target=".helpmail" data-toggle="modal">
                  <i class="fa fa-life-saver"></i>
                </a>

              </li>
        
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?=$loginame?></span>
                </a>
                <ul class="dropdown-menu">
    
                  <!-- Menu Footer-->
                  <li class="user-footer">
                   
                    <div class="pull-right">
                      <?=anchor("site/logout","Logout"," class='btn btn-default btn-flat' ")?>
                     
                    </div>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$loginame?></p>
              <!-- Status -->
              <a href="admin"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>



          <?=$this->load->view($sidebar_menu)?>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$pagetitle?>
            
          </h1>
          <?=$this->breadcrumbs->show()?>
        </section>

        <!-- Main content -->
        <section class="content">
          
          <?=$this->load->view($main_content)?>
          
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <div class="loader"></div>
      <div class="modal fade confirm" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
       <div class="modal-body">
    <span class="war-text">Are you sure?</span>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="clear">Cancel</button>
  </div>
    </div>
  </div>
</div>

      <div class="modal fade helpmail" >
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Help Form</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label>Message</label>
                  <?=form_open("admin/helpemail",' id="mailform" ')?>
                  <?=form_textarea("message",set_value('message'),"placeholder='Type your issue Here' id='bodyemail' class='form-control' required")?>
                </div>

            </div>
            <div class="modal-footer">
              <?=form_submit("Send","Send"," id='emailhelp' class='btn btn-primary'")?>
              <?=form_close();?>
            </div>
          </div>
        </div>
      </div>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <?php
                $this->db->where('user_id',$this->session->userdata('user_id'));
                $this->db->order_by('date_time',"DESC");
                $this->db->limit(6);
                $query=$this->db->get('activity_log_tbl')->result_array();
                foreach($query as $key => $row):
              ?>

              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-gear bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">
                    <?php if($this->session->userdata('user_id')==$row['user_id']):?>
                      You
                      <?php else:?>
                  <?=$row['real_name']?>
                    <?php endif;?>
                    </h4>
                    <p><?=$row['description']?> <?=$row['About']?> - <?=date_time($row['date_time'])?></p>
                  </div>
                </a>
              </li>
              <?php endforeach;?>
              <li>
              <a href="admin/all-activity" class="btn btn-primary">View All</a>
              </li>
            </ul><!-- /.control-sidebar-menu -->


            </ul><!-- /.control-sidebar-menu -->

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->

        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
       
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="http://www.owayhrsolutions.com.mm">Oway</a>.</strong> All rights reserved.
        <div class="pull-right hidden-xs">
          Powered by <strong> <a target="_blank" href="http://www.invisiblestudio-mm.com">Invisible Studio</a></strong>

        </div>
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="js/jquery.js"></script>
     <!-- DataTables -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Other -->
    <script type="text/javascript" src="js/cropper.js"></script>
    <script type="text/javascript" src="js/currency.js"></script>
    <script type="text/javascript" src="js/currencyall.js"></script>
    <script type="text/javascript" src="js/cropper-support.js"></script>
    <script type="text/javascript" src="js/chosen-js.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/bootstrap3-wysihtml5.all.min.js"></script>
<!--    <script type="text/javascript" src="js/jquery.ajaxfileupload.js"></script>-->
    <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
    <!-- AdminLTE App -->
    <script src="js/app.min.js"></script>
    <script src="js/myscript.js"></script>


  </body>
</html>
