<section class="row smallsidepadding">
<div class="col-md-3">
<div class="setting-block text-center">
<i class="fa fa-sitemap fa-5x zoom text-light-green"></i><br>
<?=anchor('admin/job-category-show','Job Category Management')?>
</div>
</div>

	<div class="col-md-3">
		<div class="setting-block text-center">
			<i class="fa fa-sitemap fa-5x zoom text-light-green"></i><br>
			<?=anchor('industry/industry-show','Job Industry Management')?>
		</div>
	</div>

<div class="col-md-3">
<div class="setting-block text-center">
	<i class="fa fa-flag-checkered fa-5x zoom text-dark-bluegreen"></i><br>
<?=anchor('admin/country-management','Country Management')?>
</div>
</div>
<div class="col-md-3">
<div class="setting-block text-center">
	<i class="fa fa-user-plus fa-5x  "></i><br>
<?=anchor('admin/user-management','User Management')?>
</div>
</div>
</section>