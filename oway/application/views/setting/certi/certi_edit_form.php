<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $id = urldecode($this->uri->segment(3));
    $this->db->where('t_id',$id);
    $col=$this->db->get('certi_tbl')->row_array();
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="certi_form_wrapper">
    <?=form_open("",' id="certi_form" ')?>
        <div class="row">
          <div class="col-md-5">
          <div class="form-group"> 
          <?=form_label('Certificate')?>                       
                <?=form_input("certi",$col['certi'],"placeholder='Certificate'  id='certi'   class='form-control' ")?>
          </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md-5">
          <div class="form-group"> 
          <?=form_label('Training Center')?>                       
          <?=form_input("t_center",$col['t_center'],"placeholder='Training Center'  id='t_center' class='form-control'")?>
          </div>
          </div> 

        </div>   
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Country </label>
                    <select name="certi_country" id="certi_country" data-placeholder="Country" class="chosen-select" tabindex="2">
                        <option value=""></option>
                        <?php

                        $query=$this->db->get('country_tbl')->result_array();
                        foreach ($query as $key => $val) :
                            ?>
                            <option value="<?=$val['t_id']?>" <?php if($col['certi_country']==$val['t_id']){echo "selected";}?> ><?=$val['d_name']?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                   <?=form_label('Date Issued')?>                       
                <select name="certi_year" id="certi_year" class="form-control">
                  <option value=""> Select Year</option>
                  <?php
                   $year = date('Y');
                    for($i=$year; $i>1980; $i--):
                   ?> 
                      <option value="<?=$i?>" <?php if($i==$col['certi_year']){echo "selected";}?> ><?=$i?></option>
                  <?php
                    endfor;
                  ?>
                </select>          
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                   <label class="labelless"> </label>                       
              <select name="certi_month" id="certi_month" class="form-control">
                  <option value=""> Select Month</option>
                  <?php
                   $month = 1;
                    for($i=$month; $i<=12; $i++):
                   ?> 
                      <option value="<?=$i?>" <?php if($i==$col['certi_month']){echo "selected";}?> ><?=name_of_month($i)?></option>
                  <?php
                    endfor;
                  ?>
                </select>        
              </div>
            </div>
        </div>    
          <input type="hidden" id="it_t_id" value='<?=$col['t_id']?>'>
          <div class="modal-footer">                    
            <a class='btn btn-danger' onclick="cancel_btn('certi_section','<?=base_url()?>admin/certi_show','certi_form_con','certi_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='certi_edit_submit'  url='".base_url()."admin/certi_edit'  ")?>
          </div>

    <?=form_close();?>
</div>

