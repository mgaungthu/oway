<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="certi_form_wrapper">
<?=form_open("",' id="certi_form" ')?>
    <div class="row">
      <div class="col-md-5">
      <div class="form-group"> 
      <?=form_label('Certificate')?>                       
            <?=form_input("certi",set_value('certi'),"placeholder='Certificate'  id='certi'   class='form-control' required")?>
      </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-5">
      <div class="form-group"> 
      <?=form_label('Training Center')?>                       
      <?=form_input("t_center",set_value('t_center'),"placeholder='Training Center'  id='t_center' class='form-control' required")?>
      </div>
      </div> 

    </div>   
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Country </label>
                <select name="certi_country" id="certi_country" data-placeholder="Country" class="chosen-select" tabindex="2" required>
                    <option value=""></option>
                    <?php

                    $query=$this->db->get('country_tbl')->result_array();
                    foreach ($query as $key => $val) :
                        ?>
                        <option value="<?=$val['t_id']?>"><?=$val['d_name']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
               <?=form_label('Date Issued')?>                       
            <select name="certi_year" id="certi_year" class="form-control">
              <option value=""> Select Year</option>
              <?php
               $year = date('Y');
                for($i=$year; $i>1980; $i--):
               ?> 
                  <option value="<?=$i?>"  ><?=$i?></option>
              <?php
                endfor;
              ?>
            </select>          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
               <label class="labelless"> </label>                       
          <select name="certi_month" id="certi_month" class="form-control">
              <option value=""> Select Month</option>
              <?php
               $month = 1;
                for($i=$month; $i<=12; $i++):
               ?> 
                  <option value="<?=$i?>" ><?=name_of_month($i)?></option>
              <?php
                endfor;
              ?>
            </select>        
          </div>
        </div>

    </div>    
      
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('certi_section','<?=base_url()?>admin/certi_show','certi_form_con','certi_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='certi_submit'  url='".base_url()."admin/certi_insert'  ")?>
      </div>

<?=form_close();?>
</div>
