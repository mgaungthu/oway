<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="certi_show_wrapper">
<?php 
  $cv_id = urldecode($this->uri->segment(3));
 $this->db->order_by('certi_year','DESC');
  $this->db->where('cv_id',$cv_id);
  $query=$this->db->get('certi_tbl')->result_array();
  foreach ($query as $key => $value) :
?>
<div class="cv-section">
  <div id="certi-<?=$value['t_id']?>" >
<ul class="list-inline pull-right">
                        <li>
                            <a class="edit-icon" id="edit_certi" name="<?=$value['t_id']?>">
                                <i class="fa fa-lg fa-edit" ></i>
                            </a>
                        </li>
                        <li>
                            <a id="confirm_modal" onClick="delete_dynamic('<?=$value['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/certi_show','certi_tbl','#certi-<?=$value['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                <i class="fa fa-lg fa-trash"></i>
                            </a>
                            
                            <input type="hidden" id="edit_certi_url" value="<?=base_url()?>admin/edit_certi">
                        </li>
</ul>
<dl>
                        <dt class="job-title"><?=degree_lvl($value['certi'])?></dt>
                        <dd class="job-bind">
                            <?=$value['t_center']?>, <?=$this->main_model->country($value['certi_country'])?>
                        </dd>
                        <dd class="job-bind">
                            <?=name_of_month($value['certi_month'])?>,
                            <?=$value['certi_year']?>
                        </dd>

</dl>

</div>
</div>
<?php endforeach;?>


</div>