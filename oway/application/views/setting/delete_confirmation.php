<?php if ($this->uri->segment(2)=='delete-jobcategory') :?>
<div class="bigsidepadding">
  <p class="blacktext">Are you sure to delete?</p>
<?=anchor('admin/job-category-delete/'.$this->uri->segment(3),'Delete',' class="btn btn-danger" ')?> | <?=anchor('admin/job-category-show','Cancel',' class="btn btn-primary" ')?>
</div>
<?php endif;?>

<?php if ($this->uri->segment(2)=='delete-country') :?>
<div class="bigsidepadding">
  <p class="blacktext">Are you sure to delete?</p>
<?=anchor('admin/delete-country-process/'.$this->uri->segment(3),'Delete',' class="btn btn-danger" ')?> | <?=anchor('admin/country-management','Cancel',' class="btn btn-primary" ')?>
</div>
<?php endif;?>

<?php if ($this->uri->segment(2)=='delete-industry') :?>
  <div class="bigsidepadding">
    <p class="blacktext">Are you sure to delete?</p>
    <?=anchor('industry/industry-delete/'.$this->uri->segment(3),'Delete',' class="btn btn-danger" ')?> | <?=anchor('industry/industry-show','Cancel',' class="btn btn-primary" ')?>
  </div>
<?php endif;?>
