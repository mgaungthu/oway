<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<input type="hidden" value="<?=base_url()?>admin/profile_upload" id="profileUploadUrl"/>
<div id="crop-avatar">
<div class="avatar-view" title="Upload Cv photo">
    <img src="uploads/profile/" alt="Cv Photo">
</div>

<!-- Cropping modal -->
<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="avatar-form" action="admin/uploadpf" enctype="multipart/form-data" method="post">
                <input readonly placeholder="Applicant No" type="hidden" name="photocv" id="cv_id"  value="<?=$cv_id?>" class="form-control" />

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                </div>
                <div class="modal-body">
                    <div class="avatar-body">

                        <!-- Upload image and data -->
                        <div class="avatar-upload">
                            <input type="hidden" class="avatar-src" name="avatar_src">
                            <input type="hidden" class="avatar-data" name="avatar_data">
                            <label for="avatarInput">Local upload</label>
                            <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                        </div>

                        <!-- Crop and preview -->
                        <div class="row">
                            <div class="col-md-9">
                                <div class="avatar-wrapper"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="avatar-preview preview-lg"></div>
                                <div class="avatar-preview preview-md"></div>
                                <div class="avatar-preview preview-sm"></div>
                            </div>
                        </div>

                        <div class="row avatar-btns">
                            <div class="col-md-9">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
                                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button id="profileUpload" type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </form>
        </div>
    </div>
</div><!-- /.modal -->
</div>
<!-- Loading state -->
<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>

<div class="row">
<div id="personal_section" class="col-md-12">
<!-- <div class="container" > -->
  <?=form_open("admin/personal-information-added",' id="c_info_form" ')?>


<div class="col-md-4">
<div class="creat-block">
   <span id="app_no"><input readonly placeholder="Applicant No" type="text" name="app_no" id="cv_id" value="<?=$cv_id?>" class="form-control" /></span>
</div>
</div>
<div class="col-md-4">
<div class="creat-block">
  <select name="target_location"  id="target_location" data-placeholder="Location" class="chosen-select" tabindex="2">
              <option value=""></option>       
              <option value="1" <?php if (1== $target_location) { echo "selected"; }?>>Local</option>
              <option value="2" <?php if (2== $target_location) { echo "selected"; }?>>Oversea</option>
</select> 
</div>
</div>
<div class="col-md-4">
<div class="creat-block">
<input type="hidden" id="url" value="<?=base_url()?>admin/app_no"/>
    <input readonly type="text" value="<?=$job_cate?> - <?=$this->main_model->job_des($job_cate)?>" class="form-control">
<!--<select name="job_cate" data-placeholder="Job Category " id="cat_code" class="chosen-select" tabindex="2">-->
<!--              <option value=""></option>-->
<!--              --><?php
//              $this->db->order_by('cat_code','ASC');
//               $query=$this->db->get('job_category_tbl')->result_array();
//               foreach ($query as $key => $val) :
//                ?>
<!--              <option value="--><?//=$val['cat_code']?><!--" --><?php //if ($val['cat_code']== $job_cate) { echo "selected"; }?><!-- >--><?//=$val['cat_code']?><!-- - --><?//=$val['description']?><!--</option>-->
<!--            --><?php //endforeach;?>
<!--</select>-->
</div>
</div>

<fieldset class="col-md-12 midtopmargin" id="form1">

<legend>Contact Information</legend>
<div class="row">
<div class="col-md-6">
<div class="form-group"> 
<?=form_label('Phone No')?>                       
      <?=form_input("contact_no",set_value('contact_no'),"placeholder='Phone No' type='number' rangelength='[6,12]' data-rule-required='true'  id='contact_no' class='form-control'")?>
</div>
</div>

<div class="col-md-6">

<div class="form-group"> 
<?=form_label('Email')?>                       
      <?=form_input("email",set_value('email'),"placeholder='Email' id='email' class='form-control' data-rule-required='true' data-rule-email='true'  ")?>
</div>
</div>
<input type="hidden" id="url2" value="<?=base_url()?>admin/show"/>
<input type="hidden" id="cv_id" value="<?=$cv_id?>"/>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group"> 
<?=form_label('Address')?>                       
      <?=form_textarea("address",set_value('address'),"placeholder='Address' id='address' class='form-control'   ")?>
</div>
</div>
<div>
<div class="modal-footer">
              
             <?=form_submit("Save","Save","class='btn ft-btn btn-primary' id='con_de' disabled ")?>
        </div>
</fieldset>
<?=form_close();?>
<!--  Preview -->
<div id="testshow" style="display:none;">
  
  <div class="col-md-12 midtopmargin">

      <legend>Contact Information</legend>
      <div class="edit-button text-right blacktext">
      <a  id="edit_contact"><i class="fa fa-pencil-square-o"></i></a>
      </div>
    <div class="row">
      <div class="col-md-6">
         <div class="contact-block">
      <span class="title" > Contact Number </span> : <span  id="contactshow"></span>
        </div>
      </div>
      <div class="col-md-6">
        <div class="contact-block">
      <span class="title" > Email Address </span> : <span  id="emailshow"></span>
        </div>
     </div>
    </div>
    <div class="row">
    <div class="col-md-12" >
      <div class="contact-block">
        <div class="col-md-1 nopadding">
          <span class="title" > Address </span> : 
        </div>
        <div class="col-md-11 nopadding">
          <span  id="addshow"></span>
        </div>
      </div>
    </div>
  </div>
 
  </div>


</div>

<!-- </div> -->

<?=form_open("",' id="p_info_form" ')?>
<fieldset class="col-md-12 midtopmargin" id="form2">
      <legend>Personal Information</legend>
      <div class="row">
      <div class="col-md-3">
      <div class="form-group"> 
      <?=form_label('First Name')?>                       
            <?=form_input("first_name",set_value('first_name'),"placeholder='First Name'  id='first_name'  data-rule-required='true' class='form-control'")?>
      </div>
      </div>
      <div class="col-md-3">
      <div class="form-group"> 
      <?=form_label('Last Name')?>                       
            <?=form_input("last_name",set_value('last_name'),"placeholder='Last Name'  id='last_name'  data-rule-required='true' class='form-control'")?>
      </div>
      </div>
      <div class="col-md-6">

      </div>
      <input type="hidden" id="form2url" value="<?=base_url()?>admin/show2"/>
      </div>
      <div class="row">
      <div class="col-md-3">
      <div class="form-group"> 
      <?=form_label('Nrc Number')?>                       
      <?=form_input("nrc_no",set_value('nrc_no'),"placeholder='Nrc Number' data-rule-required='true' id='nrc_no' class='form-control'")?>
      </div>
      </div> 
      <div class="col-md-3">
      <div class="form-group"> 
      <?=form_label('Religion')?>                       
      <?=form_input("religion",set_value('religion'),"placeholder='Religion' data-rule-required='true' id='religion' class='form-control'")?>
      </div>
      </div>         
        <div class="col-md-6 ">
           <?=form_label('Gender')?>
          <div class="form-group">
              <?php
              $options = array(
                  ''       => 'Choose One',
                  '1'      => 'Male',
                  '2'      => 'Female',

              );
              ?>
              <?=form_dropdown('gender', $options,'','class="form-control" id="gender" required')?>
          </div>
        </div>  
    </div>
    <div class="row">
        <div class="col-md-3">
      <div class="form-group"> 
      <?=form_label('Date of Birth')?>                       
      <?=form_input("date_of_birth",set_value('date_of_birth'),"placeholder='Date of Birth' id='date_of_birth' class='form-control' required")?>
      </div>
        </div>  
        <div class="col-md-3">
     <div class="form-group"> 
      <?=form_label('Father Name')?>                       
      <?=form_input("dad_name",set_value('dad_name'),"placeholder='Father Name' id='dad_name' class='form-control'")?>
      </div>
      </div>  
        <div class="col-md-6">
       <?=form_label('Marital Status')?>
          <div class="form-group">
           <?php
              $options = array(
                  ''       => 'Choose One',
                  '1'      => 'Single',
                  '2'      => 'Married',

              );
              ?>
              <?=form_dropdown('marital', $options,'','class="form-control" id="marital" required')?>
          </div>
        </div> 
      
    </div>  

    <div class="row">
    <div class="col-md-6">
      <div class="form-group">
           <?=form_label('Nationality')?>                       
      <?=form_input("nationality",set_value('nationality'),"placeholder='Nationality' id='nationality' class='form-control'")?>
      </div>
    </div>
        <div class="col-md-6">
      <div class="form-group"> 
      <?=form_label('Current Location')?> 
                <select name="cur_location" id="cur_location" data-placeholder="Current Location" class="chosen-select" tabindex="2">
              <option value=""></option>
              <?php
             
               $query=$this->db->get('country_tbl')->result_array();
               foreach ($query as $key => $val) :
                ?>
              <option value="<?=$val['t_id']?>"><?=$val['d_name']?></option>
            <?php endforeach;?>
            </select>
      </div>
        </div> 
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Current Position')?>
                <?=form_input("cur_post",set_value('cur_post'),"placeholder='Current Position' id='cur_post' class='form-control'")?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Current Company')?>
                <?=form_input("cur_company",set_value('cur_company'),"placeholder='Current Company' id='cur_company' class='form-control'")?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Salary Expectations')?>
                <?=form_input("salary_exp",set_value('salary_exp'),"placeholder='Salary Expectations' id='salary_exp' class='currency form-control'")?>

            </div>
        </div>
    </div>

      
      <div class="modal-footer">                    
        <?=form_submit("Save","Save","class='btn sec-btn btn-primary' id='per_info' disabled")?>
      </div>
</fieldset>
<?=form_close();?>

<!--  personalpreview  -->
<div id="personalpreview" style="display:none;">
  
  <div class="col-md-12 midtopmargin">

      <legend>Personal Information</legend>
      <div class="edit-button text-right blacktext">
      <a  id="edit_personal"><i class="fa fa-pencil-square-o"></i></a>
      </div>

    <div class="row">
      <div class="col-md-4">
         <div class="contact-block">
       <span class="title" > Name </span> : <span  id="first_name"></span> <span  id="last_name"></span>
        </div>
      </div>
      
      <div class="col-md-8">

     </div>
    </div>
    <div class="row">
    <div class="col-md-3" >
      <div class="contact-block">
    <span class="title" > Nrc No </span> : <span id="nrc_no"></span>
      </div>
    </div>
      <div class="col-md-3" >
      <div class="contact-block">
   <span class="title" > Religion </span> : <span id="religion"></span>
      </div>
    </div>
    <div class="col-md-6" >
      <div class="contact-block">
       <span class="title" > Gender </span> : <span id="gender"></span>
      </div>
    </div>
        <div class="col-md-3" >
      <div class="contact-block">
        <span class="title" > Age </span> : <span id="date_of_birth"></span>
      </div>
    </div>
     <div class="col-md-3" >
      <div class="contact-block">
       <span class="title" >Father Name</span> : <span id="dad_name"></span>
      </div>
    </div>
    <div class="col-md-6" >
      <div class="contact-block">
      <span class="title" >  Marital Status </span> : <span id="marital"></span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="contact-block">
      <span class="title" >  Nationality </span> : <span id="nationality"></span>
      </div>
    </div>
    <div class="col-md-6">
      <div class="contact-block">
      <span class="title" >  Current Location </span> : <span id="cur_location"></span>
      </div>
    </div>
  </div>


      <div class="row">
          <div class="col-md-4">
              <div class="contact-block">
                  <span class="title" >  Current Position </span> : <span id="cur_post"></span>
              </div>
          </div>
          <div class="col-md-4">
              <div class="contact-block">
                  <span class="title" >  Current Company </span> : <span id="cur_company"></span>
              </div>
          </div>
          <div class="col-md-4">
              <div class="contact-block">
                  <span class="title" >  Salary Expectations </span> : <span id="salary_exp"></span>
              </div>
          </div>
      </div>
 
  </div>




</div>

</div>

<!--    // Career Objective-->

    <div class="col-md-12 midtopmargin" id="career_section">
        <input type="hidden" id="objurl" value="<?=base_url()?>admin/career_update"/>
       <fieldset class="col-md-12 midtopmargin" id="carrer_form">
           <legend>Career Objective</legend>
        <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?=form_textarea("c_objective",set_value('address'),"placeholder='Career Objective' id='c_objective' class='form-control'   ")?>
                    </div>
                </div>
                <div>
                    <div class="modal-footer">
                        <?=form_submit("Save","Save","class='btn ft-btn btn-primary' id='career_obj' ")?>
                    </div>
        </fieldset>
        <?=form_close();?>
        <div id="career_wrap" style="display:none;">

            <div class="col-md-12 midtopmargin">

                <legend>Career Objective</legend>
                <div class="edit-button text-right blacktext">
                    <a  id="edit_obj"><i class="fa fa-pencil-square-o"></i></a>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <div class="contact-block">
                            <div class="col-md-12 nopadding">
                                <span  id="cb_show"></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

    <!-- ????????????? App Pos SECTION ???????????? -->

    <div class="col-md-12 midtopmargin" id="post_section">
        <input type="hidden" id="post_url" value="<?=base_url()?>admin/post_show">
        <legend><i class="fa fa-fw fa-dot-circle-o"></i> Apply Position</legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/post_form','post_section','post_show','post_form_con')" ><i class="fa fa-plus"></i></a>
        </div>
        <div id="post_show">

        </div>
        <div id="post_form_con">

        </div>


    </div>


<!-- ????????????? EXP SECTION ???????????? -->

  <div class="col-md-12 midtopmargin" id="exp_section">

       <legend><i class="fa fa-fw fa-briefcase"></i> Working Experience</legend>
      <div class="edit-button text-right blacktext">
      <a  id="add_new" url="<?=base_url()?>admin/work_exp"><i class="fa fa-plus"></i></a>
      </div>
      <div id="exp_show">
            
    </div>
      <div id="working_exp_form">

      </div>
    
    
    </div>

<!-- ????????????? Education SECTION ???????????? -->

    <div class="col-md-12 midtopmargin" id="edu_section">
       <legend>
        <i class="fa fa-fw fa-graduation-cap"></i>   Educational Background
      </legend>
      <div class="edit-button text-right blacktext">
      <a  id="add_new" onClick="addnew('<?=base_url()?>admin/education_form','edu_section','edu_show','education_form_con')" ><i class="fa fa-plus"></i></a>
      <input type="hidden" id="edu_url" value="<?=base_url()?>admin/edu_show">
      </div>
      <div id="edu_show">
            
        </div>
      <div id="education_form_con">

      </div>
    
    
    </div>

<!-- ????????????? Cerificate SECTION ???????????? -->

    <div class="col-md-12 midtopmargin" id="certi_section">
       <legend>
        <i class="fa fa-fw fa-certificate"></i>   Other Qualifications
      </legend>
      <div class="edit-button text-right blacktext">
      <a  id="add_new" onClick="addnew('<?=base_url()?>admin/certi_form','certi_section','certi_show','certi_form_con')" ><i class="fa fa-plus"></i></a>
      <input type="hidden" id="certi_url" value="<?=base_url()?>admin/certi_show">
      </div>
      <div id="certi_show">
            
        </div>
      <div id="certi_form_con">

      </div>    
    </div>

<!-- ????????????? IT-Skill SECTION ???????????? -->

    <div class="col-md-12 midtopmargin" id="it_section">
       <legend>
        <i class="fa fa-fw fa-laptop"></i>  IT Skills  
      </legend>
      <div class="edit-button text-right blacktext">
      <a  id="add_new" onClick="addnew('<?=base_url()?>admin/it_form','it_section','it_show','it_form_con')" ><i class="fa fa-plus"></i></a>
      <input type="hidden" id="it_url" value="<?=base_url()?>admin/it_show">
      </div>
      <div id="it_show">
            
        </div>
      <div id="it_form_con">

      </div>    
    </div>

<!-- ????????????? Language SECTION ???????????? -->

    <div class="col-md-12 midtopmargin" id="lang_section">
       <legend>
        <i class="fa fa-fw fa-language"></i>  Language Skills  
      </legend>
      <div class="edit-button text-right blacktext">
      <a  id="add_new" onClick="addnew('<?=base_url()?>admin/lang_form','lang_section','lang_show','lang_form_con')" ><i class="fa fa-plus"></i></a>
      <input type="hidden" id="lang_url" value="<?=base_url()?>admin/lang_show">
      </div>
      <div id="lang_show">
            
        </div>
      <div id="lang_form_con">

      </div>    
    </div>

    <div class="col-md-12 midtopmargin" id="lang_section">
       <legend>
       <i class="fa fa-fw fa-upload"></i> Attachment
      </legend>
    <div class="edit-button text-right blacktext">
      <a  id="add_new_img"  ><i class="fa fa-plus"></i></a>
      
      </div>
        
       <?=form_open_multipart('admin/uploadimg')?>
        <span id="app_no2">
            <input type="hidden" placeholder="Applicant No"  name="img_id" id="cv_id" value="<?=$cv_id?>" class="form-control" />
        </span>

      <div id="img_field_wrap">
       
     


      </div>

      

    </div>
    </div>
  

      <div class="col-md-12 midtopmargin text-right">
    <?=form_submit('Finish','Finish','class="btn btn-primary" id="uploadbtn" ')?>
      <?=form_close()?>

    </div>




