<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <div class="box">
<div class="box-body">
 <div class="col-md-6 col-md-push-3 bigtopmargin">

      <?=form_open("admin/job-category-add",'')?>
      <div class="col-md-3 smallpadding midsidepadding">
           <?=form_label('Code')?>
        </div>
        <div class="col-md-9">
                    <div class="form-group">                       
                        <?=form_input("code",set_value('code'),"placeholder='Code' class='form-control' required")?>
                    </div>
        </div>  
        
        <div class="col-md-3 smallpadding midsidepadding">
           <?=form_label('Description')?>
        </div>
        <div class="col-md-9">
                    <div class="form-group">                       
                        <?=form_input("description",set_value('description'),"placeholder='Description' class='form-control' required")?>
                    </div>
        </div>
        
       

     
        <div class="modal-footer">
              
      <?=form_submit("Save","Save","class='btn btn-primary'")?>
        </div>
 
</div>
        
      
  <div class="col-md-2">
  </div>
</div>
</div>
<?=form_close();?>
