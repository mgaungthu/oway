<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->db->where('cv_id',$cv_id);
$per=$this->db->get('c_information_tbl')->row_array();
?>

<?php
    if($target_location==1):
?>
    <div style="width: 230px;float: left;">
        <img src="images/Oway-Logo.png" width="220px"/>
    </div>

    <div style="width: 100px;float: left;">
    </div>

    <div class="top-box">
        <p>49/B, 1st Floor, Moe Sandar Street, Ward (1), Kamayut, Yangon, Myanmar</p>
        <span class="text-muted">Telephone:</span> 01 525011, 01 503196<br>
        <span class="text-muted">Email:</span> <a href="mailto:cvs@owayhrsolutions.com">cvs@owayhrsolutions.com.mm<br></a>
    </div>

    <div style="padding: 1px;border-bottom: 2px solid #7dc24b;"></div>
    <?php
    endif;
    ?>

                <div class="box-name">
                    <h3>
                        <?=$per['first_name']?> <?=$per['last_name']?>

                    </h3>
                    <p class="text-body text-sm text-muted space-top-none">
                        Ref: <?=$per['cv_id']?>
                    </p>

                    <?php
                        if($c_info==1):
                    ?>
                    <div class="address">
                        <span class="text-muted">Mobile:</span>
                        <?=$per['c_number']?><?php if($per['sec_contact_no']){echo ", ".$per['sec_contact_no'];}?><br>
                        <span class="text-muted">Email:</span>
                        <a href="mailto:a.baby.of.blackgang@gmail.com"><?=$per['email']?><br></a>

                    </div>
                    <?php
                        endif;
                    ?>
                                <div class="text-muted" style="width:100px;float: left;">Apply Position:</div>
                                <div  style="width:300px;float: left;" >
                                   <a href="#"> <?=$this->input->post('apply_post')?></a>
                                </div>

                                <div style="clear: both;"></div>

                                <div class="text-muted" style="width:100px;float: left;">Expected Salary:</div>
                                <div   style="width:300px;float: left;" >
                                    <?php

                                    $cur=number_format($per['salary_exp'], 2);
                                    ?>
                                   <?=$cur;?> Ks
                                </div>
                                <div style="clear: both;"></div>
                    <div class="text-muted" style="width:100px;float: left;">Noticed Period:</div>
                    <div   style="width:300px;float: left;" >
                        <?=$per["not_period"]?>
                    </div>
                    <div style="clear: both;"></div>

                </div>


                <div class="box text-right" >
                    <?php if($per['cv_pic']):?>
                <img src="uploads/profile/<?=$per['cv_pic']?>" width="220px" >
                    <?php endif?>

                </div>


    <div class="clearfix"></div>
    <h3>Summary</h3>
    <div class="line"></div>

    <div class="middle-box">

        <ul class="list-symbol-bullet space-bottom-none">
            <li>
                <div class="row">
                    <div class="smallbox">Profile:</div>
                    <div class="smallbox">
                        <?=gender($per['gender'])?>,
                        <?=marital($per['marital'])?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Date of birth</div>
                    <div class="smallbox">
                        <?=age($per['date_of_birth'])?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Father Name</div>
                    <div class="smallbox">
                        <?=$per['dad_name']?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">N.R.C No:</div>
                    <div class="smallbox">
                        <?=$per['nrc_no'];?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Nationality:</div>
                    <div class="smallbox">
                        <?=$per['nationality'];?>
                    </div>
                </div>
            </li>
            <?php
            if($per['height']):
            ?>
            <li>
                <div class="row">
                    <div class="smallbox">Height:</div>
                    <div class="smallbox">
                        <?=$per['height'];?>
                    </div>
                </div>
            </li>
            <?php
            endif;
            ?>
            <?php
            if($per['weight']):
            ?>
            <li>
                <div class="row">
                    <div class="smallbox">Weight:</div>
                    <div class="smallbox">
                        <?=$per['weight'];?>
                    </div>
                </div>
            </li>
                <?php
            endif;
            ?>
            <li>
                <div class="row">
                    <div class="smallbox">Current Location:</div>
                    <div class="smallbox">
                        <?=$this->main_model->country($per['cur_location'])?>,

                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Address:</div>
                    <div class="smallbox-add">
                        <p> <?=$per['address']?></p>

                    </div>
                </div>
            </li>
        </ul>
    </div>


    <div class="last-child">

        <ul class="list-symbol-bullet space-bottom-none">


            <li>
                <div class="row">
                    <div class="smallbox">Current Position:</div>
                    <div class="smallbox">
                        <?=$per['cur_post'];?>

                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Company:</div>
                    <div class="smallbox">
                        <?=$per['cur_company'];?>
                    </div>
                </div>
            </li>


        </ul>
    </div>


    <?php

    if($per['c_objective']):
        ?>

        <h3>Career Objective</h3>
        <div class="line"></div>
            <div style="margin-top: 15px">
                    <div>
                        <p>
                            <?=$per['c_objective'];?>
                        </p>
                    </div>
            </div>
            <?php
               endif;
            ?>


            <?php
            $count=$this->db->query("SELECT COUNT(cv_id) as count FROM ref_person_tbl where cv_id = '$cv_id'  ")->row_array();
            if($count['count']!=0):
                ?>

                <h3>Reference Person</h3>
                <div class="line"></div>
                <?php
                $i=0;
                $this->db->order_by('t_id','DESC');
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('ref_person_tbl')->result_array();
                foreach ($query as $key => $ref) :
                    $i++;
                    ?>
                    <div class="row bigsidepadding smallpadding">
                        <div class="last-child">

                            <ul class="list-symbol-bullet space-bottom-none">
                                <li>
                                    <div class="row">
                                        <div class="smallbox"> Full Name :</div>
                                        <div class="smallbox">
                                            <?=$ref['ref_name']?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="smallbox"> Occupation :</div>
                                        <div class="smallbox">
                                            <?=$ref['ref_occupation']?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="smallbox"> Employer :</div>
                                        <div class="smallbox">
                                            <?=$ref['ref_employer']?>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                if($ref['ref_ph_no']):
                                    ?>
                                    <li>
                                        <div class="row">
                                            <div class="smallbox"> Mobile No :</div>
                                            <div class="smallbox">
                                                <?=$ref['ref_ph_no']?>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                endif
                                ?>
                                <?php
                                if($ref['ref_email']):
                                    ?>
                                    <li>
                                        <div class="row">
                                            <div class="smallbox"> Email :</div>
                                            <div class="smallbox">
                                                <?=$ref['ref_email']?>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                endif
                                ?>

                            </ul>
                        </div>
                    </div>
                    <?php
                    if($count['count']!=$i):
                    ?>
                    <div style="border: 1px solid rgba(0, 0, 0, 0.18);"></div>
                        <?php
                    endif;
                    ?>
                    <?php
                endforeach;
            endif;
            ?>



    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM education_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>Educational Background</h3>
    <div class="line"></div>

    <?php
    $this->db->order_by('grad_start_year','DESC');
    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('education_tbl')->result_array();
    foreach ($query as $key => $edu) :
        ?>
    <div class="boxwrapper">
        <div class="col1" style="padding-top: 15px;">
            <dd class="job-bind">
                <?php if($edu['grad_start_month']!=0):?>
                    <?=name_of_month($edu['grad_start_month'])?>,
                <?php endif ; ?>
                <?php if($edu['grad_start_year']!=0):?>
                    <?=$edu['grad_start_year']?>
                <?php endif;?>

                <span class="job-bind" >
                                                    <?php
                                                    if ($edu['grad_date']==1) {
                                                        echo "- present";
                                                    }
                                                    else
                                                    {
                                                        if($edu['grad_month']!=0 || $edu['grad_date']!=0 )
                                                        {
                                                            echo '- ' ;
                                                        }
                                                        if($edu['grad_month']!=0) {
                                                            echo name_of_month($edu['grad_month']).', ';
                                                        }
                                                        if($edu['grad_date']!=0)
                                                        {
                                                            echo $edu['grad_date'];
                                                        }

                                                    }

                                                    ?>
                     </span>
            </dd>

        </div>
        <div class="boxex2">
            <div class="innerbox1">
                <h5><?=$edu['university']?></h5>
            </div>
            <div class="innerbox2">
                <h5>  <?php if($edu['degree_lvl']!=30){echo $this->main_model->degree_lvl($edu['degree_lvl']);}else{echo $edu['otherdegree_lvl'];} ?>, <?=$edu['subject_m']?>  </h5>
            </div>
            <div class="clearfix"></div>
            <div class="fullbox">
                <?=$this->main_model->country($edu['country'])?>
            </div>
        </div>
    </div>
    <?php
    endforeach;
    endif;
    ?>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM certi_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>Other Qualifications</h3>
    <div class="line"></div>

    <?php
    $this->db->order_by('certi_year','DESC');
    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('certi_tbl')->result_array();
    foreach ($query as $key => $certi) :
        ?>
        <div class="boxwrapper">
            <div class="col1" style="padding-top: 15px;">
                <dd class="job-bind">
                    <?php if($certi['certi_month']!=0):?>
                        <?=name_of_month($certi['certi_month'])?>,
                    <?php endif ; ?>
                    <?php if($certi['certi_year']!=0):?>
                        <?=$certi['certi_year']?>
                    <?php endif;?>


                </dd>
            </div>
            <div class="col1" style="padding-top: 6px;">
                <h5><?=$certi['t_center']?></h5>
                <p><?=$this->main_model->country($certi['certi_country'])?></p>
            </div>
            <div class="col1" style="margin-left:15px;padding-top: 6px;">
                <h5><?=$certi['certi']?>   </h5>
            </div>

        </div>
    <?php
    endforeach;
    endif;
    ?>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM it_skill_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>IT Skills</h3>
    <div class="line"></div>
    <?php
    $this->db->order_by('t_id','DESC');
    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('it_skill_tbl')->result_array();
    foreach ($query as $key => $it) :
    ?>
    <div class="last-child">
        <h5> <?=$it['topic_app']?></h5>
        <ul class="list-symbol-bullet space-bottom-none">
            <li>
                <div class="row">
                    <div style="width: 250px;float: left;"> <?=it_type($it['it_type'])?>:</div>
                    <div style="width: 150px;float: left;">

                        <?=it_skill_lvl($it['it_skill_lvl'])?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <?php
    endforeach;
    endif;
    ?>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM lang_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>
    <h3>Languages</h3>
    <div class="line"></div>

        <div class="last-child">

            <ul class="list-symbol-bullet space-bottom-none">
        <?php
      $this->db->order_by('t_id','DESC');
        $this->db->where('cv_id',$cv_id);
        $query=$this->db->get('lang_tbl')->result_array();
        foreach ($query as $key => $lang) :
            ?>
                <li>
                    <div class="row">
                        <div class="smallbox"> <?=$this->main_model->lang($lang['lang'])?>:</div>
                        <div class="smallbox">
                            <?=lang_skill($lang['lang_skill'])?>
                        </div>
                    </div>
                </li>
            <?php
        endforeach;
        endif;
        ?>
            </ul>
        </div>

    <!--    Work Experience-->

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM working_exp_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
        ?>

        <h3>Working Experience</h3>
        <div class="line"></div>

        <?php
        $i=0;
         $this->db->order_by("start_year","DESC");
        $this->db->where('cv_id',$cv_id);
        $query=$this->db->get('working_exp_tbl')->result_array();
        foreach ($query as $key => $exp) :
            $i++;
            ?>
            <div class="boxwrapper">
                <div class="col1" style="padding-top: 15px;">
                    <dd class="job-bind">
                        <?php if($exp['start_month']!=0):?>
                            <?=name_of_month($exp['start_month'])?>,
                        <?php endif ; ?>
                        <?php if($exp['start_year']!=0):?>
                            <?=$exp['start_year']?>
                        <?php endif;?>

                        <span class="job-bind" >
                                                        <?php
                                                        if ($exp['end_year']==1) {
                                                            echo "- present";
                                                        }
                                                        else
                                                        {
                                                            if($exp['end_month']!=0 || $exp['end_year']!=0 )
                                                            {
                                                                echo '- ' ;
                                                            }
                                                            if($exp['end_month']!=0) {
                                                                echo name_of_month($exp['end_month']).', ';
                                                            }
                                                            if($exp['end_year']!=0)
                                                            {
                                                                echo $exp['end_year'];
                                                            }

                                                        }
                                                        ?>

                </span>
                    </dd>

                </div>

                <div class="col1" style="padding-top: 6px;margin-left: 5px;" >
                    <h5><?=$exp['job_title']?></h5>
                </div>

                <div class="col1" style="padding-top: 6px;">
                    <h5>"<?=$exp['company']?>"</h5>
                </div>

                <div  style="width:300;float:left;margin-left: 40px">
                    <span><b>Industry</b>:</span>

                        <?=$this->main_model->job_industry($exp['exp_industry'])?>
                </div>

                <div class="col1" style="margin-left: 160px" >
                    <p><?=$exp['city']?>, <?=$this->main_model->country($exp['country'])?> </p>
                </div>

                <div class="col1" style="margin-left: 40px">
                    <?php
                    if($exp['description']):
                        ?>
                        <b>Job Description:<b>
                    <?php endif;?>
                </div>
                <div class="col2">
                    <p>
                        <?=$exp['description']?>
                    </p>
                </div>
            </div>
            <?php
            if($count['count']!=$i):
                ?>
                <div style="border: 1px solid rgba(0, 0, 0, 0.18);"></div>
                <?php
            endif;
            ?>
            <?php
        endforeach;
    endif;
    ?>


