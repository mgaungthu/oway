<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
        

<div class="box">
<div class="container bigpadding"> 
 <div class="col-md-6 col-md-push-2 smalltopmargin">
      <?=form_open("admin/final-create",'')?>
      
     <div class="col-md-3 smallpadding midsidepadding">
           <?=form_label('Job Category')?>
      </div>
       <div class="col-md-9">
        <div class="form-group">     
                <select name="job_cate" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
              <option value=""></option>
              <?php
              $this->db->order_by('cat_code','ASC');
               $query=$this->db->get('job_category_tbl')->result_array();
               foreach ($query as $key => $val) :
                ?>
              <option value="<?=$val['cat_code']?>"><?=$val['cat_code']?> - <?=$val['description']?></option>
            <?php endforeach;?>
            </select>
        </div>
       </div>

     <div class="col-md-3 smallpadding midsidepadding">
         <?=form_label('Job Industry')?>
     </div>
     <div class="col-md-9">
         <div class="form-group">
             <select name="job_industry" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                 <option value=""></option>
                 <?php
                 $this->db->order_by('description','DESC');
                 $ind=$this->db->get('industry_tbl')->result_array();
                 foreach ($ind as $key => $val) :
                     ?>
                     <option value="<?=$val['t_id']?>"><?=$val['description']?></option>
                 <?php endforeach;?>
             </select>
         </div>
     </div>


     <div class="col-md-3 smallpadding midsidepadding">
           <?=form_label('Location')?>
      </div>
       <div class="col-md-9">
        <div class="form-group">     
                <select name="target_location"  class="form-control" required >
              <option value="">Choose one</option>
               
              <option value="1">Local</option>
              <option value="2">Oversea</option>
            
            </select>
        </div>
       </div>

     <div class="col-md-3 smallpadding midsidepadding">
         <?=form_label('Position')?>
     </div>
     <div class="col-md-9">
         <div class="form-group">
             <?=form_input("apply_post",set_value('apply_post'),"placeholder='Apply Position'  id='apply_post'   class='form-control' required")?>

         </div>
     </div>
      <div class="modal-footer">              
          <?=form_submit("Save","Continue","class='btn btn-primary'")?>
      </div>


 
</div>
 </div> 
</div>
<?=form_close();?>
