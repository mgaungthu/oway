<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="edu_form_wrapper">
<?=form_open("",' id="it_form" ')?>
      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Application, topic or language')?>                       
            <?=form_input("topic_app",set_value('topic_app'),"placeholder='Application, topic or language'  id='topic_app'   class='form-control' required")?>
      </div>
      </div>

      </div> 

      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Type')?>                       
                 <?php
                    $options = array(
                                         ''       => 'Select',
                                         '1'      => 'Windows & Office tools',
                                         '2'      => 'Web programming & development',
                                         '3'      => 'Non-web programming languages',
                                         '4'      => 'Operating systems, Networking & Hardware',
                                                                     
                                         );  
                          ?>
          <?=form_dropdown('it_type', $options,'','class="form-control" id="it_type" required')?>      
        </div>
      </div>

      </div>

      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Skill Level ')?>                       
                 <?php
                    $options = array(
                                         ''       => 'Select',
                                         '1'      => 'Basic',
                                         '2'      => 'Intermediate',
                                         '3'      => 'Advanced',
                                         '4'      => 'Expert',
                                                                     
                                         );  
                          ?>
          <?=form_dropdown('it_skill_lvl', $options,'','class="form-control" id="it_skill_lvl" required')?>      
        </div>
      </div>

      </div>

    


      <input type="hidden" id="url_show" value='<?=base_url()?>admin/it_show'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('it_section','<?=base_url()?>admin/it_show','it_form_con','it_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='it_submit'  url='".base_url()."admin/it_insert'  ")?>
      </div>

<?=form_close();?>
</div>
