<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="it_show_wrapper">
<?php 
  $cv_id = urldecode($this->uri->segment(3));
  $this->db->order_by('t_id','DESC');
  $this->db->where('cv_id',$cv_id);
  $query=$this->db->get('it_skill_tbl')->result_array();
  foreach ($query as $key => $value) :
?>
<div class="cv-section">
  <div id="it-<?=$value['t_id']?>" >
<ul class="list-inline pull-right">
                        <li>
                            <a class="edit-icon" id="edit_itskill" name="<?=$value['t_id']?>">
                                <i class="fa fa-lg fa-edit" ></i>
                            </a>
                        </li>
                        <li>
                            <a id="confirm_modal" onClick="delete_dynamic('<?=$value['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/it_show','it_skill_tbl','#it-<?=$value['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                <i class="fa fa-lg fa-trash"></i>
                            </a>
                            
                            <input type="hidden" id="edit_it_url" value="<?=base_url()?>admin/edit_itskill">
                        </li>
</ul>
<dl>
                        <dt class="job-title"><?=$value['topic_app']?></dt>
                        <dd class="job-bind">
                            <?=it_type($value['it_type'])?>,
                        </dd>
                        <dd class="job-bind">
                            <?=it_skill_lvl($value['it_skill_lvl'])?>
                            
                            </span>
                          
                        </dd>
</dl>

<!-- <div id="loading" style="display:none;">
      <h1>Loading</h1>
</div> -->
</div>
</div>
<?php endforeach;?>



</div>