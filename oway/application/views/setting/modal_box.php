<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="modal fade confirm" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
       <div class="modal-body">
    <span class="war-text">Are you sure?</span>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn danger" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn" id="clear">Cancel</button>
  </div>
    </div>
  </div>
</div>