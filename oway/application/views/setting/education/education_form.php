<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="edu_form_wrapper">
<?=form_open("",' id="edu_form" ')?>
            <div class="row">
      <div class="col-md-7">
      <div class="form-group"> 
      <?=form_label('University')?>                       
            <?=form_input("university",set_value('university'),"placeholder='University'  id='university'   class='form-control' required")?>
      </div>
      </div>
      <div class="col-md-5">

      </div>
      </div>   
    <div class="row">
        <div class="col-md-2">
          <div class="form-group">
               <?=form_label('Start Date')?>                       
            <select name="grad_start_year" id="grad_start_year" class="form-control">
              <option value=""> Select Year</option>
              <?php
               $year = date('Y');
                for($i=$year; $i>1980; $i--):
               ?> 
                  <option value="<?=$i?>"  ><?=$i?></option>
              <?php
                endfor;
              ?>
            </select>          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
               <label class="labelless"> </label>                       
          <select name="grad_start_month" id="grad_start_month" class="form-control">
              <option value=""> Select Month</option>
              <?php
               $month = 1;
                for($i=$month; $i<=12; $i++):
               ?> 
                  <option value="<?=$i?>" ><?=name_of_month($i)?></option>
              <?php
                endfor;
              ?>
            </select>        
          </div>
        </div>
   
        <div class="col-md-2">
          <div class="form-group">
               <label>Graduation Date </label>                       
            <select name="grad_date" class="form-control" id="grad_date">
              <option value=""> Select Year</option>
              <?php
               $year = date('Y');
                for($i=$year; $i>1980; $i--):
               ?> 
                  <option value="<?=$i?>"  ><?=$i?></option>
              <?php
                endfor;
              ?>
            </select>          
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
               <label class="labelless"> </label>                       
          <select name="grad_month" id="grad_month" class="form-control">
              <option value=""> Select Month</option>
              <?php
               $month = 1;
                for($i=$month; $i<=12; $i++):
               ?> 
                  <option value="<?=$i?>" ><?=name_of_month($i)?></option>
              <?php
                endfor;
              ?>
            </select>        
          </div>
        </div>
    </div>   
      <div class="row">
            <div class="col-md-7" >
                    <div class="form-group">
      <?=form_label('Degree Level')?>                       


          <select name="degree_lvl" id="degree_lvl" data-placeholder="Degree Level" class="chosen-select" tabindex="2" required>
              <option value=""></option>
              <optgroup label="Ordinary Universities">
              <option value="1">Bachelor (B.Sc)</option>
                  <option value="2">Master (M.Sc)</option>
                  <option value="3">Bachelor (B.A)</option>
                  <option value="4">Master (M.A)</option>
                  <option value="5">Doctorate (Ph.D)</option>
              </optgroup>
              <optgroup label="Technological Universities">
                  <option value="6">A.G.T.I</option>
                  <option value="7">Bachelor (B.Tech)</option>
                  <option value="8">Bachelor (B.E)</option>
                  <option value="9">Master (M.E)</option>
                  <option value="10">Doctorate (Ph.D)</option>
              </optgroup>
              <optgroup label="Computer University">
                  <option value="11">Bachelor (B.C.Sc)   </option>
                  <option value="12">Master (M.C.Sc  )</option>
                  <option value="13">Bachelor (B.C.Tech)</option>
                  <option value="14">Master (M.C.Tech)</option>
                  <option value="15">Doctorate (Ph.D)</option>
              </optgroup>
              <optgroup label="University of Medicine">
                  <option value="16">Bachelor of Medicine & Surgery ( M.B.B.S )   </option>
                  <option value="17">Diploma (Dip.Med.Sc)</option>
                  <option value="18">Master (M.Med.Sc)</option>
                  <option value="19">Doctor (Dr.Med.Sc)</option>
              </optgroup>
              <optgroup label="Myanmar Maritime Univeristy">
                  <option value="20"> B.E  (Naval Architecture)</option>
                  <option value="21">B.E (marine Engineering) </option>
                  <option value="22">B.E (Port & Harbour) </option>
                  <option value="23">B.E (River & Costal) </option>
                  <option value="24">B.E (Marine Electrical & Electronic System)
                  </option>
                  <option value="25">B.E (Nautical Science)
                  </option>
                  <option value="26">B.E (Marine Mechnical)
                  </option>
              </optgroup>

              <optgroup label="Yezing Aricultural University">
                  <option value="27"> B.Agr.Sc</option>
                 </optgroup>
              <optgroup label="University of Nursing ">
                  <option value="28">Bachelor of Nursing science  (B.N.Sc )
                  </option>
                  <option value="29">Master of Nursing Science     (M.N.Sc)
                  </option>
              </optgroup>
              <option value="30">Other</option>

              </select>
        </div>
             </div>


      <div class="col-md-5">

      </div>
      </div>

    <div class="row" id="otheruni" style="display: none;">
        <div class="col-md-7" >
            <div class="form-group">
                <?=form_label('Degree Level')?>
                <?=form_input("otherdegree_lvl",set_value('degree_lvl'),"placeholder='Degree Level'  id='otherdegree_lvl' class='form-control' required")?>
            </div>
        </div>
    </div>

      <div class="row">
      <div class="col-md-7">
      <div class="form-group"> 
      <?=form_label('Subject/Major')?>                       
      <?=form_input("subject_m",set_value('subject_m'),"placeholder='Subject/Major'  id='subject_m' class='form-control' required")?>
      </div>
      </div> 
        <div class="col-md-5">

      </div>
    </div>
      <div class="row">
        <div class="col-md-5">
        <div class="form-group"> 
        <?=form_label('Country')?> 
                <select name="country" id="country_edu" data-placeholder="Country" class="chosen-select" tabindex="2" required>
              <option value=""></option>
              <?php
             
               $query=$this->db->get('country_tbl')->result_array();
               foreach ($query as $key => $val) :
                ?>
              <option value="<?=$val['t_id']?>"><?=$val['d_name']?></option>
            <?php endforeach;?>
            </select>
        </div>
        </div>      
    </div>  
      <input type="hidden" id="url_show" value='<?=base_url()?>admin/exp_show'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('edu_section','<?=base_url()?>admin/edu_show','education_form_con','edu_show')">Cancel</a>
          <?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='education_submit'  url='".base_url()."admin/education_insert'  ")?>
      </div>

<?=form_close();?>
</div>
