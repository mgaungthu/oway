<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="edu_show_wrapper">
<?php 
  $this->db->order_by('grad_start_year','DESC');
  $cv_id = urldecode($this->uri->segment(3));
  $this->db->where('cv_id',$cv_id);
  $query=$this->db->get('education_tbl')->result_array();
  foreach ($query as $key => $value) :
?>
<div class="cv-section">
  <div id="edu-<?=$value['t_id']?>" >
<ul class="list-inline pull-right">
                        <li>
                            <a defaultdegree="<?=$value['degree_lvl']?>" class="edit-icon" id="edit_edu" name="<?=$value['t_id']?>">
                                <i class="fa fa-lg fa-edit" ></i>
                            </a>
                        </li>
                        <li>
                            <a id="confirm_modal" onClick="delete_dynamic('<?=$value['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/edu_show','education_tbl','#edu-<?=$value['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                <i class="fa fa-lg fa-trash"></i>
                            </a>
                            
                            <input type="hidden" id="edit_edu_url" value="<?=base_url()?>admin/edit_edu">
                        </li>
</ul>
<dl>
                        <dt class="job-title"><?php if($value['degree_lvl']!=30){echo $this->main_model->degree_lvl($value['degree_lvl']);}else{echo $value['otherdegree_lvl'];} ?>, <?=$value['subject_m']?></dt>
                        <dd class="job-bind">
                            <?=$value['university']?>,
                            <?=$this->main_model->country($value['country'])?>
                        </dd>
                        <dd class="job-bind">
                            <?php if($value['grad_start_month']!=0):?>
                            <?=name_of_month($value['grad_start_month'])?>,
                            <?php endif;?>
                            <?php if($value['grad_start_year']!=0 ):?>
                                <?=$value['grad_start_year']?>
                            <?php endif;?>
                            <?php if($value['grad_date']!=0 || $value['grad_month'] !=0 ):?>
                                -
                            <?php endif;?>
                            <span class="job-bind" >
                              <?php


                              if($value['grad_month']!=0) {
                                  echo name_of_month($value['grad_month']).', ';
                              }
                              if($value['grad_date']!=0)
                              {
                                  echo $value['grad_date'];
                              }
                                ?>
                            </span>
                          
                        </dd>
</dl>

</div>
</div>
<?php endforeach;?>


</div>