    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $this->db->where('cv_id',$cv_id);
    $per=$this->db->get('c_information_tbl')->row_array();

    ?>
    <div style="text-align: center;border-bottom: 2px solid #7dc24b;margin-bottom: 3px;"><img src="images/Oway-Logo.png" width="200px"/></div>



                <div class="box">
                    <h3>
                        <?=$per['first_name']?> <?=$per['last_name']?>

                    </h3>
                    <p class="text-body text-sm text-muted space-top-none">
                        Ref: <?=$per['cv_id']?>
                    </p>
                </div>
                <div class="box">

                </div>

                <div class="box" style="padding-top: 30px">

                    <div class="pull-right text-right text-sm space-top-xs">
                        <span class="text-muted">Mobile:</span>
                        <?=$per['c_number']?><br>
                        <a href="mailto:a.baby.of.blackgang@gmail.com"><?=$per['email']?><br></a>
                    </div>

                </div>

                <div class="box text-right" >
                <img src="uploads/profile/<?=$per['cv_pic']?>" width="220px" >
                </div>
    <div class="clearfix"></div>
    <h3>Summary</h3>
    <div class="line"></div>

    <div class="middle-box">

        <ul class="list-symbol-bullet space-bottom-none">
            <li>
                <div class="row">
                    <div class="smallbox">Profile:</div>
                    <div class="smallbox">
                        <?=gender($per['gender'])?>,
                        <?=age($per['date_of_birth'])?>,
                        <?=marital($per['marital'])?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Nationality:</div>
                    <div class="smallbox">
                        <?=$per['nationality'];?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Current Location:</div>
                    <div class="smallbox">
                        <?=$this->main_model->country($per['cur_location'])?>,

                    </div>
                </div>
            </li>
        </ul>
    </div>


    <div class="last-child">

        <ul class="list-symbol-bullet space-bottom-none">


            <li>
                <div class="row">
                    <div class="smallbox">Current Position:</div>
                    <div class="smallbox">
                        <?=$per['cur_post'];?>

                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Company:</div>
                    <div class="smallbox">
                        <?=$per['cur_company'];?>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="smallbox">Salary Expectations:</div>
                    <div class="smallbox">
                        <?=$this->main_model->salary_exp($per['salary_exp']);?>
                    </div>
                </div>
            </li>

        </ul>
    </div>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM working_exp_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>Work Experience</h3>
    <div class="line"></div>

    <?php

    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('working_exp_tbl')->result_array();
    foreach ($query as $key => $exp) :
    ?>
        <div class="boxwrapper">
        <div class="boxex1">
            <dd class="job-bind">
                <?php if($exp['start_month']!=0):?>
                    <?=name_of_month($exp['start_month'])?>,
                <?php endif ; ?>
                <?php if($exp['start_year']!=0):?>
                    <?=$exp['start_year']?>
                <?php endif;?>

                <span class="job-bind" >
                                                        <?php
                                                        if ($exp['end_year']==1) {
                                                            echo "- present";
                                                        }
                                                        else
                                                        {
                                                            if($exp['end_month']!=0 || $exp['end_year']!=0 )
                                                            {
                                                                echo '- ' ;
                                                            }
                                                            if($exp['end_month']!=0) {
                                                                echo name_of_month($exp['end_month']).', ';
                                                            }
                                                            if($exp['end_year']!=0)
                                                            {
                                                                echo $exp['end_year'];
                                                            }

                                                        }
                                                        ?>

                </span>
            </dd>
        </div>

        <div class="boxex2">
                <div class="innerbox1">
                    <h5><?=$exp['job_title']?></h5>
                    <p><?=$exp['company']?></p>
                </div>
            <div class="innerbox2">
                <h5><?=$exp['city']?>, <?=$this->main_model->country($exp['country'])?> </h5>
            </div>
            <div class="clearfix"></div>
            <div class="fullbox">
            <p>
                <?=$exp['description']?>
            </p>
            </div>
        </div>
        </div>
    <?php
    endforeach;
    endif;
    ?>
    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM education_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>Education</h3>
    <div class="line"></div>

    <?php

    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('education_tbl')->result_array();
    foreach ($query as $key => $edu) :
        ?>
    <div class="boxwrapper">
        <div class="boxex1">
            <dd class="job-bind">
                <?php if($edu['grad_start_month']!=0):?>
                    <?=name_of_month($edu['grad_start_month'])?>,
                <?php endif ; ?>
                <?php if($edu['grad_start_year']!=0):?>
                    <?=$edu['grad_start_year']?>
                <?php endif;?>

                <span class="job-bind" >
                                                    <?php
                                                    if ($edu['grad_date']==1) {
                                                        echo "- present";
                                                    }
                                                    else
                                                    {
                                                        if($edu['grad_month']!=0 || $edu['grad_date']!=0 )
                                                        {
                                                            echo '- ' ;
                                                        }
                                                        if($edu['grad_month']!=0) {
                                                            echo name_of_month($edu['grad_month']).', ';
                                                        }
                                                        if($edu['grad_date']!=0)
                                                        {
                                                            echo $edu['grad_date'];
                                                        }

                                                    }

                                                    ?>
                     </span>
            </dd>

        </div>
        <div class="boxex2">
            <div class="innerbox1">
                <h5><?=$edu['university']?></h5>
            </div>
            <div class="innerbox2">
                <h5> <?=$this->main_model->country($exp['country'])?>  </h5>
            </div>
            <div class="clearfix"></div>
            <div class="fullbox">
                <?=$edu['subject_m']?>, <?=degree_lvl($edu['degree_lvl'])?>
            </div>
        </div>
    </div>
    <?php
    endforeach;
    endif;
    ?>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM certi_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>Other Certificates</h3>
    <div class="line"></div>

    <?php

    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('certi_tbl')->result_array();
    foreach ($query as $key => $certi) :
        ?>
        <div class="boxwrapper">
            <div class="boxex1">
                <dd class="job-bind">
                    <?php if($certi['certi_month']!=0):?>
                        <?=name_of_month($certi['certi_month'])?>,
                    <?php endif ; ?>
                    <?php if($certi['certi_year']!=0):?>
                        <?=$certi['certi_year']?>
                    <?php endif;?>


                </dd>
            </div>
            <div class="boxex2">
                <div class="innerbox1">
                    <h5></h5>
                </div>
                <div class="innerbox2">
                    <h5><?=$certi['certi']?>, <?=$this->main_model->country($certi['certi_country'])?>  </h5>
                </div>

            </div>
        </div>
    <?php
    endforeach;
    endif;
    ?>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM it_skill_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>

    <h3>IT Skills</h3>
    <div class="line"></div>
    <?php

    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('it_skill_tbl')->result_array();
    foreach ($query as $key => $it) :
    ?>
    <div class="last-child">
        <h5> <?=$it['topic_app']?></h5>
        <ul class="list-symbol-bullet space-bottom-none">
            <li>
                <div class="row">
                    <div class="smallbox"> <?=it_type($it['it_type'])?>:</div>
                    <div class="smallbox">

                        <?=it_skill_lvl($it['it_skill_lvl'])?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <?php
    endforeach;
    endif;
    ?>

    <?php
    $count=$this->db->query("SELECT COUNT(cv_id) as count FROM lang_tbl where cv_id = '$cv_id'  ")->row_array();
    if($count['count']!=0):
    ?>
    <h3>Languages</h3>
    <div class="line"></div>

        <div class="last-child">

            <ul class="list-symbol-bullet space-bottom-none">
        <?php

        $this->db->where('cv_id',$cv_id);
        $query=$this->db->get('lang_tbl')->result_array();
        foreach ($query as $key => $lang) :
            ?>
                <li>
                    <div class="row">
                        <div class="smallbox"> <?=$this->main_model->lang($lang['lang'])?>:</div>
                        <div class="smallbox">
                            <?=lang_skill($lang['lang_skill'])?>
                        </div>
                    </div>
                </li>
            <?php
        endforeach;
        endif;
        ?>
            </ul>
        </div>


    <?php

    $this->db->where('cv_id',$cv_id);
    $query=$this->db->get('file_of_cv')->result_array();
    foreach ($query as $key => $file) :
    ?>
    <div style="padding: 10px">
    <img src="uploads/<?=$file['file_name']?>"/>
    </div>
    <?php endforeach;?>

