<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->db->where('cv_id',$cv_id);
$per=$this->db->get('c_information_tbl')->row_array();
?>

<input type="hidden" value="<?=base_url()?>admin/profile_upload" id="profileUploadUrl"/>
<div id="crop-avatar">
    <div id="cvphoto" class="avatar-view" title="Upload Cv photo">
        <img src="uploads/profile/<?=$per['cv_pic']?>" alt="Cv Photo">
    </div>

    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="avatar-form" action="admin/uploadpf" enctype="multipart/form-data" method="post">
                    <input readonly placeholder="Applicant No" type="hidden" name="photocv" id="cv_id"  value="<?=$cv_id?>" class="form-control" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="avatar-body">

                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                                <input type="hidden" class="avatar-src" name="avatar_src">
                                <input type="hidden" class="avatar-data" name="avatar_data">
                                <label for="avatarInput">Local upload</label>
                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                            </div>

                            <!-- Crop and preview -->
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="avatar-wrapper"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-preview preview-lg"></div>
                                    <div class="avatar-preview preview-md"></div>
                                    <div class="avatar-preview preview-sm"></div>
                                </div>
                            </div>

                            <div class="row avatar-btns">
                                <div class="col-md-9">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button id="profileUpload" type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </form>
            </div>
        </div>
    </div><!-- /.modal -->
</div>


<section class="row">
    <div id="personal_section" class="col-md-9">
        <!-- <div class="container" > -->
        <?=form_open("admin/personal-information-added",' id="c_info_form" ')?>


        <div class="col-md-4">
            <div class="creat-block">
                <span id="app_no"><input readonly placeholder="Applicant No" type="text" name="app_no" id="cv_id" value="<?=$cv_id?>" class="form-control" /></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="creat-block">
                <select  name="target_location"  id="target_location" data-placeholder="Location" class="chosen-select" tabindex="2">
                    <option value=""></option>
                    <option value="1" <?php if (1== $target_location) { echo "selected"; }?>>Local</option>
                    <option value="2" <?php if (2== $target_location) { echo "selected"; }?>>Oversea</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="creat-block">
                <input type="hidden" id="url" value="<?=base_url()?>admin/app_no"/>
                <input readonly type="text" value="<?=$job_cate?> - <?=$this->main_model->job_des($job_cate)?>" class="form-control">
                <!--<select name="job_cate" data-placeholder="Job Category " id="cat_code" class="chosen-select" tabindex="2">-->
                <!--              <option value=""></option>-->
                <!--              --><?php
                //              $this->db->order_by('cat_code','ASC');
                //               $query=$this->db->get('job_category_tbl')->result_array();
                //               foreach ($query as $key => $val) :
                //                ?>
                <!--              <option value="--><?//=$val['cat_code']?><!--" --><?php //if ($val['cat_code']== $job_cate) { echo "selected"; }?><!-- >--><?//=$val['cat_code']?><!-- - --><?//=$val['description']?><!--</option>-->
                <!--            --><?php //endforeach;?>
                <!--</select>-->
            </div>
        </div>

        <fieldset class="col-md-12 midtopmargin" id="form1" style="display:none;">

            <legend>Contact Information</legend>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Primary Phone No')?>
                        <?=form_input("contact_no",$per["c_number"],"placeholder='Phone No' type='number' rangelength='[6,12]' data-rule-required='true'  id='contact_no' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Secondary Phone No')?>
                        <?=form_input("sec_contact_no",$per["sec_contact_no"],"placeholder='Phone No' type='number' rangelength='[6,12]'   id='sec_contact_no' class='form-control'")?>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <?=form_label('Email')?>
                        <?=form_input("email",$per["email"],"placeholder='Email' id='email' class='form-control' data-rule-required='true' data-rule-email='true'  ")?>
                    </div>
                </div>
                <input type="hidden" id="url2" value="<?=base_url()?>admin/show"/>
                <input type="hidden" id="cv_id" value="<?=$cv_id?>"/>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?=form_label('Address')?>
                        <?=form_textarea("address",$per["address"],"placeholder='Address' id='address' class='form-control'   ")?>
                    </div>
                </div>
                <div>
                    <div class="modal-footer">

                        <?=form_submit("Save","Save","class='btn ft-btn btn-primary' id='con_de'  ")?>
                    </div>
        </fieldset>
        <?=form_close();?>
        <!--  Preview -->
        <div id="testshow" style="display:block;">

            <div class="col-md-12 midtopmargin">

                <legend>Contact Information</legend>
                <div class="edit-button text-right blacktext">
                    <a  id="edit_contact"><i class="fa fa-pencil-square-o"></i></a>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-block">
                            <span class="title" > Contact Number </span> : <span  id="contactshow"><?=$per["c_number"]?></span>,
                            <span  id="sec_contact_no"><?=$per["sec_contact_no"]?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-block">
                            <span class="title" > Email Address </span> : <span  id="emailshow"><?=$per["email"]?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <div class="contact-block">
                            <div class="col-md-1 nopadding">
                                <span class="title" > Address </span> :
                            </div>
                            <div class="col-md-11 nopadding">
                                <span  id="addshow"><?=$per["address"]?></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <!-- </div> -->

        <?=form_open("",' id="p_info_form" ')?>
        <fieldset class="col-md-12 midtopmargin" id="form2" style="display:none;">
            <legend>Personal Information</legend>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('First Name')?>
                        <?=form_input("first_name",$per["first_name"],"placeholder='First Name'  id='first_name'  data-rule-required='true' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Last Name')?>
                        <?=form_input("last_name",$per["last_name"],"placeholder='Last Name'  id='last_name'  data-rule-required='true' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-6">

                </div>
                <input type="hidden" id="form2url" value="<?=base_url()?>admin/show2"/>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Nrc Number')?>
                        <?=form_input("nrc_no",$per["nrc_no"],"placeholder='Nrc Number' data-rule-required='true' id='nrc_no' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Religion')?>
                        <?=form_input("religion",$per["religion"],"placeholder='Religion' data-rule-required='true' id='religion' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <?=form_label('Gender')?>
                    <div class="form-group">
                        <?php
                        $options = array(
                            ''       => 'Choose One',
                            '1'      => 'Male',
                            '2'      => 'Female',

                        );
                        ?>
                        <?=form_dropdown('gender', $options,$per["gender"],'class="form-control" id="gender" required')?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Date of Birth')?>
                        <?=form_input("date_of_birth",sim_date($per["date_of_birth"]),"placeholder='Date of Birth' id='date_of_birth' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Father Name')?>
                        <?=form_input("dad_name",$per["dad_name"],"placeholder='Father Name' id='dad_name' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-6">
                    <?=form_label('Marital Status')?>
                    <div class="form-group">
                        <div class="form-group">
                            <?php
                            $options = array(
                                ''       => 'Choose One',
                                '1'      => 'Single',
                                '2'      => 'Married',
                                '3'      => 'Divorced',
                                '4'      => 'Widowed',
                                '5'      => 'Cohabiting',

                            );
                            ?>
                            <?=form_dropdown('marital', $options,$per["marital"],'class="form-control" id="marital" required')?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Weight')?>
                        <?=form_input("weight",$per["weight"],"placeholder='Weight' id='weight' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Height')?>
                        <?=form_input("height",$per["height"],"placeholder='Height' id='height' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Nationality')?>
                        <?=form_input("nationality",$per["nationality"],"placeholder='Nationality' id='nationality' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?=form_label('Current Location')?>
                        <select name="cur_location" id="cur_location" data-placeholder="Current Location" class="chosen-select" tabindex="2">
                            <option value=""></option>
                            <?php

                            $query=$this->db->get('country_tbl')->result_array();
                            foreach ($query as $key => $val) :
                                ?>
                                <option value="<?=$val['t_id']?>" <?php if ($val['t_id']== $per["cur_location"]) { echo "selected"; }?> ><?=$val['d_name']?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?=form_label('Current Position')?>
                        <?=form_input("cur_post",$per["cur_post"],"placeholder='Current Position' id='cur_post' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?=form_label('Current Company')?>
                        <?=form_input("cur_company",$per["cur_company"],"placeholder='Current Company' id='cur_company' class='form-control'")?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?=form_label('Salary Expectations')?>
                        <?=form_input("salary_exp",$per["salary_exp"],"placeholder='Salary Expectations' id='salary_exp' class='currency form-control'")?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?=form_label('Noticed Period')?>
                        <?=form_input("not_period",'',"placeholder='Noticed Period' id='not_period' class='form-control'")?>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <?=form_submit("Save","Save","class='btn sec-btn btn-primary' id='per_info' ")?>
            </div>
        </fieldset>
        <?=form_close();?>



        <!--  personalpreview  -->
        <div id="personalpreview" style="display:block;">

            <div class="col-md-12 midtopmargin">

                <legend>Personal Information</legend>
                <div class="edit-button text-right blacktext">
                    <a  id="edit_personal"><i class="fa fa-pencil-square-o"></i></a>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="contact-block">
                            <span class="title" > Name </span> : <span  id="first_name"><?=$per["first_name"]?></span> <span  id="last_name"><?=$per["last_name"]?></span>
                        </div>
                    </div>

                    <div class="col-md-8">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" >
                        <div class="contact-block">
                            <span class="title" > Nrc No </span> : <span id="nrc_no"><?=$per["nrc_no"]?></span>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="contact-block">
                            <span class="title" > Religion </span> : <span id="religion"><?=$per["religion"]?></span>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="contact-block">
                            <span class="title" > Gender </span> : <span id="gender"><?=gender($per["gender"])?></span>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="contact-block">

                            <span class="title" > Age </span> : <span id="date_of_birth"><?=age($per["date_of_birth"])?></span>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="contact-block">
                            <span class="title" >Father Name</span> : <span id="dad_name"><?=$per["dad_name"]?></span>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="contact-block">
                            <span class="title" >  Marital Status </span> : <span id="marital"><?=marital($per["marital"])?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="contact-block">
                            <span class="title" >  Weight </span> : <span id="weight"><?=$per["weight"]?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="contact-block">
                            <span class="title" >  Height </span> : <span id="height"><?=$per["height"]?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="contact-block">
                            <span class="title" >  Nationality </span> : <span id="nationality"><?=$per["nationality"]?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="contact-block">
                            <span class="title" >  Current Location </span> : <span id="cur_location">
               <?php if(!empty($per['cur_location'])):?>
               <?=$this->main_model->country($per["cur_location"])?></span>
                            <?php endif;?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="contact-block">
                            <span class="title" >  Current Position </span> : <span id="cur_post"><?=$per["cur_post"]?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-block">
                            <span class="title" >  Current Company </span> : <span id="cur_company"><?=$per["cur_company"]?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-block">
                            <span class="title" >  Salary Expectations </span> :
                  <span class="cur" id="salary_exp">

                          <?=$per["salary_exp"]?>

                  </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="contact-block">
                            <span class="title" >  Noticed Period </span> : <span id="not_period"><?=$per["not_period"]?></span>
                        </div>
                    </div>

                </div>

            </div>


        </div>

    </div>


    <!--CV STRENGHT-->
    <div class="col-md-3">
        <div class="box">
            <div class="box-header">
                <div class="progress-group">
                    <span class="progress-text text-red">weak</span>
                    <span class="progress-number text-blue">strong</span>
                    <div class="progress lg">
                        <div class="progress-bar progress-bar-aqua" style="width:<?=$this->main_model->cv_strenght($cv_id)?>%"></div>
                    </div>
                </div>
            </div>
            <div class="box-body">

                <a  class="btn btn-primary" href="admin/print-setting/<?=urlencode(encryptor('encrypt',$per['t_id']))?>" ><i class="fa fa-print"></i> Print</a>
                <a  class="btn btn-primary"><i class="fa fa-users"></i> Interview</a>
            </div>
        </div>


    </div>

    <div class="col-md-9 midtopmargin" id="career_section" >
        <!--/// Career Objective-->
        <fieldset class="col-md-12 midtopmargin" id="carrer_form"  style="display:none;">
            <input type="hidden" id="objurl" value="<?=base_url()?>admin/career_update"/>

            <legend>Career Objective</legend>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?=form_textarea("c_objective",$per["c_objective"],"placeholder='Career Objective' id='c_objective' class='form-control'   ")?>
                    </div>
                </div>
                <div>
                    <div class="modal-footer">
                        <?=form_submit("Save","Save","class='btn ft-btn btn-primary' id='career_obj' ")?>
                    </div>

                    <?=form_close();?>
        </fieldset>



        <div id="career_wrap" >

            <div class="col-md-12 midtopmargin">

                <legend>Career Objective</legend>
                <div class="edit-button text-right blacktext">
                    <a  id="edit_obj"><i class="fa fa-pencil-square-o"></i></a>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <div class="contact-block">
                            <div class="col-md-12 nopadding">
                                <span  id="cb_show"><?=$per["c_objective"]?></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>



    <div class="col-md-9 midtopmargin" id="post_section">
        <input type="hidden" id="post_url" value="<?=base_url()?>admin/post_show">
        <legend><i class="fa fa-fw fa-dot-circle-o"></i> Apply Position</legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/post_form','post_section','post_show','post_form_con')" ><i class="fa fa-plus"></i></a>
        </div>
        <div id="post_show">
            <div id="post_show_wrapper">
                <?php
                $this->db->order_by('t_id','ASC');
                $this->db->where('cv_id',$cv_id);
                $queryp=$this->db->get('app_pos_tbl')->result_array();
                foreach ($queryp as $key => $post) :
                    ?>
                    <div class="cv-section">
                        <div id="post-<?=$post['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a  class="edit-icon" id="edit_post" name="<?=$post['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="delete_dynamic('<?=$post['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/post_show','app_pos_tbl','#post-<?=$post['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>

                                    <input type="hidden" id="edit_post_url" value="<?=base_url()?>admin/edit_post">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?=$this->main_model->job_des_id($post['job_cate'])?>, <?=$this->main_model->job_industry($post['job_industry'])?></dt>
                                <dd class="job-bind">
                                    <?=$post['apply_post']?>
                                </dd>
                                <dd class="job-bind">
                                    <?=$this->main_model->job_type($post['job_type'])?>
                                </dd>
                            </dl>

                            <!-- <div id="loading" style="display:none;">
                                  <h1>Loading</h1>
                            </div> -->
                        </div>
                    </div>
                <?php endforeach;?>



            </div>
        </div>
        <div id="post_form_con">

        </div>


    </div>


    <!-- ????????????? EXP SECTION ???????????? -->

    <div class="col-md-9 midtopmargin" id="exp_section">

        <legend><i class="fa fa-fw fa-briefcase"></i> Working Experience</legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" url="<?=base_url()?>admin/work_exp"><i class="fa fa-plus"></i></a>
        </div>
        <div id="exp_show">
            <div id="exp_show_wrapper">
                <?php
                $this->db->order_by("start_year","DESC");
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('working_exp_tbl')->result_array();
                foreach ($query as $key => $exp) :
                    ?>
                    <div class="cv-section">
                        <div id="exp-<?=$exp['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a class="edit-icon" id="edit_exp" name="<?=$exp['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="del_individual('<?=$exp['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>
                                    <input type="hidden" id="del_url" value="<?=base_url()?>admin/delete_exp">
                                    <input type="hidden" id="show" value="<?=base_url()?>admin/exp_show">
                                    <input type="hidden" id="edit_url" value="<?=base_url()?>admin/edit_exp">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?=$exp['job_title']?>, <?=$this->main_model->job_industry($exp['exp_industry'])?></dt>
                                <dd class="job-bind">
                                    <?=$this->main_model->job_des_id($exp['exp_cat'])?>, <?=$exp['company']?>,
                                    <?=$exp['city']?>,
                                    <?=$this->main_model->country($exp['country'])?>
                                </dd>
                                <dd class="job-bind">
                                    <?php if($exp['start_month']!=0):?>
                                        <?=name_of_month($exp['start_month'])?>,
                                    <?php endif ; ?>
                                    <?php if($exp['start_year']!=0):?>
                                        <?=$exp['start_year']?>
                                    <?php endif;?>

                                    <span class="job-bind" >
                                                <?php
                                                if ($exp['end_year']==1) {
                                                    echo "- present";
                                                }
                                                else
                                                {
                                                    if($exp['end_month']!=0 || $exp['end_year']!=0 )
                                                    {
                                                        echo '- ' ;
                                                    }
                                                    if($exp['end_month']!=0) {
                                                        echo name_of_month($exp['end_month']).', ';
                                                    }
                                                    if($exp['end_year']!=0)
                                                    {
                                                        echo $exp['end_year'];
                                                    }

                                                }
                                                ?>
                                              </span>

                                </dd>
                            </dl>
                            <p class="exp-text">
                                <?=$exp['description']?>
                            </p>
                            <!-- <div id="loading" style="display:none;">
                                  <h1>Loading</h1>
                            </div> -->
                        </div>
                    </div>
                <?php endforeach;?>


            </div>
        </div>
        <div id="working_exp_form">

        </div>


    </div>

    <!-- ????????????? Education SECTION ???????????? -->

    <div class="col-md-9 midtopmargin" id="edu_section">
        <legend>
            <i class="fa fa-fw fa-graduation-cap"></i>   Educational Background
        </legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/education_form','edu_section','edu_show','education_form_con')" ><i class="fa fa-plus"></i></a>
            <input type="hidden" id="edu_url" value="<?=base_url()?>admin/edu_show">
        </div>
        <div id="edu_show">
            <div id="edu_show_wrapper">
                <?php
                $this->db->order_by('grad_start_year','DESC');
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('education_tbl')->result_array();
                foreach ($query as $key => $edu) :
                    ?>
                    <div class="cv-section">
                        <div id="edu-<?=$edu['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a defaultdegree="<?=$edu['degree_lvl']?>" class="edit-icon" id="edit_edu" name="<?=$edu['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="delete_dynamic('<?=$edu['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/edu_show','education_tbl','#edu-<?=$edu['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>

                                    <input type="hidden" id="edit_edu_url" value="<?=base_url()?>admin/edit_edu">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?php if($edu['degree_lvl']!=30){echo $this->main_model->degree_lvl($edu['degree_lvl']);}else{echo $edu['otherdegree_lvl'];} ?>, <?=$edu['subject_m']?></dt>
                                <dd class="job-bind">
                                    <?=$edu['university']?>,
                                    <?=$this->main_model->country($edu['country'])?>
                                </dd>
                                <dd class="job-bind">
                                    <?php if($edu['grad_start_month']!=0):?>
                                        <?=name_of_month($edu['grad_start_month'])?>,
                                    <?php endif;?>
                                    <?php if($edu['grad_start_year']!=0 ):?>
                                        <?=$edu['grad_start_year']?>
                                    <?php endif;?>
                                    <?php if($edu['grad_date']!=0 || $edu['grad_month'] !=0 ):?>
                                        -
                                    <?php endif;?>
                                    <span class="job-bind" >
                                                  <?php


                                                  if($edu['grad_month']!=0) {
                                                      echo name_of_month($edu['grad_month']).', ';
                                                  }
                                                  if($edu['grad_date']!=0)
                                                  {
                                                      echo $edu['grad_date'];
                                                  }
                                                  ?>
                                                </span>

                                </dd>
                            </dl>

                        </div>
                    </div>
                <?php endforeach;?>


            </div>
        </div>
        <div id="education_form_con">

        </div>


    </div>

    <!-- ????????????? Cerificate SECTION ???????????? -->

    <div class="col-md-9 midtopmargin" id="certi_section">
        <legend>
            <i class="fa fa-fw fa-certificate"></i>   Other Qualifications
        </legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/certi_form','certi_section','certi_show','certi_form_con')" ><i class="fa fa-plus"></i></a>
            <input type="hidden" id="certi_url" value="<?=base_url()?>admin/certi_show">
        </div>
        <div id="certi_show">

            <div id="certi_show_wrapper">
                <?php
                $this->db->order_by('certi_year','DESC');
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('certi_tbl')->result_array();
                foreach ($query as $key => $cer) :
                    ?>
                    <div class="cv-section">
                        <div id="certi-<?=$cer['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a class="edit-icon" id="edit_certi" name="<?=$cer['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="delete_dynamic('<?=$cer['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/certi_show','certi_tbl','#certi-<?=$cer['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>

                                    <input type="hidden" id="edit_certi_url" value="<?=base_url()?>admin/edit_certi">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?=degree_lvl($cer['certi'])?></dt>
                                <dd class="job-bind">
                                    <?=$cer['t_center']?>, <?=$this->main_model->country($cer['certi_country'])?>
                                </dd>
                                <dd class="job-bind">
                                    <?=name_of_month($cer['certi_month'])?>,
                                    <?=$cer['certi_year']?>
                                </dd>

                            </dl>

                        </div>
                    </div>
                <?php endforeach;?>


            </div>
        </div>
        <div id="certi_form_con">

        </div>
    </div>

    <!-- ????????????? IT-Still SECTION ???????????? -->

    <div class="col-md-9 midtopmargin" id="it_section">
        <legend>
            <i class="fa fa-fw fa-laptop"></i>  IT Skills
        </legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/it_form','it_section','it_show','it_form_con')" ><i class="fa fa-plus"></i></a>
            <input type="hidden" id="it_url" value="<?=base_url()?>admin/it_show">
        </div>
        <div id="it_show">
            <div id="it_show_wrapper">
                <?php
                $this->db->order_by('t_id','DESC');
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('it_skill_tbl')->result_array();
                foreach ($query as $key => $it) :
                    ?>
                    <div class="cv-section">
                        <div id="it-<?=$it['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a class="edit-icon" id="edit_itskill" name="<?=$it['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="delete_dynamic('<?=$it['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/it_show','it_skill_tbl','#it-<?=$it['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>

                                    <input type="hidden" id="edit_it_url" value="<?=base_url()?>admin/edit_itskill">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?=$it['topic_app']?></dt>
                                <dd class="job-bind">
                                    <?=it_type($it['it_type'])?>,
                                </dd>
                                <dd class="job-bind">
                                    <?=it_skill_lvl($it['it_skill_lvl'])?>

                                    </span>

                                </dd>
                            </dl>

                            <!-- <div id="loading" style="display:none;">
                                  <h1>Loading</h1>
                            </div> -->
                        </div>
                    </div>
                <?php endforeach;?>



            </div>
        </div>
        <div id="it_form_con">

        </div>
    </div>

    <!-- ????????????? Language SECTION ???????????? -->

    <div class="col-md-9 midtopmargin" id="lang_section">
        <legend>
            <i class="fa fa-fw fa-language"></i>  Language Skills
        </legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/lang_form','lang_section','lang_show','lang_form_con')" ><i class="fa fa-plus"></i></a>
            <input type="hidden" id="lang_url" value="<?=base_url()?>admin/lang_show">
        </div>
        <div id="lang_show">
            <div id="lang_show_wrapper">
                <?php
                $this->db->order_by('t_id','DESC');
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('lang_tbl')->result_array();
                foreach ($query as $key => $lge) :
                    ?>
                    <div class="cv-section">
                        <div id="lang-<?=$lge['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a class="edit-icon" id="edit_langskill" name="<?=$lge['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="delete_dynamic('<?=$lge['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/lang_show','lang_tbl','#lang-<?=$lge['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>

                                    <input type="hidden" id="edit_lang_url" value="<?=base_url()?>admin/edit_lang">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?=$this->main_model->lang($lge['lang'])?></dt>
                                <dd class="job-bind">
                                    <?=lang_skill($lge['lang_skill'])?>
                                </dd>
                            </dl>

                            <!-- <div id="loading" style="display:none;">
                                  <h1>Loading</h1>
                            </div> -->
                        </div>
                    </div>
                <?php endforeach;?>



            </div>
        </div>
        <div id="lang_form_con">

        </div>
    </div>
    <!-- ????????????? REF SECTION ???????????? -->
    <div class="col-md-9 midtopmargin" id="refperson_section">
        <legend>
            <i class="fa fa-fw  fa-users"></i>  Reference Person
        </legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new" onClick="addnew('<?=base_url()?>admin/refperson_form','refperson_section','refperson_show','refperson_form_con')" ><i class="fa fa-plus"></i></a>
            <input type="hidden" id="ref_url" value="<?=base_url()?>admin/refperson_show">
        </div>
        <div id="refperson_show">
            <div id="refperson_show_wrapper">
                <?php
                $this->db->order_by('t_id','DESC');
                $this->db->where('cv_id',$cv_id);
                $query=$this->db->get('ref_person_tbl')->result_array();
                foreach ($query as $key => $ref) :
                    ?>
                    <div class="cv-section">
                        <div id="ref-<?=$ref['t_id']?>" >
                            <ul class="list-inline pull-right">
                                <li>
                                    <a class="edit-icon" id="edit_ref" name="<?=$ref['t_id']?>">
                                        <i class="fa fa-lg fa-edit" ></i>
                                    </a>
                                </li>
                                <li>
                                    <a id="confirm_modal" onClick="delete_dynamic('<?=$ref['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/refperson_show','ref_person_tbl','#ref-<?=$ref['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                        <i class="fa fa-lg fa-trash"></i>
                                    </a>

                                    <input type="hidden" id="edit_ref_url" value="<?=base_url()?>admin/edit_refperson">
                                </li>
                            </ul>
                            <dl>
                                <dt class="job-title"><?=$ref['ref_name']?>, <?=$ref['ref_employer']?>, <?=$ref['ref_occupation']?></dt>
                                <dd class="job-bind">
                                    <?=$ref['ref_ph_no']?>, <?=$ref['ref_email']?>
                                </dd>


                                </dd>
                            </dl>

                            <!-- <div id="loading" style="display:none;">
                                  <h1>Loading</h1>
                            </div> -->
                        </div>
                    </div>
                <?php endforeach;?>



            </div>
        </div>
        <div id="refperson_form_con">

        </div>
    </div>

    <div class="col-md-9 midtopmargin" id="lang_section">
        <legend>
            <i class="fa fa-fw fa-upload"></i> Attachment
        </legend>
        <div class="edit-button text-right blacktext">
            <a  id="add_new_img"  ><i class="fa fa-plus"></i></a>

        </div>

        <?=form_open_multipart('admin/editimg/'.$this->uri->segment(3))?>
        <span id="app_no2">
            <input type="hidden" placeholder="Applicant No"  name="img_id" id="cv_id" value="<?=$cv_id?>" class="form-control" />
        </span>
        <div id="img_field_wrap">
            <?php
            $this->db->where('cv_id',$cv_id);
            $query=$this->db->get("file_of_cv")->result_array();
            foreach ($query as $key => $row) :
                ?>

                <div style="min-height: 360px;" class="preview<?=$row["t_id"]?>1231 col-md-3 countdiv">
                    <input readonly value="<?=$row["title"]?>" name="editfile[]" type="text" placeholder="Name" class="form-control" style="margin-bottom: 5px;">
                    <img width="175px" height="130px" id="preview<?=$row["t_id"]?>1231" src="uploads/<?=$row["file_name"]?>" class="img-responsive">
                    <div class="fileUpload btn btn-info" style="display:none;">
                        <span>Upload</span>
                        <input value="<?=$row["file_name"]?>" name="editfile[]" class="upload" id="userfile" onchange="imguploaddy(this,<?=$row["t_id"]?>1231);" multiple="" type="file">
                    </div>
                    <input id="removebtn" tid="<?=$row["t_id"]?>" imgname="<?=$row["file_name"]?>" url="<?=base_url()?>admin/deleimg"  value="Remove" class="btn btn-danger" type="submit">

                </div>

                <?php
            endforeach;
            ?>

        </div>

    </div>
    <div class="col-md-9 midtopmargin" id="lang_section">
        <legend>
            <i class="fa fa-fw fa-file-word-o"></i> Cover Letter
        </legend>
        <div class="col-md-4">
            <div class="form-group">
                <label>File input</label>
                <input name="coverletter"  type="file" value="">
                <p class="help-block"><?=$per["cover_letter"]?></p>
            </div>

        </div>
<a href="admin" style="margin-left:5px;" class="btn btn-success pull-right bigtopmargin" >Finish</a> 
<?=form_submit('Upload All','Upload All','class="btn btn-primary pull-right bigtopmargin" id="uploadbtn" ')?>
    <?=form_close()?>

    </div>

</section>

<div class="col-md-12 midtopmargin" >

    
</div>





