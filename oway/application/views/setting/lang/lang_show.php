<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="lang_show_wrapper">
<?php 
  $cv_id = urldecode($this->uri->segment(3));
  $this->db->order_by('t_id','DESC');
  $this->db->where('cv_id',$cv_id);
  $query=$this->db->get('lang_tbl')->result_array();
  foreach ($query as $key => $value) :
?>
<div class="cv-section">
  <div id="lang-<?=$value['t_id']?>" >
<ul class="list-inline pull-right">
                        <li>
                            <a class="edit-icon" id="edit_langskill" name="<?=$value['t_id']?>">
                                <i class="fa fa-lg fa-edit" ></i>
                            </a>
                        </li>
                        <li>
                            <a id="confirm_modal" onClick="delete_dynamic('<?=$value['t_id']?>','<?=base_url()?>admin/delete_edu','<?=base_url()?>admin/lang_show','lang_tbl','#lang-<?=$value['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                <i class="fa fa-lg fa-trash"></i>
                            </a>
                            
                            <input type="hidden" id="edit_lang_url" value="<?=base_url()?>admin/edit_lang">
                        </li>
</ul>
<dl>
                        <dt class="job-title"><?=$this->main_model->lang($value['lang'])?></dt>
                        <dd class="job-bind">
                            <?=lang_skill($value['lang_skill'])?>
                        </dd>                        
</dl>

<!-- <div id="loading" style="display:none;">
      <h1>Loading</h1>
</div> -->
</div>
</div>
<?php endforeach;?>



</div>