<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="lang_form_wrapper">
  <ul class="list-unstyled lang-scope">
                <li><strong>Basic</strong> - I can have a simple conversation and understand the written language</li>
                <li><strong>Conversational</strong> - I can use the language for Conversational, read documents and be interviewed</li>
                <li><strong>Fluent</strong> - I can read, write and speak fluently in this language without any mistakes</li>
                <li><strong>Native</strong> - Mother tongue</li>
            </ul>

  <?php 
  $id = urldecode($this->uri->segment(3));
  $this->db->where('t_id',$id);
  $col=$this->db->get('lang_tbl')->row_array();
?>
<?=form_open("",' id="lang_form" ')?>


      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Language')?>
          <select name="lang" id="lang" data-placeholder="Language" class="chosen-select" tabindex="2">
              <option value=""></option>
              <?php

              $query=$this->db->get('all_lang_tbl')->result_array();
              foreach ($query as $key => $val) :
                  ?>
                  <option value="<?=$val['t_id']?>" <?php if($col['lang']==$val['t_id']){echo 'selected';} ?>><?=$val['lang_name']?></option>
              <?php endforeach;?>
          </select>
        </div>
      </div>

      </div>

      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Skill Level ')?>                       
                 <?php
                    $options = array(
                                         ''       => 'Select',
                                         '1'      => 'Basic',
                                         '2'      => 'Conversational',
                                         '3'      => 'Fluent',
                                         '4'      => 'Native',
                                                                     
                                         );  
                          ?>
          <?=form_dropdown('lang_skill', $options,$col['lang_skill'],'class="form-control" id="lang_skill" ')?>      
        </div>
      </div>

      </div>
      <input type="hidden" id="lang_id" value='<?=$col['t_id']?>'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('lang_section','<?=base_url()?>admin/lang_show','lang_form_con','lang_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='lang_edit_submit'  url='".base_url()."admin/lang_edit'  ")?>
      </div>

<?=form_close();?>
</div>
