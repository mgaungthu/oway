<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="exp_show_wrapper">
<?php 
  $cv_id = urldecode($this->uri->segment(3));
   $this->db->order_by("start_year","DESC");
  $this->db->where('cv_id',$cv_id);
  $query=$this->db->get('working_exp_tbl')->result_array();
  foreach ($query as $key => $value) :
?>
<div class="cv-section">
  <div id="exp-<?=$value['t_id']?>" >
<ul class="list-inline pull-right">
                        <li>
                            <a class="edit-icon" id="edit_exp" name="<?=$value['t_id']?>">
                                <i class="fa fa-lg fa-edit" ></i>
                            </a>
                        </li>
                        <li>
                            <a id="confirm_modal" onClick="del_individual('<?=$value['t_id']?>')"  data-toggle="modal" data-target=".confirm" >
                                <i class="fa fa-lg fa-trash"></i>
                            </a>
                            <input type="hidden" id="del_url" value="<?=base_url()?>admin/delete_exp">
                            <input type="hidden" id="show" value="<?=base_url()?>admin/exp_show">
                            <input type="hidden" id="edit_url" value="<?=base_url()?>admin/edit_exp">
                        </li>
</ul>
<dl>
                        <dt class="job-title"><?=$value['job_title']?>, <?=$this->main_model->job_industry($value['exp_industry'])?> </dt>
                        <dd class="job-bind">
                            <?=$this->main_model->job_des_id($value['exp_cat'])?>, <?=$value['company']?>,
                            <?=$value['city']?>,
                            <?=$this->main_model->country($value['country'])?>
                        </dd>
                        <dd class="job-bind">
                            <?php if($value['start_month']!=0):?>
                            <?=name_of_month($value['start_month'])?>,
                            <?php endif ; ?>
                            <?php if($value['start_year']!=0):?>
                            <?=$value['start_year']?>
                            <?php endif;?>
                            <span class="job-bind" >
                              <?php
                              if ($value['end_year']==1) {
                                  echo "- present";
                              }
                              else
                              {
                                  if($value['end_month']!=0 || $value['end_year']!=0 )
                                  {
                                      echo '- ' ;
                                  }
                                  if($value['end_month']!=0) {
                                      echo name_of_month($value['end_month']).', ';
                                  }
                                  if($value['end_year']!=0)
                                  {
                                      echo $value['end_year'];
                                  }

                              }
                              ?>
                            </span>
                          
                        </dd>
</dl>
<p class="exp-text">
<?=$value['description']?>
</p>
<!-- <div id="loading" style="display:none;">
      <h1>Loading</h1>
</div> -->
</div>
</div>
<?php endforeach;?>


</div>