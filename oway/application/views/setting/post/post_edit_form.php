<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$id = urldecode($this->uri->segment(3));
$this->db->where('t_id',$id);
$col=$this->db->get('app_pos_tbl')->row_array();
?>
<div id="post_form_wrapper">

<?=form_open("",' id="post_form" ')?>


      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Job Category')?>
          <select id="app_job" name="job_cate" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
              <option value=""></option>
              <?php
              $this->db->order_by('cat_code','ASC');
              $query=$this->db->get('job_category_tbl')->result_array();
              foreach ($query as $key => $val) :
                  ?>
                  <option value="<?=$val['t_id']?>" <?php if($col['job_cate']==$val['t_id']){echo 'selected';} ?> ><?=$val['cat_code']?> - <?=$val['description']?></option>
              <?php endforeach;?>
          </select>
        </div>
      </div>
      </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Job Industry')?>

                <select name="app_industry" id="app_industry" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                    <option value=""></option>
                    <?php
                    $this->db->order_by('description','DESC');
                    $ind=$this->db->get('industry_tbl')->result_array();
                    foreach ($ind as $key => $val) :
                        ?>
                        <option value="<?=$val['t_id']?>" <?php if($col['job_industry']==$val['t_id']){echo 'selected';} ?> ><?=$val['description']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Apply Position')?>

                <?=form_input("apply_post",$col['apply_post'],"placeholder='Apply Post'  id='apply_post'   class='form-control' ")?>

            </div>
        </div>

    </div>

      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Job Type')?>
          <select id="job_type" name="job_type" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
              <option value=""></option>
              <?php
              $this->db->order_by('t_id','ASC');
              $query=$this->db->get('job_type_tbl')->result_array();
              foreach ($query as $key => $val) :
              ?>
                  <option value="<?=$val['t_id']?>" <?php if($col['job_type']==$val['t_id']){echo 'selected';} ?> ><?=$val['job_type']?> </option>
              <?php endforeach;?>
          </select>

        </div>
      </div>

      </div>
    <input type="hidden" id="post_id" value='<?=$col['t_id']?>'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('post_section','<?=base_url()?>admin/post_show','post_form_con','post_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='post_edit_submit'  url='".base_url()."admin/post_update'  ")?>
      </div>

<?=form_close();?>
</div>
