<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="post_form_wrapper">

<?=form_open("",' id="post_form" ')?>


      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Job Category')?>

          <select id="app_job" name="job_cate" data-placeholder="Choose one" class="chosen-select" tabindex="2" aria-required="true" required>
              <option value=""></option>
              <?php
              $this->db->order_by('cat_code','ASC');
              $query=$this->db->get('job_category_tbl')->result_array();
              foreach ($query as $key => $val) :
                  ?>
                  <option value="<?=$val['t_id']?>"><?=$val['cat_code']?> - <?=$val['description']?></option>
              <?php endforeach;?>
          </select>
        </div>
      </div>
      </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Job Industry')?>

                <select name="app_industry" id="app_industry" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                    <option value=""></option>
                    <?php
                    $this->db->order_by('description','DESC');
                    $ind=$this->db->get('industry_tbl')->result_array();
                    foreach ($ind as $key => $val) :
                        ?>
                        <option value="<?=$val['t_id']?>"><?=$val['description']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Apply Position')?>

                <?=form_input("apply_post",set_value('app_post'),"placeholder='Apply Post'  id='apply_post'   class='form-control' required")?>

            </div>
        </div>

    </div>

      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Job Type')?>
          <select id="job_type" name="job_type" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
              <option value=""></option>
              <?php
              $this->db->order_by('t_id','ASC');
              $query=$this->db->get('job_type_tbl')->result_array();
              foreach ($query as $key => $val) :
                  ?>
                  <option value="<?=$val['t_id']?>"><?=$val['job_type']?> </option>
              <?php endforeach;?>
          </select>

        </div>
      </div>

      </div>
      <input type="hidden" id="post_url" value='<?=base_url()?>admin/post_show'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('post_section','<?=base_url()?>admin/post_show','post_form_con','post_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='post_submit'  url='".base_url()."admin/post_insert'  ")?>
      </div>

<?=form_close();?>
</div>
