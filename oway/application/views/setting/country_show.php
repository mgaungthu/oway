<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <div class="box">
<div class="table-responsive bigsidepadding smallpadding" >
       <table class="table table-bordered table-hover" id="sorting">
        <thead>
          <tr>
            <th>
              No
            </th>
           
            <th>
              Country Name
            </th>
            <th>
              Created Date
            </th>
            <th>
              Who Created
            </th>
            <th>
              Edit
            </th>
            <th>
              Delete
            </th>
          </tr>
        </thead>
        <tbody>
        <?php 
            $id=1;
            foreach ($query as $key => $row) :
        ?>
        <tr>
            <td>
              <?=$id++?>
            </td>
            <td>
              <?=$row['d_name']?>
            </td>
            <td>
             <?=date_time($row['creat_date'])?>
            </td>
            <td>
              <?=$row['creator_name']?>
            </td>
            <td>
            <?=anchor('admin/country-name-edit/'.$row['t_id'],'Edit')?>
            </td>
            <td>
              <?=anchor('admin/delete-country/'.$row['t_id'],'Delete')?>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
      </table>
  </div>
     <div class="box-footer clearfix">
         <a href="admin/country-add-new" class="btn btn-sm btn-info btn-flat pull-left">Creat New Country</a>

     </div>
 </div>
