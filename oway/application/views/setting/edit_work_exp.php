<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $id = urldecode($this->uri->segment(3));
    $this->db->where('t_id',$id);
    $col=$this->db->get('working_exp_tbl')->row_array();

?>
<div id="exp_form_wrapper">
<?=form_open("",' id="edit_exp" ')?>
<input type="hidden" value="<?=$id?>" id="t_id"/>
    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <?=form_label('Job Category')?>

                <select id="exp_cat" name="exp_cat" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                    <option value=""></option>
                    <?php
                    $this->db->order_by('cat_code','ASC');
                    $query=$this->db->get('job_category_tbl')->result_array();
                    foreach ($query as $key => $val) :
                        ?>
                        <option value="<?=$val['t_id']?>" <?php if($val['t_id']==$col['exp_cat']){echo "selected";}?> ><?=$val['cat_code']?> - <?=$val['description']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <?=form_label('Job Industry')?>

                <select name="exp_industry" id="exp_industry" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                    <option value=""></option>
                    <?php
                    $this->db->order_by('description','DESC');
                    $ind=$this->db->get('industry_tbl')->result_array();
                    foreach ($ind as $key => $val) :
                        ?>
                        <option value="<?=$val['t_id']?>" <?php if($val['t_id']==$col['exp_industry']){echo "selected";}?> ><?=$val['description']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>

            <div class="row">
      <div class="col-md-7">
      <div class="form-group"> 
      <?=form_label('Company')?>                       
            <?=form_input("company",$col['company'],"placeholder='Company'  id='company'   class='form-control' ")?>
      </div>
      </div>
      <div class="col-md-5">

      </div>
      </div>     
      <div class="row">
      <div class="col-md-7">
      <div class="form-group"> 
      <?=form_label('Job Title')?>                       
            <?=form_input("job_title",$col['job_title'],"placeholder='Job Title'  id='job_title'   class='form-control' ")?>
      </div>
      </div>
      <div class="col-md-5">

      </div>
      </div>
      <div class="row">
        <div class="col-md-5">
        <div class="form-group"> 
        <?=form_label('Country')?> 
                <select name="country" id="country" data-placeholder="Country" class="chosen-select" tabindex="2">
              <option value=""></option>
              <?php
             
               $query=$this->db->get('country_tbl')->result_array();
               foreach ($query as $key => $val) :
                ?>
              <option value="<?=$val['t_id']?>" <?php if($val['t_id']==$col['country']){echo "selected";}?> ><?=$val['d_name']?></option>
            <?php endforeach;?>
            </select>
        </div>
        </div>      
    </div>  

      <div class="row">
      <div class="col-md-7">
      <div class="form-group"> 
      <?=form_label('City')?>                       
      <?=form_input("city",$col['city'],"placeholder='City'  id='city' class='form-control'")?>
      </div>
      </div> 
        <div class="col-md-5">

      </div>
    </div>


    <div class="row">
        <div class="col-md-2">
          <div class="form-group">
               <?=form_label('Time Period')?>                       
            <select name="start_year" id="start_year" class="form-control">
              <option value=""> Select Year</option>
              <?php
               $year = date('Y');
                for($i=$year; $i>1980; $i--):
               ?> 
                  <option value="<?=$i?>"  <?php if($i==$col['start_year']){echo "selected";}?> ><?=$i?></option>
              <?php
                endfor;
              ?>
            </select>          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
               <label class="labelless"> </label>                       
          <select name="start_month" id="start_month" class="form-control">
              <option value=""> Select Month</option>
              <?php
               $month = 1;
                for($i=$month; $i<=12; $i++):
               ?> 
                  <option value="<?=$i?>" <?php if($i==$col['start_month']){echo "selected";}?> ><?=name_of_month($i)?></option>
              <?php
                endfor;
              ?>
            </select>        
          </div>
        </div>
        <div class="col-md-1">
         <span class="sperator">To</span>          
        </div>
        <div class="col-md-2">
          <div class="form-group">
               <label class="labelless"> </label>                       
            <select name="end_year" class="form-control" id="present">
              <option value=""> Select Year</option>
              <option value="1" <?php if(1==$col['end_year']){echo "selected";}?> > Present</option>
              <?php
               $year = date('Y');
                for($i=$year; $i>1980; $i--):
               ?> 
                  <option value="<?=$i?>" <?php if($i==$col['end_year']){echo "selected";}?> ><?=$i?></option>
              <?php
                endfor;
              ?>
            </select>          
          </div>
        </div>
        <div class="col-md-2" id="individual" style="<?php if(1==$col['end_year']){echo "display:none;";}?>">
          <div class="form-group">
               <label class="labelless"> </label>                       
          <select name="end_month" id="end_month" class="form-control">
              <option value=""> Select Month</option>
              <?php
               $month = 1;
                for($i=$month; $i<=12; $i++):
               ?> 
                  <option value="<?=$i?>" <?php if($i==$col['end_month']){echo "selected";}?> ><?=name_of_month($i)?></option>
              <?php
                endfor;
              ?>
            </select>        
          </div>
        </div>
    </div> 
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
             <?=form_label('Description')?>                       
        <?=form_textarea("description",$col['description'],"placeholder='Description' id='description' class='form-control'")?>
        </div>
      </div>
    </div> 
   
      <input type="hidden" id="url_show" value='<?=base_url()?>admin/exp_show'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' id="cancel_exp">Cancel</a> <?=form_submit("Save","Save","class='btn exp-btn btn-primary' id='edit_exp_submit'  url='".base_url()."admin/work_exp_edit'  ")?>
      </div>

<?=form_close();?>
</div>
