<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="ref_form_wrapper">
<?=form_open("",' id="refperson_form" ')?>
      <div class="row">
      <div class="col-md-4">
      <div class="form-group"> 
      <?=form_label('Full Name')?>
            <?=form_input("ref_name",set_value('ref_name'),"placeholder='Full Name'  id='ref_name'   class='form-control' required")?>
      </div>
      </div>

      </div> 

      <div class="row">
      <div class="col-md-4">
      <div class="form-group">
          <?=form_label('Occupation')?>
          <?=form_input("ref_occupation",set_value('ref_occupation'),"placeholder='Occupation'  id='ref_occupation'   class='form-control' required")?>
        </div>
      </div>

      </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Employer')?>
                <?=form_input("ref_employer",set_value('ref_employer'),"placeholder='Employer'  id='ref_employer'   class='form-control' required")?>
            </div>
        </div>

    </div>

      <div class="row">
      <div class="col-md-4">
      <div class="form-group">
          <?=form_label('Mobile No')?>
          <?=form_input("ref_ph_no",set_value('ref_ph_no'),"placeholder='Mobile No'  id='ref_ph_no'   class='form-control' ")?>
        </div>
      </div>

      </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Email')?>
                <?=form_input("ref_email",set_value('ref_email'),"placeholder='Email'  id='ref_email'   class='form-control' ")?>
            </div>
        </div>

    </div>

    


      <input type="hidden" id="url_show" value='<?=base_url()?>admin/refperson_show'>
      <div class="modal-footer">                    
        <a class='btn btn-danger' onclick="cancel_btn('refperson_section','<?=base_url()?>admin/refperson_show','refperson_form_con','refperson_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='ref_submit'  url='".base_url()."admin/refperson_insert'  ")?>
      </div>

<?=form_close();?>
</div>
