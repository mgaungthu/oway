<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php 
  $id = urldecode($this->uri->segment(3));
  $this->db->where('t_id',$id);
  $col=$this->db->get('ref_person_tbl')->row_array();
?>
<div id="ref_form_wrapper">
    <?=form_open("",' id="refperson_form" ')?>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Full Name')?>
                <?=form_input("ref_name",$col['ref_name'],"placeholder='Full Name'  id='ref_name'   class='form-control' required")?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Occupation')?>
                <?=form_input("ref_occupation",$col['ref_occupation'],"placeholder='Occupation'  id='ref_occupation'   class='form-control' required")?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Employer')?>
                <?=form_input("ref_employer",$col['ref_employer'],"placeholder='Employer'  id='ref_employer'   class='form-control' required")?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Mobile No')?>
                <?=form_input("ref_ph_no",$col['ref_ph_no'],"placeholder='Mobile No'  id='ref_ph_no'   class='form-control' ")?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?=form_label('Email')?>
                <?=form_input("ref_email",$col['ref_email'],"placeholder='Email'  id='ref_email'   class='form-control' ")?>
            </div>
        </div>

    </div>




    <input type="hidden" id="ref_id" value='<?=$col['t_id']?>'>
    <div class="modal-footer">
        <a class='btn btn-danger' onclick="cancel_btn('refperson_section','<?=base_url()?>admin/refperson_show','refperson_form_con','refperson_show')">Cancel</a><?=form_submit("Save","Save","class='btn edu-btn btn-primary' id='ref_edit_submit'  url='".base_url()."admin/refperson_edit'  ")?>
    </div>

    <?=form_close();?>
</div>
