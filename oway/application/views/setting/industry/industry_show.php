
<div class="box">
<div class="table-responsive bigsidepadding smallpadding">
       <table class="table table-bordered table-hover" id="sorting">
        <thead>
          <tr>
            <th>
              No
            </th>

            <th>
              Description
            </th>
            <th>
              Created Date
            </th>
            <th>
              Who Created
            </th>
            <th>
              Edit
            </th>
            <th>
              Delete
            </th>
          </tr>
        </thead>
        <tbody>
        <?php 
            $id=1;
            foreach ($query as $key => $row) :
        ?>
        <tr>
            <td>
              <?=$id++?>
            </td>
            <td>
              <?=$row['description']?>
            </td>
            <td>
             <?=date_time($row['creat_date'])?>
            </td>
            <td>
              <?=$row['creator_name']?>
            </td>
            <td>
            <?=anchor('industry/edit-industry/'.$row['t_id'],'Edit')?>
            </td>
            <td>
              <?=anchor('industry/delete-industry/'.$row['t_id'],'Delete')?>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
      </table>
  </div>
    <div class="box-footer clearfix">
        <a href="industry/industry-setting" class="btn btn-sm btn-info btn-flat pull-left">Creat New Industry</a>

    </div>
</div>