<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Content-Type: text/html; charset=utf-8');
	class Main_model extends CI_Model{

		function __construct()
	    {
	        // Call the Model constructor
	        parent::__construct();
	        $this->form_validation->set_message ('required','*');
	        $this->form_validation->set_message ('numeric','*');
	        $this->form_validation->set_message ('matches','*');
	        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

	        function gender($value)
	        {
	        	$input = $value;
	        	$gender = array('Male','Female');
	        	$digit = array(1,2);
	        	return str_replace($digit,$gender,$input);
	        }

			 function marital($value)
		{
			$input = $value;
			$marital = array('Single','Married','Divorced','Widowed','Cohabiting');
			$digit = array(1,2,3,4,5);
			return str_replace($digit,$marital,$input);
		}

			function encryptor($action, $string) {
				$output = false;

				$encrypt_method = "AES-256-CBC";
				//pls set your unique hashing key
				$secret_key = 'muni';
				$secret_iv = 'muni123';

				// hash
				$key = hash('sha256', $secret_key);

				// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
				$iv = substr(hash('sha256', $secret_iv), 0, 16);

				//do the encyption given text/string/number
				if( $action == 'encrypt' ) {
					$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
					$output = base64_encode($output);
				}
				else if( $action == 'decrypt' ){
					//decrypt the given text/string/number
					$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
				}

				return $output;
			}



	       	function degree_lvl($value)
	        {
	        	$input = $value;
	        	$degree_lvl = array('Bachelor (BSc)','Master (MSc)','Bachelor (BA)','Master (MA)','Doctorate (PhD)','A.G.T.I','Bachelor (B.Tech)','Bachelor (B.E)','Master (M.E)'
				,'Doctorate (PhD)','Bachelor (B.C.Sc)','Master (M.C.Sc)','Bachelor (B.C.Tech)','Master (M.C.Tech)','Doctorate (PhD)','Bachelor of Medicine & Surgery ( M.B.B.S )',
					'Diploma (Dip.Med.Sc)','Master (M.Med.Sc)','Doctor (Dr.Med.Sc)','B.E  (Naval Architecture)','B.E (marine Engineering)','B.E (Port & Harbour)','>B.E (River & Costal)'
					,'B.E (Marine Electrical & Electronic System)','B.E (Nautical Science)','B.E (Marine Mechnical)',' B.Agr.Sc','Bachelor of Nursing science  (B.N.Sc )','Master of Nursing Science (M.N.Sc)'
				);
	        	$digit = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29);
	        	return str_replace($digit,$degree_lvl,$input);
	        }

	       	function it_type($value)
	        {
	        	$input = $value;
	        	$it_type = array('Windows & Office tools','Web programming & development','Non-web programming languages','Operating systems, Networking & Hardware');
	        	$digit = array(1,2,3,4,5);
	        	return str_replace($digit,$it_type,$input);
	        }


	       	function it_skill_lvl($value)
	        {
	        	$input = $value;
	        	$it_skill_lvl = array('Basic','Intermediate','Advanced','Expert');
	        	$digit = array(1,2,3,4,5);
	        	return str_replace($digit,$it_skill_lvl,$input);
	        }




	       	function lang_skill($value)
	        {
	        	$input = $value;
	        	$lang_skill = array('Basic','Conversational','Fluent','Native');
	        	$digit = array(1,2,3,4);
	        	return str_replace($digit,$lang_skill,$input);
	        }


	        function name_of_month($value)
	        {
	        	switch ($value)
	        	{
	        		case 1:
	        			return 'January';
	        			break;
	        		case 2:
	        			return 'February';
	        			break;
	        		case 3:
	        			return 'March';
	        			break;
	        		case 4:
	        			return 'April';
	        			break;
	        		case 5:
	        			return 'May';
	        			break;
	        		case 6:
	        			return 'June';
	        			break;
	        		case 7:
	        			return 'July';
	        			break;
	        		case 8:
	        			return 'August';
	        			break;
	        		case 9:
	        			return 'September';
	        			break;
	        		case 10:
	        			return 'October';
	        			break;
	        		case 11:
	        			return 'November';
	        			break;
					case 12:
						return 'December';
						break;
	        		default:
	        			return '';
	        			break;
	        	}
	        }

	        function date_time($value)
	        {
	        	date_default_timezone_set("Asia/Rangoon");
	        	$date_time=date('d-m-Y H:i:s',$value);
	        	return $date_time;
	        }

	       	function sim_date($value)
	        {
	        	date_default_timezone_set("Asia/Rangoon");
	        	$date_time=date('d-m-Y',$value);
	        	return $date_time;
	        }

	        function age($val)
	        {
//	        	$birthDate = sim_date($val);
//              //explode the date to get month, day and year
//              $birthDate = explode("-", $birthDate);
//              //get age from date or birthdate
//              $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
//                ? ((date("Y") - $birthDate[2]) - 1)
//                : (date("Y") - $birthDate[2]));
				date_default_timezone_set("Asia/Rangoon");
				$date_time=date('d-m-Y',$val);
				return $date_time;

	        }

			function user_role($value)
			{
				$input = $value;
				$it_skill_lvl = array('Super Admin','Finance','Data Entry');
				$digit = array(1,2,3);
				return str_replace($digit,$it_skill_lvl,$input);
			}


	        
	    }

		public function degree_lvl($value)
		{
			$this->db->where('t_id',$value);
			$res=$this->db->get('university_tbl')->row_array();
			return $res['uni_name'];
		}

		public function salary_exp($value)
		{
			$this->db->where('t_id',$value);
			$val=$this->db->get('salary_exp_tbl')->row_array();
			return $val['salary_amount'] ;
		}

		public function lang($value)
	        {
	        	$this->db->where('t_id',$value);
				$row=$this->db->get('all_lang_tbl')->row_array();
	        	return $row['lang_name'];
	        }

		public function job_industry($value)
		{
			$this->db->where('t_id',$value);
			$row=$this->db->get('industry_tbl')->row_array();
			return $row['description'];
		}

	   

		public function loginValidate()
		{
			$this->form_validation->set_rules('loginName','','required');
			$this->form_validation->set_rules('loginPassword','','required');

			if ($this->form_validation->run()==FALSE)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public function gender($value)
	        {
	        	$input = $value;
	        	$gender = array('Male','Female');
	        	$digit = array(1,2);
	        	return str_replace($digit,$gender,$input);
	        }

	    public function marital($value)
	        {
	        	$input = $value;
	        	$marital = array('Single','Married','Divorced','Widowed','Cohabiting');
				$digit = array(1,2,3,4,5);
	        	return str_replace($digit,$marital,$input);
	        }

	    public function country($value)
	    {

	    	$this->db->where('t_id',$value);
	    	$val=$this->db->get('country_tbl')->row_array();
	    	return $val['d_name'];
	    }

		public function loginState($username,$password)
		{
			$result='';
			$query = $this->db->query('SELECT * FROM users_tbl');
			if (!$query) 
			{
				die ('Error:'.mysql_error());
				
			}
				else 
				{
					foreach ($query->result() as $row)
					{
						if ($row->username == $username)
						{
							if ($row->password == md5($password))
							{
								$this->session->set_userdata('login_state','true');
								$this->session->set_userdata('username',$username);
								$this->session->set_userdata('user_id',$row->user_id);
								$this->session->set_userdata('user_role',$row->user_role);
								$this->session->set_userdata('real_name',$row->first_name.' '.$row->last_name);
								$result = 'true';
								break;
							}
							else 
							{
									$result = 'false';
							}
						}
							else {
								$result = 'false';
							}
							
					}

					if ($result == 'true')
					{
						return true;
					}
						else 
						{
							return false;
						}
				} 

		}

		/**
		 * @param $cv_id
		 * @return int
         */
		public function cv_strenght($cv_id){
			$this->db->where('cv_id',$cv_id);
			$count=$this->db->get('c_information_tbl')->row_array();
			if(!empty($count['cv_id'])){
				$pinfo = 5;
			}
			else{
				$pinfo= 0;
			}
			$this->db->where('cv_id',$cv_id);
			$count=$this->db->get('working_exp_tbl')->row_array();
			if(!empty($count['cv_id']))
			{
				$exp=25;
			}
			else
			{
				$exp=0;
			}
			$this->db->where('cv_id',$cv_id);
			$count=$this->db->get('education_tbl')->row_array();
			if(!empty($count['cv_id']))
				{
					$edu=20;
				}
				else
				{
					$edu=0;
				}
			$this->db->where('cv_id',$cv_id);
			$count=$this->db->get('certi_tbl')->row_array();
			if(!empty($count['cv_id'])){
				$certi=20;
			}
			else {
				$certi = 0;
			}
			$this->db->where('cv_id',$cv_id);
			$count=$this->db->get('it_skill_tbl')->row_array();
			if(!empty($count['cv_id'])){
				$it=20;
			}
			else {
				$it = 0;
			}
			$this->db->where('cv_id',$cv_id);
			$count=$this->db->get('lang_tbl')->row_array();
			if(!empty($count['cv_id'])){
				$lang=10;
			}
			else {
				$lang = 0;
			}

				return $exp+$pinfo+$edu+$certi+$it+$lang;
		}

			public function job_des($value)
		{
			$this->db->where('cat_code',$value);
			$val=$this->db->get('job_category_tbl')->row_array();
			return $val['description'];
		}

		public function job_des_id($value)
		{
			$this->db->where('t_id',$value);
			$val=$this->db->get('job_category_tbl')->row_array();
			return $val['description'];
		}

		public function job_type($value)
		{
			$this->db->where('t_id',$value);
			$val=$this->db->get('job_type_tbl')->row_array();
			return $val['job_type'];
		}

		public function exp_search($value)
		{
			$val=$this->db->query(" SELECT * FROM working_exp_tbl WHERE work_exp LIKE '%$value%' ")->result_array();
			return $val['cv_id'];
		}



		// ------------------------ Upload Image Function --------------------------

	function upload_img($userfile,$attachName)
	{		

				$this->load->library('upload');
				$files = $_FILES;					
				if (!empty($_FILES[$userfile]['name'])) {
				$cpt = count($_FILES[$userfile]['name']);					
			    for($i=0; $i<$cpt; $i++)
			    {
					$rename=str_replace(' ','_',$attachName[$i]);
			        $_FILES['userfile']['name']= $rename.'.jpeg';
			        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
			        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
			        $_FILES['userfile']['size']= $files['userfile']['size'][$i];
					$title = $attachName[$i];
			        $data = array(
			        	'file_name' => $_FILES['userfile']['name'],
						'title'=>$title,
						'cv_id'=> $this->input->post('img_id')
                                    );  
			        $this->db->insert("file_of_cv",$data);
			        $this->upload->initialize($this->set_upload_options());
			        $this->upload->do_upload();	
			    }
			    }
	}


		private function set_upload_options()
		{   
		    //upload an image options
		    $config = array();		    
			$config['upload_path'] = 'uploads/';
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size'] = 5000;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$config['remove_spaces'] = 'true';
		    return $config;
		}

		public function upload_coverletter($userfile,$cv_id)
		{
			$file = $_FILES[$userfile]['name'];
			$config['upload_path'] = 'uploads/coverletters';
			$config['allowed_types'] = 'jpeg|jpg|pdf|doc|docx';
			$config['max_size'] = '5000KB';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ($this->upload->do_upload($userfile))
			{
				$file=str_replace(' ','_',$file);
				$this->db->where('cv_id',$cv_id);
				$this->db->update('c_information_tbl',array('cover_letter'=>$file));
			}
			else
			{
				return $this->upload->display_errors();
			}

		}












	}