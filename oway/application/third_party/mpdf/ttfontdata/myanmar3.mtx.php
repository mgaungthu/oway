<?php
$name='Myanmar3';
$type='TTF';
$desc=array (
  'CapHeight' => 736,
  'XHeight' => 500,
  'FontBBox' => '[-951 -414 2108 968]',
  'Flags' => 4,
  'Ascent' => 968,
  'Descent' => -414,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 404,
);
$unitsPerEm=900;
$up=-139;
$ut=56;
$strp=287;
$strs=54;
$ttffile='D:/XAMPP/htdocs/ERP/application/third_party/mpdf/ttfonts/myanmar3.ttf';
$TTCfontID='0';
$originalsize=122540;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='myanmar3';
$panose=' 0 0 2 2 6 3 5 4 5 2 3 4';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 889, -222, 0
// usWinAscent/usWinDescent = 1000, -444
// hhea Ascent/Descent/LineGap = 1000, -444, 100
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'mymr' => 'DFLT ',
);
$GSUBFeatures=array (
  'mymr' => 
  array (
    'DFLT' => 
    array (
      'clig' => 
      array (
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
        12 => 12,
        13 => 13,
        14 => 16,
        15 => 17,
        16 => 18,
        17 => 19,
        18 => 20,
        19 => 21,
        20 => 22,
        21 => 23,
        22 => 24,
        23 => 25,
        24 => 26,
        25 => 27,
        26 => 28,
        27 => 29,
        28 => 30,
        29 => 31,
        30 => 32,
        31 => 33,
        32 => 34,
        33 => 35,
        34 => 36,
        35 => 37,
        36 => 38,
        37 => 39,
        38 => 40,
        39 => 41,
        40 => 42,
        41 => 43,
        42 => 44,
        43 => 45,
        44 => 46,
        45 => 47,
        46 => 48,
        47 => 49,
        48 => 50,
        49 => 51,
        50 => 52,
        51 => 53,
        52 => 54,
        53 => 55,
        54 => 56,
      ),
      'liga' => 
      array (
        0 => 14,
        1 => 15,
        2 => 21,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 99550,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 99608,
      1 => 99626,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 99712,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 99788,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 99846,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 99922,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 99992,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100058,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100116,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100174,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100232,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100290,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100358,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100390,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100428,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100640,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 100920,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 100988,
      1 => 101008,
      2 => 101026,
      3 => 101046,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 101182,
      1 => 101200,
      2 => 101220,
      3 => 101240,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 101472,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 101514,
      1 => 101532,
      2 => 101552,
      3 => 101570,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 101672,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 101770,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 101822,
      1 => 101840,
      2 => 101860,
      3 => 101878,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 101988,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 102074,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 102160,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 102246,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 3,
    'Subtables' => 
    array (
      0 => 102324,
      1 => 102344,
      2 => 102364,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 102466,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 6,
    'Subtables' => 
    array (
      0 => 102544,
      1 => 102564,
      2 => 102584,
      3 => 102604,
      4 => 102626,
      5 => 102648,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 102806,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 102902,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 3,
    'Subtables' => 
    array (
      0 => 102990,
      1 => 103010,
      2 => 103032,
    ),
    'MarkFilteringSet' => '',
  ),
  34 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 103148,
    ),
    'MarkFilteringSet' => '',
  ),
  35 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 103218,
    ),
    'MarkFilteringSet' => '',
  ),
  36 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 103288,
      1 => 103308,
    ),
    'MarkFilteringSet' => '',
  ),
  37 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 103428,
      1 => 103450,
    ),
    'MarkFilteringSet' => '',
  ),
  38 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 103568,
      1 => 103588,
    ),
    'MarkFilteringSet' => '',
  ),
  39 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 103708,
    ),
    'MarkFilteringSet' => '',
  ),
  40 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 103778,
    ),
    'MarkFilteringSet' => '',
  ),
  41 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 103816,
    ),
    'MarkFilteringSet' => '',
  ),
  42 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 3,
    'Subtables' => 
    array (
      0 => 103904,
      1 => 103922,
      2 => 103942,
    ),
    'MarkFilteringSet' => '',
  ),
  43 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 8,
    'Subtables' => 
    array (
      0 => 104064,
      1 => 104086,
      2 => 104108,
      3 => 104132,
      4 => 104156,
      5 => 104180,
      6 => 104204,
      7 => 104230,
    ),
    'MarkFilteringSet' => '',
  ),
  44 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 104932,
    ),
    'MarkFilteringSet' => '',
  ),
  45 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 104978,
      1 => 105006,
    ),
    'MarkFilteringSet' => '',
  ),
  46 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 14,
    'Subtables' => 
    array (
      0 => 105218,
      1 => 105244,
      2 => 105270,
      3 => 105296,
      4 => 105322,
      5 => 105346,
      6 => 105370,
      7 => 105394,
      8 => 105418,
      9 => 105442,
      10 => 105466,
      11 => 105490,
      12 => 105514,
      13 => 105540,
    ),
    'MarkFilteringSet' => '',
  ),
  47 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 106740,
      1 => 106768,
    ),
    'MarkFilteringSet' => '',
  ),
  48 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 3,
    'Subtables' => 
    array (
      0 => 106978,
      1 => 107002,
      2 => 107026,
    ),
    'MarkFilteringSet' => '',
  ),
  49 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 18,
    'Subtables' => 
    array (
      0 => 107264,
      1 => 107286,
      2 => 107308,
      3 => 107330,
      4 => 107352,
      5 => 107376,
      6 => 107400,
      7 => 107422,
      8 => 107444,
      9 => 107466,
      10 => 107488,
      11 => 107512,
      12 => 107536,
      13 => 107560,
      14 => 107584,
      15 => 107608,
      16 => 107632,
      17 => 107654,
    ),
    'MarkFilteringSet' => '',
  ),
  50 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 109044,
      1 => 109070,
    ),
    'MarkFilteringSet' => '',
  ),
  51 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 14,
    'Subtables' => 
    array (
      0 => 109296,
      1 => 109318,
      2 => 109340,
      3 => 109362,
      4 => 109384,
      5 => 109406,
      6 => 109428,
      7 => 109454,
      8 => 109480,
      9 => 109504,
      10 => 109528,
      11 => 109552,
      12 => 109576,
      13 => 109600,
    ),
    'MarkFilteringSet' => '',
  ),
  52 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 110802,
      1 => 110820,
      2 => 110840,
      3 => 110860,
    ),
    'MarkFilteringSet' => '',
  ),
  53 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 111008,
    ),
    'MarkFilteringSet' => '',
  ),
  54 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 111902,
    ),
    'MarkFilteringSet' => '',
  ),
  55 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 111960,
      1 => 111978,
    ),
    'MarkFilteringSet' => '',
  ),
  56 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 112124,
      1 => 112142,
    ),
    'MarkFilteringSet' => '',
  ),
  57 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112234,
    ),
    'MarkFilteringSet' => '',
  ),
  58 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112248,
    ),
    'MarkFilteringSet' => '',
  ),
  59 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112262,
    ),
    'MarkFilteringSet' => '',
  ),
  60 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112276,
    ),
    'MarkFilteringSet' => '',
  ),
  61 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112290,
    ),
    'MarkFilteringSet' => '',
  ),
  62 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112304,
    ),
    'MarkFilteringSet' => '',
  ),
  63 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112318,
    ),
    'MarkFilteringSet' => '',
  ),
  64 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112336,
    ),
    'MarkFilteringSet' => '',
  ),
  65 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112350,
    ),
    'MarkFilteringSet' => '',
  ),
  66 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112364,
    ),
    'MarkFilteringSet' => '',
  ),
  67 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112378,
    ),
    'MarkFilteringSet' => '',
  ),
  68 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112392,
    ),
    'MarkFilteringSet' => '',
  ),
  69 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112430,
    ),
    'MarkFilteringSet' => '',
  ),
  70 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112444,
    ),
    'MarkFilteringSet' => '',
  ),
  71 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112466,
    ),
    'MarkFilteringSet' => '',
  ),
  72 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112554,
    ),
    'MarkFilteringSet' => '',
  ),
  73 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112572,
    ),
    'MarkFilteringSet' => '',
  ),
  74 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112590,
    ),
    'MarkFilteringSet' => '',
  ),
  75 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112608,
    ),
    'MarkFilteringSet' => '',
  ),
  76 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112626,
    ),
    'MarkFilteringSet' => '',
  ),
  77 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112640,
    ),
    'MarkFilteringSet' => '',
  ),
  78 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 112654,
    ),
    'MarkFilteringSet' => '',
  ),
  79 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 113234,
    ),
    'MarkFilteringSet' => '',
  ),
  80 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 113682,
    ),
    'MarkFilteringSet' => '',
  ),
  81 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 114130,
    ),
    'MarkFilteringSet' => '',
  ),
  82 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 114644,
    ),
    'MarkFilteringSet' => '',
  ),
  83 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 115026,
    ),
    'MarkFilteringSet' => '',
  ),
  84 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 115540,
    ),
    'MarkFilteringSet' => '',
  ),
  85 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 115922,
    ),
    'MarkFilteringSet' => '',
  ),
  86 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 116436,
    ),
    'MarkFilteringSet' => '',
  ),
  87 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 116818,
    ),
    'MarkFilteringSet' => '',
  ),
  88 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 117332,
    ),
    'MarkFilteringSet' => '',
  ),
  89 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 117714,
    ),
    'MarkFilteringSet' => '',
  ),
  90 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 118096,
    ),
    'MarkFilteringSet' => '',
  ),
  91 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 118610,
    ),
    'MarkFilteringSet' => '',
  ),
  92 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 118992,
    ),
    'MarkFilteringSet' => '',
  ),
  93 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 119374,
    ),
    'MarkFilteringSet' => '',
  ),
  94 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 119822,
    ),
    'MarkFilteringSet' => '',
  ),
  95 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 119844,
    ),
    'MarkFilteringSet' => '',
  ),
  96 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 119884,
    ),
    'MarkFilteringSet' => '',
  ),
  97 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 120506,
    ),
    'MarkFilteringSet' => '',
  ),
  98 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 120530,
    ),
    'MarkFilteringSet' => '',
  ),
  99 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 120544,
    ),
    'MarkFilteringSet' => '',
  ),
  100 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 120584,
    ),
    'MarkFilteringSet' => '',
  ),
  101 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 121496,
    ),
    'MarkFilteringSet' => '',
  ),
  102 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 121524,
    ),
    'MarkFilteringSet' => '',
  ),
  103 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 121620,
    ),
    'MarkFilteringSet' => '',
  ),
  104 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 122302,
    ),
    'MarkFilteringSet' => '',
  ),
  105 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 122342,
    ),
    'MarkFilteringSet' => '',
  ),
  106 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 122390,
    ),
    'MarkFilteringSet' => '',
  ),
  107 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 122418,
    ),
    'MarkFilteringSet' => '',
  ),
  108 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 122432,
    ),
    'MarkFilteringSet' => '',
  ),
  109 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 122446,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'mymr' => 'DFLT ',
);
$GPOSFeatures=array (
  'mymr' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
        2 => 2,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 97510,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 8,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 97620,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 97808,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 97846,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>