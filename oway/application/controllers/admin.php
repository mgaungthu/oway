<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Content-Type: text/html; charset=utf-8');


class Admin extends CI_Controller {

    function __construct()
    {             
        parent::__construct();
 		
     		if ($this->session->userdata('login_state') != 'true')
     		{
     			redirect('site');
     		}
        $this->load->library('encrypt');

    }

    public function test()
    {
        $ran= mt_rand(1, 1000);
        $ran2= mt_rand(1, 100);
        $before = str_pad('', 3, $ran, STR_PAD_LEFT);
        $after=str_pad('', 2, $ran2, STR_PAD_LEFT);
        $y=date('Y');
        $y=substr($y,-2);
        $m=date('m');
        $m=substr($m,-1);
        $count=3;
        $x = 1;
        $y = 2;

        function my_callback_function() {
            echo 'hello world!';
        }

        call_user_func('my_callback_function');
        

    }

      public function index()
     {	
         $data['query']=$this->db->query(" SELECT
        GROUP_CONCAT(DISTINCT exp.job_title  SEPARATOR ', ' ) as job_title,c_info.salary_exp,c_info.cv_id,c_info.first_name,c_info.last_name,c_info.c_number,c_info.email,c_info.creator_name,c_info.creat_date,c_info.description,c_info.t_id FROM c_information_tbl as c_info
        LEFT JOIN  working_exp_tbl AS exp ON c_info.cv_id = exp.cv_id LEFT  JOIN  app_pos_tbl AS pos ON c_info.cv_id = pos.cv_id
           GROUP BY exp.cv_id,c_info.cv_id,pos.cv_id ORDER BY c_info.creat_date DESC")->result_array();
         $this->breadcrumbs->push('Home','admin/index');
         $data['loginame']=$this->session->userdata('real_name');
         $data['pagetitle']='Overview';
         $data['sidebar_menu']='admin/sidebar_menu';
     	 $data['main_content']='admin/home';
         $this->load->view('admin/admin_template',$data);
      }
    public function search_cv()
    {
        $nrc_no=$this->input->post('nrc_no');
        $salary_exp=$this->input->post('salary_exp');
        $job_cate=$this->input->post('job_cate');
        $exp_skill=$this->input->post('exp_skill');
        $job_type=$this->input->post('job_type');
        $apply_post=$this->input->post('apply_post');
        $exp_industry=$this->input->post('exp_industry');
        $app_industry=$this->input->post('app_industry');
        // salary
        if($salary_exp==1){
            $start_sal = 0;
            $end_sal = 0;
        }
        else if ($salary_exp==2)
        {
            $start_sal = 0;
            $end_sal = 100000;
        }
        else if ($salary_exp==3)
        {
            $start_sal = 100000;
            $end_sal = 250000;
        }
        else if ($salary_exp==4)
        {
            $start_sal = 250000;
            $end_sal = 500000;
        }
        else if ($salary_exp==5)
        {
            $start_sal = 500000;
            $end_sal = 750000;
        }
        else if ($salary_exp==6)
        {
            $start_sal = 750000;
            $end_sal = 300000000;
        }
        else{
            $start_sal = 0;
            $end_sal = 300000000;
        }
        $data['query']=$this->db->query
        (" SELECT
        GROUP_CONCAT(DISTINCT exp.job_title  SEPARATOR ', ' ) as job_title,c_info.salary_exp,c_info.cv_id,c_info.first_name,c_info.last_name,c_info.c_number,c_info.email,c_info.creator_name,c_info.creat_date,c_info.description,c_info.t_id FROM c_information_tbl as c_info
        LEFT JOIN  working_exp_tbl AS exp ON c_info.cv_id = exp.cv_id LEFT JOIN  app_pos_tbl AS pos ON c_info.cv_id = pos.cv_id
          WHERE c_info.salary_exp BETWEEN '$start_sal' AND '$end_sal' AND pos.apply_post LIKE '%$apply_post%' AND c_info.cat_code LIKE '$job_cate%'  AND pos.job_type LIKE '$job_type%'
          AND pos.job_industry LIKE '$app_industry%' AND c_info.nrc_no LIKE '$nrc_no%'  GROUP BY exp.cv_id,c_info.cv_id,pos.cv_id ORDER BY c_info.creat_date DESC")->result_array();
        $exp_skill=$this->input->post('exp_skill');
        $job_title=$this->input->post('job_title');
        $start_exp =0;
        $end_exp = 100;
        if($exp_skill==1){
            $start_exp = 0;
            $end_exp = 1;
        }
        else if ($exp_skill==2)
        {
            $start_exp = 2;
            $end_exp = 3;
        }
        else if ($exp_skill==3)
        {
            $start_exp = 3;
            $end_exp = 5;
        }
        else if ($exp_skill==4)
        {
            $start_exp = 5;
            $end_exp = 8;
        }
        else if ($exp_skill==5)
        {
            $start_exp = 6;
            $end_exp = 10;
        }

        if(!empty($exp_skill) || !empty($job_title) || !empty($exp_industry) ) {
        $data['query'] = $this->db->query
        (" SELECT
        GROUP_CONCAT(DISTINCT exp.job_title  SEPARATOR ', ' ) as job_title,c_info.salary_exp,c_info.cv_id,c_info.first_name,c_info.last_name,c_info.c_number,c_info.email,c_info.creator_name,c_info.creat_date,c_info.description,c_info.t_id FROM c_information_tbl as c_info
        LEFT JOIN  working_exp_tbl AS exp ON c_info.cv_id = exp.cv_id LEFT JOIN  app_pos_tbl AS pos ON c_info.cv_id = pos.cv_id
          WHERE pos.job_industry LIKE '$app_industry%' AND pos.apply_post LIKE '%$apply_post%' AND c_info.cat_code LIKE '$job_cate%'  AND pos.job_type LIKE '$job_type%'
          AND exp.exp_industry LIKE '$exp_industry%' AND  exp.work_exp BETWEEN '$start_exp' AND '$end_exp'  AND  exp.job_title LIKE '%$job_title%'
          AND c_info.salary_exp BETWEEN '$start_sal' AND '$end_sal'AND c_info.nrc_no LIKE '$nrc_no%'  GROUP BY exp.cv_id,c_info.cv_id,pos.cv_id ORDER BY c_info.creat_date DESC")->result_array();

        }
        $data['nrc_no']=$nrc_no;
        $data['salary_exp']=$salary_exp;
        $data['exp_skill']=$exp_skill;
        $data['job_title']=$job_title;
        $data['job_cate']=$job_cate;
        $data['job_type']=$job_type;
        $data['apply_post']=$apply_post;
        $data['exp_industry']=$exp_industry;
        $data['app_industry']=$app_industry;
        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('Search Cv','admin/search-cv');
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Overview';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='admin/search_cv';
        $this->load->view('admin/admin_template',$data);
    }


    public function dashboard()
    {
        $data['query']=$this->db->get('c_information_tbl')->result_array();
        $this->breadcrumbs->push('Dashboard','admin/dashboard');
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Dashboard';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='admin/dashboard';
        $this->load->view('admin/admin_template',$data);
    }

    public function all_activity(){

        $this->db->where('user_id',$this->session->userdata('user_id'));
        $this->db->order_by('date_time','DESC');
        $data['query']=$this->db->get('activity_log_tbl')->result_array();
        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('All Activity','admin/all-activity');
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='All Activity';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='admin/all_activity';
        $this->load->view('admin/admin_template',$data);
    }



    public function print_setting(){
        $id=urldecode(encryptor('decrypt',$this->uri->segment(3)));
        $this->db->where('t_id',$id);
        $col = $this->db->get('c_information_tbl')->row_array();
        $cv_id=$col['cv_id'];
        $data['cv_id']=$cv_id;
        $data['target_location']=$col['target_location'];
        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('Print Setting','print-setting');
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Print Setting';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='admin/print_preview';
        $this->load->view('admin/admin_template',$data);

    }

    public function print_cv()
    {

        $id=urldecode(encryptor('decrypt',$this->uri->segment(3)));
        $email=$this->input->post('to_mail');
        $subject=$this->input->post('subject');
        $message=$this->input->post('body');
        $target_location=$this->input->post('target_location');
        $this->db->where('t_id',$id);
        $name=$this->db->get('c_information_tbl')->row_array();
        $data['c_info']=$this->input->post('c_info');
        $email_sit=$this->input->post('email_sit');
        $this->db->where('t_id',$id);
        $col = $this->db->get('c_information_tbl')->row_array();
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $cv_id=$col['cv_id'];
        $data['cv_id']=$cv_id;
        $data['job_cate']=$col['cat_code'];
        $data['target_location']=$target_location;
        $this->load->library('m_pdf');
        $pdf = $this->m_pdf->load('C');
        $stylesheet1 = $this->load->view('bootstrap.css','',true);
        $stylesheet2 = $this->load->view('stylesheet.css','',true);

        $html = $this->load->view('setting/print_cv',$data,true);
        $pdf->mPDF('C','A4-P','','',18,20,10,25,10,10);
        if($target_location==1){
            $pdf->SetWatermarkText('Oway');
        }

        $pdf->watermarkTextAlpha = 0.1;
        $pdf->showWatermarkText = true;
        $pdf->SetDisplayMode('fullpage');
        if($target_location==1) {
            $pdf->SetHTMLfooter('<div style="padding:5px;border-top: 1px solid #7dc24b;"><div><p>&copy; ' . Date('Y') . ' Oway Hr Solutions</p></div></div>');
        }
        $pdf->WriteHTML($stylesheet1,1);
        $pdf->WriteHTML($stylesheet2,1);
        $pdf->WriteHTML($html);
        if($email_sit==1) {

            $newFile  = FCPATH .'uploads/allpdf/'.$name["cv_id"].'-CV.pdf';
            $coverletter = FCPATH .'uploads/coverletters/'.$name["cover_letter"];
            $pdf->Output($newFile, 'F');
            $this->db->where('cv_id',$name["cv_id"]);
            $query=$this->db->get('file_of_cv')->result_array();
            foreach ($query as $key => $file) {
                $otherfile[] = FCPATH .'uploads/'.$file["file_name"];

            }
            if($query && $name["cover_letter"]) {
                array_unshift($otherfile,$newFile,$coverletter);

            }
            elseif($query){
                array_unshift($otherfile,$newFile);

            }
            elseif($name["cover_letter"]){
                $otherfile= array($newFile,$coverletter);

            }

        }
        else{
            $data = array
            (
                'user_id' => $this->session->userdata('user_id'),
                'username' => $this->session->userdata('username'),
                'real_name' => $this->session->userdata('real_name'),
                'About' => 'Application',
                'description' => 'Print Out',
                'date_time' => $time
            );
            $this->db->insert('activity_log_tbl',$data);
            $newFile  = $name["cv_id"].'-CV.pdf';
            $pdf->Output($newFile, 'I');
        }
    if($email_sit==1) {
            // email Section //
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.owayhrsolutions.com.mm',
            'smtp_port' => 465,
            'smtp_user' => 'cvs@owayhrsolutions.com.mm',
            'smtp_pass' => 'hrsolutions.com',
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('cvs@owayhrsolutions.com.mm');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if($query || $name["cover_letter"]) {
            foreach ($otherfile as $attachfile) {
                $this->email->attach($attachfile);
            }
        }
        else{
            $this->email->attach($newFile);
        }

        if($this->email->send())
        {
            $data = array
             (
                 'user_id' => $this->session->userdata('user_id'),
                 'username' => $this->session->userdata('username'),
                 'real_name' => $this->session->userdata('real_name'),
                 'About' => 'Application',
                 'description' => 'Sent Email',
                 'date_time' => $time
             );
             $this->db->insert('activity_log_tbl',$data);
           redirect('admin/print-setting/'.$this->uri->segment(3).'/complete');

        }
        else
        {
            redirect('admin/print-setting/'.$this->uri->segment(3).'/failed');
        }
    }

    }

    public function helpemail(){

        $bodyemail=$this->input->post('bodyemail');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.owayhrsolutions.com.mm',
            'smtp_port' => 465,
            'smtp_user' => 'cvs@owayhrsolutions.com.mm',
            'smtp_pass' => 'hrsolutions.com',
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('cvs@owayhrsolutions.com.mm');
        $this->email->to('mr.mgaungthu@gmail.com');
        $this->email->subject('Oway Help Mail -'. $this->session->userdata('real_name') );
        $this->email->message($bodyemail);
        $response=$this->email->send();
        echo json_encode($response);
    }

//    public function sendEmail()
//    {
//
//        $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'ssl://mail.invisiblestudio-mm.com',
//            'smtp_port' => 465,
//            'smtp_user' => 'info@invisiblestudio-mm.com',
//            'smtp_pass' => 'zwekoko88.com',
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//        );
//        $email="xyz@gmail.com";
//        $subject="some text";
//        $message="some text";
//
//
//        $this->load->library('email', $config);
//        $this->email->set_newline("\r\n");
//        $this->email->from('info@invisiblestudio-mm.com');
//        $this->email->to($email);
//        $this->email->subject($subject);
//        $this->email->message($message);
//        $this->email->attach('C:\Users\xyz\Desktop\images\abc.png');
//        if($this->email->send())
//        {
//            echo 'Email send.';
//        }
//        else
//        {
//            show_error($this->email->print_debugger());
//        }
//
//    }



    /**
     *
     */
    public function creat_cv()
     {
      $this->breadcrumbs->push('Home','admin/index');
     $this->breadcrumbs->push('Creat Cv','admin/creat-cv');
     $data['loginame']=$this->session->userdata('real_name');
     $data['pagetitle']='Creat Cv';
     $data['sidebar_menu']='admin/sidebar_menu';
     $data['main_content']='setting/creat_cv';
     $this->load->view('admin/admin_template',$data);
     }

    public function final_create()
     {
           $job_cate=$this->input->post('job_cate');
           $target_location=$this->input->post('target_location');
           $job_industry=$this->input->post('job_industry');
           $apply_post=$this->input->post('apply_post');
           $query = $this->db->query("SELECT DISTINCT Count(cat_code) AS count FROM  c_information_tbl WHERE cat_code = '$job_cate' ")->row_array();
           $count= $query['count'] + 1;
            $ran= mt_rand(1, 1000);
             $ran2= mt_rand(1, 100);
             $before = str_pad('', 3, $ran, STR_PAD_LEFT);
             $after=str_pad('', 2, $ran2, STR_PAD_LEFT);
             $y=date('Y');
             $y=substr($y,-2);
             $m=date('m');
             $m=substr($m,-1);
             $cv_id=$job_cate.'-'.$before.$y.$m.$after.'-'.$count;
           date_default_timezone_set("Asia/Rangoon");
           $time =  strtotime(date("Y-m-d H:i:s"));
           $this->db->where('cat_code',$job_cate);
           $res=$this->db->get('job_category_tbl')->row_array();
           $data = array
           (
            'cv_id' =>$cv_id ,
            'cat_code' =>$job_cate ,
            'target_location' =>$target_location ,
            'description'=>$res['description'],
            'user_id'=> $this->session->userdata('user_id'),
            'creator_name' => $this->session->userdata('real_name'),
            'creat_date' => $time
           );
           $this->db->insert('c_information_tbl',$data);
         $last_row = $this->db->insert_id();
            $this->db->where('cat_code',$job_cate);
            $jres=$this->db->get('job_category_tbl')->row_array();
           $this->db->insert('app_pos_tbl',array('cv_id'=>$cv_id,'job_type'=>3,'job_cate'=>$jres['t_id'],
               'job_industry'=>$job_industry,'apply_post'=>$apply_post));
           $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'),
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Application',
                      'description' => 'Add new',
                      'date_time' => $time
                  );
           $this->db->insert('activity_log_tbl',$data);
         redirect('admin/edit-cv/'.urldecode(encryptor('encrypt',$last_row)));
//           $data['cv_id']=$cv_id;
//           $data['job_cate']=$this->input->post('job_cate');
//           $data['target_location']=$this->input->post('target_location');
//           $this->breadcrumbs->push('Home','admin/index');
//           $this->breadcrumbs->push('Creat Cv','admin/creat-cv');
//           $data['loginame']=$this->session->userdata('real_name');
//           $data['pagetitle']='Creat Cv';
//           $data['sidebar_menu']='admin/sidebar_menu';
//           $data['loginame']=$this->session->userdata('username');
////           $data['main_content']='setting/final_created';
//           $this->load->view('admin/admin_template',$data);

     }

    public function edit_cv()
     {  
      $id=urldecode(encryptor('decrypt',$this->uri->segment(3)));
      $this->db->where('t_id',$id);
      $col = $this->db->get('c_information_tbl')->row_array();
      @$cv_id=$col['cv_id'];
         if(count(@$cv_id)!=0) {
             $data['cv_id'] = $cv_id;
             $data['job_cate'] = $col['cat_code'];
             $data['target_location'] = $col['target_location'];
             $this->breadcrumbs->push('Home', 'admin/index');
             $this->breadcrumbs->push('Edit Cv', 'admin/edit-cv');
             $data['loginame'] = $this->session->userdata('real_name');
             $data['pagetitle'] = 'Edit Cv';
             $data['sidebar_menu'] = 'admin/sidebar_menu';
             $data['main_content'] = 'setting/edit_cv';
             $this->load->view('admin/admin_template', $data);
         }
         else {
             $this->breadcrumbs->push('Home', 'admin/index');
             $this->breadcrumbs->push('Edit Cv', 'admin/edit-cv');
             $this->breadcrumbs->push('Error', 'admin/edit-cv');
             $data['loginame'] = $this->session->userdata('real_name');
             $data['pagetitle'] = 'Error';
             $data['sidebar_menu'] = 'admin/sidebar_menu';
             $data['main_content'] = 'admin/error';
             $this->load->view('admin/admin_template', $data);
         }

     }

    public function career_update(){

        $c_objective=$this->input->post('c_objective');
        $cv_id=$this->input->post('cv_id');
        $data=array(
            'c_objective'=>$c_objective
        );
        $this->db->where('cv_id',$cv_id);
        $this->db->update('c_information_tbl',$data);
        echo json_encode($data);
    }


    public function uploadpf(){

        $this->load->library('crop');
        $this->crop->load();
    }

    public function profile_upload()
    {
        $cv_id= urldecode($this->uri->segment(3));
       // $img_val= urldecode($this->uri->segment(4));
        date_default_timezone_set("Asia/Rangoon");
        $val = date('YmdHis');
        $date=$val-1;
        $img_val= $cv_id . '.png';
        $data=array('cv_pic'=>$img_val);
        $this->db->where('cv_id',$cv_id);
        $this->db->update('c_information_tbl',$data);
        echo "<img src='uploads/profile/".$cv_id.".png'>";
    }

     public function uploadimg()
     {
         $name= $this->input->post('attachName');
         $this->main_model->upload_img('userfile',$name);
         redirect('admin/edit-cv/'.$this->uri->segment(3));
     }

     public function editimg()
     {
        $name= $this->input->post('attachName');
         $cv_id = $this->input->post('img_id');;
       $this->main_model->upload_img('userfile',$name);
         $this->main_model->upload_coverletter('coverletter',$cv_id);
       redirect('admin/edit-cv/'.$this->uri->segment(3).'/#lang_section');
    }

     public function deleimg()
     {
       $this->db->where('t_id',$this->input->post('t_id'));
       $this->db->delete('file_of_cv');
       unlink('uploads/' . $this->input->post('imgname'));
     
     }

     public function app_no()
     {
      $this->load->view('setting/app_no');
     }

     public function show()
      {
              $cv_id=$this->input->post('cv_id');
              $data = array(
                          'c_number' => $this->input->post('contact_no'),
                          'email' => $this->input->post('email'),
                          'address' => $this->input->post('address'),
                            'sec_contact_no'=>$this->input->post('sec_contact_no')
                          );

              $this->db->where('cv_id',$cv_id);
              $this->db->update('c_information_tbl',$data);  
              echo json_encode($data);  

      }

    /**
     *
     */
    public function show2()
      {
                  $cv_id=$this->input->post('cv_id');
                  $gender= $this->input->post('gender');
                  $gender= $this->main_model->gender($gender);
                  $marital= $this->input->post('marital');
                  $marital= $this->main_model->marital($marital);
                  $cur_location= $this->input->post('cur_location');
                  $cur_post= $this->input->post('cur_post');
                  $cur_company= $this->input->post('cur_company');
                  $salary_exp= $this->input->post('salary_exp');
                  $weight= $this->input->post('weight');
                  $height= $this->input->post('height');
                  $not_period=$this->input->post('not_period');
                  $a = $salary_exp;
                  $b = str_replace( ',', '', $a );

                  if( is_numeric( $b ) ) {
                      $a = $b;
                  }
              if ($cur_location!='')
              {                
                $cur_location=$this->main_model->country($cur_location);              
              } 
//              //date in mm/dd/yyyy format; or it can be in other formats as well
//              $birthDate = $this->input->post('date_of_birth');
//              //explode the date to get month, day and year
//              $birthDate = explode("-", $birthDate);
//              //get age from date or birthdate
//              $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
//                ? ((date("Y") - $birthDate[2]) - 1)
//                : (date("Y") - $birthDate[2]));
                           
              $data = array(                          
                              'first_name' => $this->input->post('first_name'),
                              'last_name' => $this->input->post('last_name'),
                              'nrc_no' => $this->input->post('nrc_no'),
                              'religion'=>$this->input->post('religion'),
                              'gender' => $gender,
                              'marital' => $marital,
                              'date_of_birth' => $this->input->post('date_of_birth'),
                              'dad_name' => $this->input->post('dad_name'),
                              'cur_location' =>  $cur_location,
                              'nationality' => $this->input->post('nationality'),
                              'cur_post'=> $cur_post,
                              'cur_company'=> $cur_company,
                              'salary_exp'=>  $salary_exp,
                              'weight'=>$weight,
                              'height'=>$height,
                              'not_period'=>$not_period

                          );
              $up = array(                          
                          'first_name' => $this->input->post('first_name'),
                          'last_name' => $this->input->post('last_name'),
                          'nrc_no' => $this->input->post('nrc_no'),
                          'religion'=>$this->input->post('religion'),
                          'gender' => $this->input->post('gender'),
                          'marital' => $this->input->post('marital'),
                          'date_of_birth' => strtotime($this->input->post('date_of_birth')),
                          'dad_name' => $this->input->post('dad_name'),
                          'cur_location' =>  $this->input->post('cur_location'),
                          'nationality' => $this->input->post('nationality'),
                          'cur_post'=> $cur_post,
                          'cur_company'=> $cur_company,
                          'salary_exp'=> $b,
                          'weight'=>$weight,
                          'height'=>$height,
                            'not_period'=>$not_period
                          );               
              $this->db->where('cv_id',$cv_id);
              $this->db->update('c_information_tbl',$up);  
              echo json_encode($data);  
      }

    public function work_exp()
      {
         $this->load->view('setting/work_exp');
      }

    public function work_exp_insert()
      {

          $end_year = $this->input->post('end_year');
          $current_working='';
          $now=0;
          $work_exp=$end_year-$now-$this->input->post('start_year');
          if ($end_year=='1')
          {
              $current_working=1;
              $now= date('Y');
              $work_exp=$now-$this->input->post('start_year');
          }
          $data = array(
                  'cv_id' => $this->input->post('cv_id'), 
                  'company' => $this->input->post('company'),
                  'exp_cat'=>$this->input->post('exp_cat'),
                  'job_title' => $this->input->post('job_title'),
                  'exp_industry' => $this->input->post('exp_industry'),
                  'city' => $this->input->post('city'),
                  'country' => $this->input->post('country'),
                  'start_year' => $this->input->post('start_year'),
                  'start_month' => $this->input->post('start_month'),
                  'end_year' => $this->input->post('end_year'),
                  'end_month' => $this->input->post('end_month'),
                  'current_working'=>$current_working,
                  'work_exp'=>$work_exp,
                  'description'=>$this->input->post('description')
                  );
          $this->db->insert('working_exp_tbl',$data);
          echo json_encode($data); 
      }

    public function work_exp_edit()
      {

          $end_year = $this->input->post('end_year');
          $current_working='';
          $now=0;

          $work_exp=$end_year-$now-$this->input->post('start_year');
          if ($end_year=='1')
          {
              $current_working=1;
              $now= date('Y');
              $work_exp=$now-$this->input->post('start_year');
          }
          $t_id=$this->input->post('t_id');
          $data = array(
                  'company' => $this->input->post('company'),
                  'exp_cat'=>$this->input->post('exp_cat'),
                  'job_title' => $this->input->post('job_title'),
                  'exp_industry' => $this->input->post('exp_industry'),
                  'city' => $this->input->post('city'),
                  'country' => $this->input->post('country'),
                  'start_year' => $this->input->post('start_year'),
                  'start_month' => $this->input->post('start_month'),
                  'end_year' => $this->input->post('end_year'),
                  'end_month' => $this->input->post('end_month'),
                  'current_working'=>$current_working,
                  'work_exp'=>$work_exp,
                  'description'=>$this->input->post('description')
                  );
          $this->db->where('t_id',$t_id);
          $this->db->update('working_exp_tbl',$data);
          echo json_encode($data); 
      }

    public function exp_show()
      {
        $this->load->view('setting/exp_show');
      }

    public function delete_exp()
      {
        $id=$this->input->post('id');
        if (!empty($id)) 
        {
        $this->db->where('t_id',$id);
        $this->db->delete('working_exp_tbl');
        exit();
        }
        return false;
      }

    public function edit_exp()
      {
        $this->load->view('setting/edit_work_exp');
      }

      // Education

    public function education_form()
      {
         $this->load->view('setting/education/education_form');
      }

    public function education_insert()
      { 


          $data = array(
                  'cv_id' => $this->input->post('cv_id'), 
                  'university' => $this->input->post('university'), 
                  'grad_start_year' => $this->input->post('grad_start_year'), 
                  'grad_start_month' => $this->input->post('grad_start_month'),
                  'grad_date' => $this->input->post('grad_date'),
                  'grad_month' => $this->input->post('grad_month'),
                  'degree_lvl' => $this->input->post('degree_lvl'),
                  'otherdegree_lvl'=>$this->input->post("otherdegree_lvl"),
                  'subject_m' => $this->input->post('subject_m'),
                  'country' => $this->input->post('country'),
                 
                  );
          $this->db->insert('education_tbl',$data);
          echo json_encode($data); 
      }

    public function edu_show()
      {
        $this->load->view('setting/education/edu_show');
      }

    public function edit_edu()
      {
        $this->load->view('setting/education/education_edit_form');
      }

    public function education_edit()
      { 

          $id = $this->input->post('t_id');
          $data = array(
                  
                  'university' => $this->input->post('university'), 
                  'grad_start_year' => $this->input->post('grad_start_year'), 
                  'grad_start_month' => $this->input->post('grad_start_month'),
                  'grad_date' => $this->input->post('grad_date'),
                  'grad_month' => $this->input->post('grad_month'),
                  'degree_lvl' => $this->input->post('degree_lvl'),
                  'subject_m' => $this->input->post('subject_m'),
                  'country' => $this->input->post('country'),
                 
                  );
          $this->db->where('t_id',$id);
          $this->db->update('education_tbl',$data);
          echo json_encode($data); 
      }

  /// Certificate

     public function certi_form()
      {
         $this->load->view('setting/certi/certi_form');
      }

          public function certi_insert()
      { 
          $data = array(
                  'cv_id' => $this->input->post('cv_id'), 
                  'certi' => $this->input->post('certi'), 
                  'certi_year' => $this->input->post('certi_year'),
                  'certi_month' => $this->input->post('certi_month'),
                  't_center' => $this->input->post('t_center'),
                    'certi_country'=>$this->input->post('certi_country')
                    );
          $this->db->insert('certi_tbl',$data);
          echo json_encode($data); 
      }

      public function certi_edit()
      {

        $id = $this->input->post('t_id');
         $data = array(
                  'certi' => $this->input->post('certi'), 
                  'certi_year' => $this->input->post('certi_year'),
                  'certi_month' => $this->input->post('certi_month'),
                  't_center' => $this->input->post('t_center'),
                   'certi_country'=>$this->input->post('certi_country')
                    );
         $this->db->where('t_id',$id);
          $this->db->update('certi_tbl',$data);
          echo json_encode($data); 
      }

      public function certi_show()
      {
         $this->load->view('setting/certi/certi_show');
      }

      public function edit_certi()
      {
         $this->load->view('setting/certi/certi_edit_form');
      }


  /// IT SKILL

      public function it_form()
      {
         $this->load->view('setting/it/it_form');
      }

      public function it_insert()
      { 
          $data = array(
                  'cv_id' => $this->input->post('cv_id'), 
                  'topic_app' => $this->input->post('topic_app'), 
                  'it_type' => $this->input->post('it_type'), 
                  'it_skill_lvl' => $this->input->post('it_skill_lvl'),
                  );
          $this->db->insert('it_skill_tbl',$data);
          echo json_encode($data); 
      }

          public function it_edit()
      { 

          $id = $this->input->post('t_id');
          $data = array(
                  'topic_app' => $this->input->post('topic_app'), 
                  'it_type' => $this->input->post('it_type'), 
                  'it_skill_lvl' => $this->input->post('it_skill_lvl'),
                 
                  );
          $this->db->where('t_id',$id);
          $this->db->update('it_skill_tbl',$data);
          echo json_encode($data); 
      }

            public function it_show()
      { 
        $this->load->view('setting/it/it_show');
      }

      public function edit_itskill()
      { 
        $this->load->view('setting/it/it_edit_form');
      }

    //REF PERSON

    public function refperson_form()
    {
        $this->load->view('setting/refperson/refperson_form');
    }

    public function refperson_show()
    {
        $this->load->view('setting/refperson/refperson_show');
    }

    public function refperson_edit()
    {
        $id = $this->input->post('t_id');
        $data = array(

            'ref_name' => $this->input->post('ref_name'),
            'ref_occupation' => $this->input->post('ref_occupation'),
            'ref_employer' => $this->input->post('ref_employer'),
            'ref_ph_no' => $this->input->post('ref_ph_no'),
            'ref_email' => $this->input->post('ref_email'),
        );
        $this->db->where("t_id",$id);
        $this->db->update('ref_person_tbl',$data);
        echo json_encode($data);
    }

    public function refperson_insert()
    {
        $data = array(
            'cv_id' => $this->input->post('cv_id'),
            'ref_name' => $this->input->post('ref_name'),
            'ref_occupation' => $this->input->post('ref_occupation'),
            'ref_employer' => $this->input->post('ref_employer'),
            'ref_ph_no' => $this->input->post('ref_ph_no'),
            'ref_email' => $this->input->post('ref_email'),
        );
        $this->db->insert('ref_person_tbl',$data);
        echo json_encode($data);
    }

    public function edit_refperson()
    {
        $this->load->view('setting/refperson/refperson_edit_form');
    }


    /// Language SKILL

      public function lang_form()
      {
         $this->load->view('setting/lang/lang_form');
      }

      public function lang_insert()
      { 
          $data = array(
                  'cv_id' => $this->input->post('cv_id'), 
                  'lang' => $this->input->post('lang'), 
                  'lang_skill' => $this->input->post('lang_skill'), 
                  );
          $this->db->insert('lang_tbl',$data);
          echo json_encode($data); 
      }

      public function edit_lang()
      { 
        $this->load->view('setting/lang/lang_edit_form');
      }


            public function lang_edit()
      { 
         $id = $this->input->post('t_id');
          $data = array(                   
                  'lang' => $this->input->post('lang'), 
                  'lang_skill' => $this->input->post('lang_skill'), 
                  );
          $this->db->where('t_id',$id);
          $this->db->update('lang_tbl',$data);
          echo json_encode($data); 
      }

            public function lang_show()
      { 
         $this->load->view('setting/lang/lang_show');
      }

// Apply Position

    public function post_form(){
        $this->load->view('setting/post/post_form');
    }

    public function post_insert()
    {
        $data = array(
            'cv_id' => $this->input->post('cv_id'),
            'job_industry'=>$this->input->post('app_industry'),
            'job_type' => $this->input->post('job_type'),
            'job_cate' => $this->input->post('app_job'),
            'apply_post' => $this->input->post('apply_post'),
        );
        $this->db->insert('app_pos_tbl',$data);
        echo json_encode($data);
    }

    public function post_update()
    {
        $id = $this->input->post('t_id');
        $data = array(
            'job_type' => $this->input->post('job_type'),
            'job_industry'=>$this->input->post('app_industry'),
            'job_cate' => $this->input->post('app_job'),
            'apply_post' => $this->input->post('apply_post')
        );
        $this->db->where('t_id',$id);
        $this->db->update('app_pos_tbl',$data);
        echo json_encode($data);
    }

    public function edit_post(){
        $this->load->view('setting/post/post_edit_form');
    }

    public function post_show()
    {
        $this->load->view('setting/post/post_show');
    }



      //// Dynamically function

    public function delete_edu()
    {
        $id=$this->input->post('id');
        $table_name=$this->input->post('table_name');
        if (!empty($id)) 
        {
        $this->db->where('t_id',$id);
        $this->db->delete($table_name);
        exit();
        }
       return false; 
      }

   // SETTING ********************************************

   public function setting()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->db->order_by('cat_code','ASC');
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Setting';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['query']=$this->db->get('job_category_tbl')->result_array();
    $data['main_content']='setting/setting';
    $this->load->view('admin/admin_template',$data);
   }





  // JOB CATEGORY ****************************************

   public function job_category_show()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('Job Category','admin/job-category-show');
    $this->db->order_by('cat_code','ASC');
    $data['query']=$this->db->get('job_category_tbl')->result_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Setting';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/job_category_show';
    $this->load->view('admin/admin_template',$data);
   }

   public function job_category_setting()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('Job Category','admin/job-category-show');
    $this->breadcrumbs->push('Add New Job Category','admin/job-category-setting');
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Jobg Category Setting';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/job_category_setting_form';
    $this->load->view('admin/admin_template',$data);
   }

      public function edit_jobcategory()
   {
    $id=$this->uri->segment(3);
    $this->db->where('t_id',$id);
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('Job Category','admin/job-category-show');
    $this->breadcrumbs->push('Edit Job Category','edit-jobcategory');    
    $data['row']=$this->db->get('job_category_tbl')->row_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Setting';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/job_category_setting_edit_form';
    $this->load->view('admin/admin_template',$data);
   }

    public function delete_jobcategory()
   {
    $id=$this->uri->segment(3);
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Setting';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/delete_confirmation';
    $this->load->view('admin/admin_template',$data);
   }


   public function job_category_add()
   {
      $description=$this->input->post('description');
      $code=$this->input->post('code');
      date_default_timezone_set("Asia/Rangoon");
      $time =  strtotime(date("Y-m-d H:i:s")); 
     
      $data = array
        (
        'cat_code' =>$code ,
        'description' =>$description ,
        'creat_date'=>$time,
        'user_id'=>$this->session->userdata('user_id'),
        'creator_name'=>$this->session->userdata('real_name')
        );      
      $this->db->insert('job_category_tbl',$data);
      $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Job Category',
                      'description' => 'Add new',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data);
      redirect('admin/job-category-show');
   }

   public function job_category_edit()
   {
      $id=$this->uri->segment(3);     
      $description=$this->input->post('description');
      $data = array
      (
      'description' =>$description , 
      );
      $this->db->where('t_id',$id);
      $this->db->update('job_category_tbl',$data);
      date_default_timezone_set("Asia/Rangoon");
      $time =  strtotime(date("Y-m-d H:i:s")); 
      $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Job Category',
                      'description' => 'Edit',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data);
      redirect('admin/job-category-show');
   }


      public function job_category_delete()
   {
      $id=$this->uri->segment(3);
      $this->db->where('t_id',$id);
      $this->db->delete('job_category_tbl'); 
      date_default_timezone_set("Asia/Rangoon");
      $time =  strtotime(date("Y-m-d H:i:s"));
      $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Job Category',
                      'description' => 'Delete',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data); 
      redirect('admin/job-category-show/');
   }

  // JOB CATEGORY END ****************************************

  // Country Managment ****************************************

   public function country_management()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('Country Management','admin/country-management');
    $data['query']=$this->db->get('country_tbl')->result_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Setting';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/country_show';
    $this->load->view('admin/admin_template',$data);
   }

   public function country_add_new()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('Country Management','admin/country-management');
    $this->breadcrumbs->push('Add Country','admin/country-add-new');
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Add New Country';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/country_add_new_form';
    $this->load->view('admin/admin_template',$data);
   }

   public function country_add_new_process()
   {
    $d_name= $this->input->post('d_name');
    date_default_timezone_set("Asia/Rangoon");
    $time =  strtotime(date("Y-m-d H:i:s"));
    $data = array
    (
    'd_name' => $d_name , 
    'creat_date' => $time , 
    'user_id' => $this->session->userdata('user_id'),
    'creator_name'=> $this->session->userdata('real_name')
    );
    $this->db->insert('country_tbl',$data);
    $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Country',
                      'description' => 'Add new',
                      'date_time' => $time
                  );
    $this->db->insert('activity_log_tbl',$data);
    redirect('admin/country-management');
   }


      public function country_name_edit()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('Country Management','admin/country-management');
    $this->breadcrumbs->push('Edit Country Name','admin/country-name-edit');
    $id=$this->uri->segment(3);
    $this->db->where('t_id',$id);
    $data['row']=$this->db->get('country_tbl')->row_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='Country Name Edit';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/country_name_edit';
    $this->load->view('admin/admin_template',$data);
   }

   public function country_name_edit_process()
   {
      $id=$this->uri->segment(3);
      $d_name=$this->input->post('d_name');
      date_default_timezone_set("Asia/Rangoon");
      $time =  strtotime(date("Y-m-d H:i:s"));
      $data = array
      ('d_name' => $d_name , );
      $this->db->where('t_id',$id);
      $this->db->update('country_tbl',$data);
      $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Country',
                      'description' => 'Edit',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data);
      redirect('admin/country-management');
   }

    public function delete_country()
    {
    $id=$this->uri->segment(3);
    $data['loginame']=$this->session->userdata('username');
    $data['main_content']='setting/delete_confirmation';
    $this->load->view('admin/admin_template',$data);
    }

  public function delete_country_process()
   {
      $id=$this->uri->segment(3);
      $this->db->where('t_id',$id);
      $this->db->delete('country_tbl'); 
      date_default_timezone_set("Asia/Rangoon");
      $time =  strtotime(date("Y-m-d H:i:s"));
      $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Country',
                      'description' => 'Delete',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data); 
      redirect('admin/country-management/');
   }






  // User Managment ****************************************

   public function user_management()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('User Managment','admin/user-management');
    $this->db->order_by('user_role','DESC');
    $data['query']=$this->db->get('users_tbl')->result_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='User Managment';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/user_show';
    $this->load->view('admin/admin_template',$data);
   }

    public function user_detail()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('User Managment','admin/user-management');
    $this->breadcrumbs->push('User Detail','admin/user-detail');
    $id=$this->uri->segment(3);
    $this->db->where('t_id',$id);
    $data['row']=$this->db->get('users_tbl')->row_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='User Detail';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/detail_show';
    $this->load->view('admin/admin_template',$data);
   }

   public function add_user()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('User Managment','admin/user-management');
    $this->breadcrumbs->push('Add User','admin/add-user');
    $data['query']=$this->db->get('users_tbl')->result_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='User Managment';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/add_user_form';
    $this->load->view('admin/admin_template',$data);
   }

   public function add_user_process()
   {
    $first_name=$this->input->post('first_name');
    $last_name=$this->input->post('last_name');
    $username=$this->input->post('username');
    $password=$this->input->post('password');
    $user_role=$this->input->post('user_role');
    date_default_timezone_set("Asia/Rangoon");
    $time =  strtotime(date("Y-m-d H:i:s"));
    $user_id=$this->db->count_all('users_tbl');
      $data = array(
      'user_id'=>$user_id,
      'username' => $username , 
      'first_name' => $first_name , 
      'last_name' => $last_name ,
      'password'=> md5($password),
      'user_role'=> $user_role,
      'creat_date'=> $time,
      'creator_name'=> $this->session->userdata('real_name')
      );
    $this->db->insert('users_tbl',$data);
    $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'User Managment',
                      'description' => 'Add New',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data);
      redirect('admin/user-management');

   }

  public function edit_user()
   {
    $this->breadcrumbs->push('Home','admin');
    $this->breadcrumbs->push('Setting','admin/setting');
    $this->breadcrumbs->push('User Managment','admin/user-management');
    $this->breadcrumbs->push('Edit User','admin/edit-user');
    $id=$this->uri->segment(3);
    $this->db->where('t_id',$id);
    $data['row']=$this->db->get('users_tbl')->row_array();
    $data['loginame']=$this->session->userdata('real_name');
    $data['pagetitle']='User Managment';
    $data['sidebar_menu']='admin/sidebar_menu';
    $data['main_content']='setting/edit_user_form';
    $this->load->view('admin/admin_template',$data);
   }

  public function edit_user_process()
   {
    $id=$this->uri->segment(3);
    $first_name=$this->input->post('first_name');
    $last_name=$this->input->post('last_name');
    $username=$this->input->post('username');
    $password=$this->input->post('password');
    $user_role=$this->input->post('user_role');
    date_default_timezone_set("Asia/Rangoon");
    $time =  strtotime(date("Y-m-d H:i:s"));
    $data = array(
      'username' => $username , 
      'first_name' => $first_name , 
      'last_name' => $last_name ,
      'password'=> md5($password),
      'user_role'=> $user_role,
      'creat_date'=> $time,
      'creator_name'=> $this->session->userdata('real_name')
      );
    $this->db->where('t_id',$id);
    $this->db->update('users_tbl',$data);
    $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'User Managment',
                      'description' => 'Edit',
                      'date_time' => $time
                  );
    $this->db->insert('activity_log_tbl',$data);
    redirect('admin/user-management');
   }

public function delete_user()
   {
      $id=$this->uri->segment(3);
      $this->db->where('t_id',$id);
      $this->db->delete('users_tbl'); 
      date_default_timezone_set("Asia/Rangoon");
      $time =  strtotime(date("Y-m-d H:i:s"));
      $data = array
                  (
                      'user_id' => $this->session->userdata('user_id'), 
                      'username' => $this->session->userdata('username'),
                      'real_name' => $this->session->userdata('real_name'),
                      'About' => 'Country',
                      'description' => 'Delete',
                      'date_time' => $time
                  );
      $this->db->insert('activity_log_tbl',$data); 
      redirect('admin/user-management');
   }

   public function upload_insert()
   {
    $img_val=$this->input->post('img_val');
    $cv_id=$this->input->post('cv_id');
    $data = array('certi' => $img_val ,'cv_id' => $cv_id );
    $this->db->insert('certi_tbl',$data);
   }





   



}