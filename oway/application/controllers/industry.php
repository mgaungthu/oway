<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Content-Type: text/html; charset=utf-8');


class Industry extends CI_Controller {

    function __construct()
    {             
        parent::__construct();
 		
     		if ($this->session->userdata('login_state') != 'true')
     		{
     			redirect('site');
     		}


    }



    public function industry_show()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Setting','admin/setting');
        $this->breadcrumbs->push('Job Industry','industry/job-industry-show');
        $this->db->order_by('description','DESC');
        $data['query']=$this->db->get('industry_tbl')->result_array();
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Setting';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='setting/industry/industry_show';
        $this->load->view('admin/admin_template',$data);
    }

    public function industry_setting()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Setting','admin/setting');
        $this->breadcrumbs->push('Job Industry','industry/industry-show');
        $this->breadcrumbs->push('Add New Job Category','industry/industry-setting');
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Jobg Category Setting';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='setting/industry/industry_setting_form';
        $this->load->view('admin/admin_template',$data);
    }

    public function edit_industry()
    {
        $id=$this->uri->segment(3);
        $this->db->where('t_id',$id);
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Setting','admin/setting');
        $this->breadcrumbs->push('Job Industry','industry/industry-show');
        $this->breadcrumbs->push('Edit Job Category','edit-industry');
        $data['row']=$this->db->get('job_category_tbl')->row_array();
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Setting';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='setting/industry/industry_setting_edit_form';
        $this->load->view('admin/admin_template',$data);
    }

    public function delete_industry()
    {
        $id=$this->uri->segment(3);
        $data['loginame']=$this->session->userdata('real_name');
        $data['pagetitle']='Setting';
        $data['sidebar_menu']='admin/sidebar_menu';
        $data['main_content']='setting/delete_confirmation';
        $this->load->view('admin/admin_template',$data);
    }


    public function industry_add()
    {
        $description=$this->input->post('description');
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));

        $data = array
        (
            'description' =>$description ,
            'creat_date'=>$time,
            'user_id'=>$this->session->userdata('user_id'),
            'creator_name'=>$this->session->userdata('real_name')
        );
        $this->db->insert('industry_tbl',$data);
        $data = array
        (
            'user_id' => $this->session->userdata('user_id'),
            'username' => $this->session->userdata('username'),
            'real_name' => $this->session->userdata('real_name'),
            'About' => 'Job Industry',
            'description' => 'Add new',
            'date_time' => $time
        );
        $this->db->insert('activity_log_tbl',$data);
        redirect('industry/industry-show/');
    }

    public function industry_edit()
    {
        $id=$this->uri->segment(3);
        $description=$this->input->post('description');
        $data = array
        (
            'description' =>$description ,
        );
        $this->db->where('t_id',$id);
        $this->db->update('industry_tbl',$data);
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $data = array
        (
            'user_id' => $this->session->userdata('user_id'),
            'username' => $this->session->userdata('username'),
            'real_name' => $this->session->userdata('real_name'),
            'About' => 'Job Industry',
            'description' => 'Edit',
            'date_time' => $time
        );
        $this->db->insert('activity_log_tbl',$data);
        redirect('industry/industry-show/');
    }


    public function industry_delete()
    {
        $id=$this->uri->segment(3);
        $this->db->where('t_id',$id);
        $this->db->delete('industry_tbl');
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $data = array
        (
            'user_id' => $this->session->userdata('user_id'),
            'username' => $this->session->userdata('username'),
            'real_name' => $this->session->userdata('real_name'),
            'About' => 'Job Industry',
            'description' => 'Delete',
            'date_time' => $time
        );
        $this->db->insert('activity_log_tbl',$data);
        redirect('industry/industry-show/');
    }
   



}