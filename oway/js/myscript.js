 $(document).ready(function() 
    {
        $('.cur').formatCurrency(
            {
                symbol: 'Ks',
                digitGroupSymbol: ',',
                positiveFormat: '%n %s',
                negativeFormat: '-%n %s'
            }
        );

        $('.currency').blur(function()
        {
            $('.currency').formatCurrency({
                symbol: 'Ks',
                digitGroupSymbol: ',',
                positiveFormat: '%n %s',
                negativeFormat: '-%n %s'
            });
        });
        
        $.validator.setDefaults({ ignore: ":hidden:not(select)" })
          $("form").validate({
            rules: {chosen:"required"},
            
          });



        $('input[id="email_sit"]').change(function (event) {
            if($(this).val()==1 ) {
                $('#printTrigger').val('Send Email');
            }
            else{
                $('#printTrigger').val('Export PDF');
            }
        });


        $("body").on("click", "#printTrigger", function(){
           var radio = $('input[id="email_sit"]:checked').val();
            if(radio==1 && $('#apply_post option:selected').val()){
            //$('#loading').fadeIn();
                $(this).val('Please wait..');
            }

        });


        $('#sorting').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $("#compose-textarea").wysihtml5();

        $('#cv_table').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        jQuery('#date_of_birth').datetimepicker({
          timepicker:false,
          format:'d-m-Y',

          formatDate:'d-m-Y',
          closeOnDateSelect: true
        });

        // DatetimePicker set Interval function

        $("body").on("click", "#date_timepicker_start", function(){
            $(this).datetimepicker({
                format:'Y/m/d',
                onShow:function( ct ){
                    this.setOptions({
                        maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false
                    })
                },

                timepicker:false,
                closeOnDateSelect: true

            });
            $(this).datetimepicker("show");

        });

        $("body").on("click", "#date_timepicker_end", function(){
            $(this).datetimepicker({
                format:'Y/m/d',
                onShow:function( ct ){
                    this.setOptions({
                        minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
                    })
                },

                timepicker:false,
                closeOnDateSelect: true

            });
            $(this).datetimepicker("show");

        });

      $('.chosen-select').chosen();
      $('.chosen-select-deselect').chosen({ allow_single_deselect: true });


        $('[id=cat_code]').on('change',function()
        {
          var cat_code = $('[id=cat_code]').val();
              url     = $('[id=url]').val();
              cv_id = $('[id=cv_id]').val();
              target_location = $('#target_location option:selected').val();
                          
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('app_no').innerHTML = xmlhttp.responseText;
                document.getElementById('app_no2').innerHTML = xmlhttp.responseText;
                $('#app_no2').fadeOut();
              }
            }
              xmlhttp.open('GET',url+'/'+cat_code+'/'+cv_id+'/'+target_location,true);
              xmlhttp.send();     
        });   



    // Contact information

                
        $('#c_info_form input').on('keyup blur', function () {
        if ($('#c_info_form').valid()) 
        {           
                    
        $('.ft-btn').prop('disabled', false);
         
        } 
        else 
        {

            $('.ft-btn').prop('disabled', 'disabled');
        }
        });


        $("#emailhelp").click(function(event)
        {
            event.preventDefault();

            var bodyemail = $("textarea#bodyemail").val();
                url     = $('[id=mailform]').attr('action');
            $(this).val('Sending...');


            jQuery.ajax({
                type: "POST",
                url: url ,
                data: {bodyemail: bodyemail},
                success: function(response) {
                    if (response)
                    {
                        $('.helpmail').modal('hide');
                        $(this).val('Send');
                    }
                }
            });

        });

        
         $("#con_de").click(function(event)
        {
                    event.preventDefault();
                    $("#c_info_form").validate();
                    var email = $("input#email").val();
                        address = $("textarea#address").val();
                        contact_no = $("input#contact_no").val();
                        sec_contact_no = $("input#sec_contact_no").val();
                        cv_id = $('[id=cv_id]').val();
                        url2     = $('[id=url2]').val();
                        
                        $('#form1').fadeOut();
                        
                        // $('#form2').fadeIn();
                    
        jQuery.ajax({
                        type: "POST",
                        url: url2 ,
                        dataType: 'json',
                        data: {email: email,address:address,contact_no:contact_no,sec_contact_no:sec_contact_no,cv_id:cv_id},
                        success: function(res) {
                            if (res)
                            {
                                jQuery("div#testshow").fadeIn();
                                jQuery("span#contactshow").html(res.c_number);
                                jQuery("span#emailshow").html(res.email);
                                jQuery("span#addshow").html(res.address);
                                jQuery("span#sec_contact_no").html(res.sec_contact_no);
                                
                               
                            }
                        }
                    });   

        });

        $("#edit_contact").click(function(event) {
            $('#form1').fadeIn();
            $('#testshow').fadeOut();
         });


    // personal information

        

        $('#p_info_form input').on('keyup blur', function () {
        if ($('#p_info_form').valid()) 
        {                       
            $('.sec-btn').prop('disabled', false);         
        } 
        else 
        {
            $('.sec-btn').prop('disabled', 'disabled');
        }
        });

        


         $("#per_info").click(function(event)
        {
                    event.preventDefault();
                    $("#p_info_form").validate();
                    var first_name = $("input#first_name").val();
                        last_name = $("input#last_name").val();
                        nrc_no = $("input#nrc_no").val();
                        religion = $("input#religion").val();
                        gender = $("#gender option:selected").val();
                        marital  = $("#marital option:selected").val();
                        date_of_birth = $("input#date_of_birth").val();
                        cur_location = $("#cur_location option:selected").val();
                        dad_name = $("input#dad_name").val();
                        weight = $("input#weight").val();
                        height = $("input#height").val();
                        nationality = $("input#nationality").val();
                        cur_post = $("input#cur_post").val();
                        cur_company = $("input#cur_company").val();
                        salary_exp = $("input#salary_exp").val();
                        not_period = $("input#not_period").val();
                        cv_id = $('[id=cv_id]').val();
                        form2url     = $('[id=form2url]').val();
                        $('#form2').fadeOut();
                        // console.log(form2url);

        jQuery.ajax({
                    type: "POST",
                    url: form2url ,
                    dataType: 'json',
                    data: {not_period:not_period,weight:weight,height:height,cur_post:cur_post,cur_company:cur_company,salary_exp:salary_exp,first_name: first_name,last_name:last_name,nrc_no:nrc_no,religion:religion,gender:gender,marital:marital,date_of_birth:date_of_birth,cur_location:cur_location,dad_name:dad_name,nationality:nationality,cv_id:cv_id},
                    success: function(res) 
                            {
                    if (res)
                                 {
                                        jQuery("div#personalpreview").fadeIn();
                                        jQuery("span#first_name").html(res.first_name);
                                        jQuery("span#last_name").html(res.last_name);
                                        jQuery("span#nrc_no").html(res.nrc_no);
                                        jQuery("span#religion").html(res.religion);
                                        jQuery("span#gender").html(res.gender);
                                        jQuery("span#date_of_birth").html(res.date_of_birth);
                                        jQuery("span#dad_name").html(res.dad_name);
                                        jQuery("span#marital").html(res.marital);
                                        jQuery("span#cur_location").html(res.cur_location);
                                        jQuery("span#nationality").html(res.nationality);
                                        jQuery("span#cur_post").html(res.cur_post);
                                        jQuery("span#cur_company").html(res.cur_company);
                                        jQuery("span#salary_exp").html(res.salary_exp);
                                         jQuery("span#weight").html(res.weight);
                                         jQuery("span#height").html(res.height);
                                     jQuery("span#not_period").html(res.not_period);
                                }
                             }
                    });  
      

        });

        $("#career_obj").click(function(event)
        {
            event.preventDefault();

            var c_objective = $("textarea#c_objective").val();
                cv_id = $('input#cv_id').val();
                url     = $('input#objurl').val();

            $('#carrer_form').fadeOut();

            // $('#form2').fadeIn();

            jQuery.ajax({
                type: "POST",
                url: url ,
                dataType: 'json',
                data: {c_objective: c_objective,cv_id:cv_id},
                success: function(res) {
                    if (res)
                    {
                        jQuery("div#career_wrap").fadeIn();
                        jQuery("span#cb_show").html(res.c_objective);

                    }
                }
            });

        });

        $("#edit_obj").click(function(event) {
            $('#carrer_form').fadeIn();
            $('#career_wrap').fadeOut();
        });

        $("#edit_personal").click(function(event) {
            $('#form2').fadeIn();
            $('#personalpreview').fadeOut();
         }); 

  /// working exp

    $('[id=add_new]').on('click',function(event) 
        {
          var url     = $(this).attr('url');
              cv_id = $('[id=cv_id]').val();
                      
              // $('#add_new').fadeOut();
             // console.log(url);
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('working_exp_form').innerHTML = xmlhttp.responseText;
                  $('[id=exp_form_wrapper]').focus();
                  $("#description").wysihtml5();
                 $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
                 $('#exp_show_wrapper').fadeOut();
                 $('html,body').animate({
                  scrollTop: $("#exp_section").offset().top},
                  'slow');
              }
            }
              xmlhttp.open('GET',url+'/'+cv_id,true);
              xmlhttp.send();
              $('#working_exp_form').fadeIn();

        });



        $('body').on('click','#exp_submit',function(event)
        {
          event.preventDefault();
          $("#exp_form").validate();
            if ($('#exp_form').valid())
            {
                var url_insert = $(this).attr('url');
                  cv_id = $("input#cv_id").val();
                   company = $("input#company").val();
                  job_title = $("input#job_title").val();
                  city = $("input#city").val();
                  exp_industry = $("#exp_industry option:selected").val();
                  exp_cat = $("#exp_cat option:selected").val();
                  country = $("#country option:selected").val();
                  start_year = $("#start_year option:selected").val();
                  start_month = $("#start_month option:selected").val();
                  end_year = $("#present option:selected").val();
                  end_month = $("#end_month option:selected").val();
                  description = $("textarea#description").val();
                  url_show = $("input#url_show").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_insert,
                      dataType: 'json',
                      data: {exp_industry:exp_industry,exp_cat:exp_cat,cv_id:cv_id,company:company,job_title: job_title, city: city,country:country,start_year:start_year,start_month:start_month,end_year:end_year,end_month:end_month,description:description},
                      success: function(data) 
                        {
                          if (data)
                         {
                            $('#exp_show').focus();
                            $('#exp_form_wrapper').remove();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('exp_show').innerHTML = xmlhttp.responseText;
                              
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
       
                          }
                        }
                  });                                
            } 

                 
        });


       $('body').on('click','#edit_exp_submit',function(event)
        {   
          event.preventDefault();
 
          $("#edit_exp").validate();
            if ($('#edit_exp').valid()) 
            {

              var url_edit = $(this).attr('url');
                  cv_id = $("input#cv_id").val();
                  t_id = $("input#t_id").val();
                  company = $("input#company").val();
                  job_title = $("input#job_title").val();
                  city = $("input#city").val();
                exp_cat = $("#exp_cat option:selected").val();
                exp_industry = $("#exp_industry option:selected").val();
                  country = $("#country option:selected").val();
                  start_year = $("#start_year option:selected").val();
                  start_month = $("#start_month option:selected").val();
                  end_year = $("#present option:selected").val();
                  end_month = $("#end_month option:selected").val();
                  description = $("textarea#description").val();
                  url_show = $("input#url_show").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_edit,
                      dataType: 'json',
                      data: {exp_industry:exp_industry,exp_cat:exp_cat,t_id:t_id,company:company,job_title: job_title, city: city,country:country,start_year:start_year,start_month:start_month,end_year:end_year,end_month:end_month,description:description},
                      success: function(data) 
                        {
                          if (data)
                         {
                            $('#exp_show').focus();
                            $('#exp_form_wrapper').remove();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('exp_show').innerHTML = xmlhttp.responseText;
                              $('html,body').animate({
                              scrollTop: $("#exp-"+t_id).parent(".cv-section").offset().top},
                              'slow');
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();

                          }
                        }
                  });                                
            } 

                 
        });

  
      $('body').on('change','#present',function()
          {
            var present = $('#present option:selected').val();
            
            if (present=='1') 
            {
              $('[id=individual]').fadeOut();
            }
            else
            {
              $('[id=individual]').fadeIn();
            }
            
          });

//Other University

        $('body').on('change','#degree_lvl',function()
        {
            var present = $('#degree_lvl option:selected').val();

            if (present=='30')
            {

                $('[id=otheruni]').fadeIn();
            }
            else
            {
                $('[id=otheruni]').fadeOut();
            }

        });



        del_individual = function(value)
      {

          var del_url = $("input#del_url").val();
              show_url = $("input#show").val();
              cv_id = $("input#cv_id").val();
              $("#delete").attr("tid", value);            
      }

                 $('body').on('click','button[id="delete"]',function()
           {             
                var id = $(this).attr("tid");
                    x =new Object();
                    del_url = $("input#del_url").val();
                    show_url = $("input#show").val();
                    cv_id = $("input#cv_id").val();

                x = {
                    'id':id,
                    };
                      
                $.ajax({
                    url: del_url,
                    cache:false,
                    type: 'POST',
                    data: x,
                    success: function(data) 
                    {                     
                        $('html,body').animate({
                        scrollTop: $("#exp-"+id).parent(".cv-section").offset().top},
                        'slow');
                         $("#exp-"+id).parent(".cv-section").fadeOut('slow');   
                         return false;
                    },              

                });
             
           });

        $('body').on('click','#edit_exp',function() 
        {
           var id = $(this).attr('name');

               url     = $("input#edit_url").val();

                    $('html,body').animate({
                    scrollTop: $("#exp_section").offset().top},
                    'slow');
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('working_exp_form').innerHTML = xmlhttp.responseText;
                  $("#description").wysihtml5();
                 $('#exp_show_wrapper').remove();
                  $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
              }
            }
              xmlhttp.open('GET',url+'/'+id,true);
              xmlhttp.send();             

        });

         $('body').on('click','#cancel_exp',function(event){
          event.preventDefault();
             cv_id = $("input#cv_id").val();
                    $('html,body').animate({
                    scrollTop: $("#exp_section").offset().top},
                    'slow');
          url_show = $("input#url_show").val();
          $('#exp_form_wrapper').fadeOut();                        
              xmlhttp= new XMLHttpRequest();
              xmlhttp.onreadystatechange = function(){
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                 {
                 document.getElementById('exp_show').innerHTML = xmlhttp.responseText;
                  }
                }
              xmlhttp.open('GET',url_show+'/'+cv_id,true);
              xmlhttp.send();
              
        });


      //   $('[id=add_new]').click(function (e) {
      //      e.stopPropagation();
      //  $('[id=add_new]').addClass("currenting");
      //   return false;
      // });

  // Education 

         addnew = function(value,value2,value3,value4)
        {
          var url     = value ;  
              focus     = value2 ;                            
              // $('#add_new').fadeOut();
            $('#'+value3).fadeOut();
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById(value4).innerHTML = xmlhttp.responseText;
                  
                 $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
                 $('html,body').animate({
                  scrollTop: $("#"+focus).offset().top},
                  'slow');
              }
            }
              xmlhttp.open('GET',url,true);
              xmlhttp.send();
              $('#'+value4).fadeIn();
        }


      $('body').on('click','#education_submit',function(event)
        {   
          event.preventDefault();
          $("#edu_form").validate();
            if ($('#edu_form').valid()) 
            {                   
                var url_insert = $(this).attr('url');
                  cv_id = $("input#cv_id").val();
                  university = $("input#university").val();
                  grad_start_year = $("#grad_start_year option:selected").val();
                  grad_start_month = $("#grad_start_month option:selected").val();
                  grad_date = $("#grad_date option:selected").val();
                  grad_month = $("#grad_month option:selected").val();
                  degree_lvl = $("#degree_lvl option:selected").val();
                otherdegree_lvl = $("input#otherdegree_lvl").val();
                  subject_m = $("input#subject_m").val();
                  country = $("#country_edu option:selected").val();
                  url_show = $("input#edu_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_insert,
                      dataType: 'json',
                      data: {cv_id:cv_id,university:university,otherdegree_lvl:otherdegree_lvl,subject_m:subject_m,country:country,grad_start_year:grad_start_year,grad_start_month:grad_start_month,grad_date:grad_date,grad_month:grad_month,degree_lvl:degree_lvl},
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#education_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('edu_show').innerHTML = xmlhttp.responseText;
                               $('#edu_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
       
                          }
                        }
                  });                                
            }                  
        });

        $('body').on('click','#edit_edu',function() 
        {
           var id = $(this).attr('name');
            defaultdegree = $(this).attr('defaultdegree');
               url     = $("input#edit_edu_url").val();
                    $('html,body').animate({
                    scrollTop: $("#edu_section").offset().top},
                    'slow');
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('education_form_con').innerHTML = xmlhttp.responseText;
                 $('#edu_show_wrapper').remove();
                  $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
                  $("#degree_lvl").val(defaultdegree).trigger("chosen:updated");
                  var present = $('#degree_lvl option:selected').val();

                  if (present=='30')
                  {

                      $('[id=otheruni]').fadeIn();
                  }
                  else
                  {
                      $('[id=otheruni]').fadeOut();
                  }
              }
            }
              xmlhttp.open('GET',url+'/'+id,true);
              xmlhttp.send();             
              $('#education_form_con').fadeIn();
        });


      $('body').on('click','#edit_edu_submit',function(event)
        {   
          event.preventDefault();
          $("#edu_form").validate();
            if ($('#edu_form').valid()) 
            {                   
              var url_edit = $(this).attr('url');
                  t_id = $("input#edu_t_id").val();
                  cv_id = $("input#cv_id").val();
                  university = $("input#university").val();
                  grad_start_year = $("#grad_start_year option:selected").val();
                  grad_start_month = $("#grad_start_month option:selected").val();
                  grad_date = $("#grad_date option:selected").val();
                  grad_month = $("#grad_month option:selected").val();
                  degree_lvl = $("#degree_lvl option:selected").val();
                  subject_m = $("input#subject_m").val();
                  country = $("#country_edu option:selected").val();
                  url_show = $("input#edu_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_edit,
                      dataType: 'json',
                      data: {t_id:t_id,university:university,subject_m:subject_m,country:country,grad_start_year:grad_start_year,grad_start_month:grad_start_month,grad_date:grad_date,grad_month:grad_month,degree_lvl:degree_lvl},
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#education_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('edu_show').innerHTML = xmlhttp.responseText;
                              $('html,body').animate({
                              scrollTop: $("#edu-"+t_id).parent(".cv-section").offset().top},
                              'slow');
                              $('#edu_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
                        
       
                          }
                        }
                    });                                
                }                  
          });

// Certificate

  $('body').on('click','#certi_submit',function(event)
        {   
          event.preventDefault();
          $("#certi_form").validate();
            if ($('#certi_form').valid()) 
            {                   
              var url_insert = $(this).attr('url');
                  cv_id = $("input#cv_id").val();
                  certi = $("input#certi").val();
                  certi_year = $("#certi_year option:selected").val();
                  certi_month = $("#certi_month option:selected").val();
                  certi_country = $("#certi_country option:selected").val();
                  t_center = $("input#t_center").val();
                  url_show = $("input#certi_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_insert,
                      dataType: 'json',
                      data: {cv_id:cv_id,certi:certi,certi_year:certi_year,certi_month:certi_month,certi_country:certi_country,t_center:t_center},
                      success: function(data) 
                        {
                          if (data)
                         {                            
                            $('#certi_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('certi_show').innerHTML = xmlhttp.responseText;
                               $('#certi_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
       
                          }
                        }
                  });
            }                  
        });

        $('body').on('click','#edit_certi',function() 
        {
           var id = $(this).attr('name');
               url     = $("input#edit_certi_url").val();
                    $('html,body').animate({
                    scrollTop: $("#certi_section").offset().top},
                    'slow');
          var xmlhttp= new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('certi_form_con').innerHTML = xmlhttp.responseText;
                 $('#certi_show_wrapper').remove();
                  $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
              }
            }
              xmlhttp.open('GET',url+'/'+id,true);
              xmlhttp.send();             
              $('#certi_form_con').fadeIn();
        });

      $('body').on('click','#certi_edit_submit',function(event)
        {   
          event.preventDefault();
          $("#certi_form").validate();
            if ($('#certi_form').valid()) 
            {                   
              var url_edit = $(this).attr('url');
                  t_id = $("input#it_t_id").val();
                  cv_id = $("input#cv_id").val();
                  certi = $("input#certi").val();
                  certi_year = $("#certi_year option:selected").val();
                  certi_month = $("#certi_month option:selected").val();
                certi_country = $("#certi_country option:selected").val();
                  t_center = $("input#t_center").val();
                  url_show = $("input#certi_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                 
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_edit,
                      dataType: 'json',
                      data: {t_id:t_id,certi:certi,certi_country:certi_country,certi_year:certi_year,certi_month:certi_month,t_center:t_center},
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#certi_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('certi_show').innerHTML = xmlhttp.responseText;
                              $('html,body').animate({
                              scrollTop: $("#certi-"+t_id).parent(".cv-section").offset().top},
                              'slow');
                              $('#certi_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();                       
       
                          }
                        }
                    });                                
                }                  
          });


        // IT SKILL 

      $('body').on('click','#it_submit',function(event)
        {   
          event.preventDefault();
          $("#it_form").validate();
            if ($('#it_form').valid()) 
            {                   
              var url_insert = $(this).attr('url');
                  cv_id = $("input#cv_id").val();
                  topic_app = $("input#topic_app").val();
                  it_type = $("#it_type option:selected").val();
                  it_skill_lvl = $("#it_skill_lvl option:selected").val();
                  url_show = $("input#it_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_insert,
                      dataType: 'json',
                      data: {cv_id:cv_id,topic_app:topic_app,it_type:it_type,it_skill_lvl:it_skill_lvl},
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#it_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('it_show').innerHTML = xmlhttp.responseText;
                               $('#it_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
       
                          }
                        }
                  });                                
            }                  
        });

        $('body').on('click','#edit_itskill',function() 
        {
           var id = $(this).attr('name');
               url     = $("input#edit_it_url").val();
                    $('html,body').animate({
                    scrollTop: $("#it_section").offset().top},
                    'slow');
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('it_form_con').innerHTML = xmlhttp.responseText;
                 $('#it_show_wrapper').remove();
                  $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
              }
            }
              xmlhttp.open('GET',url+'/'+id,true);
              xmlhttp.send();             
              $('#it_form_con').fadeIn();
        });



      $('body').on('click','#it_edit_submit',function(event)
        {   
          event.preventDefault();
          $("#it_form").validate();
            if ($('#it_form').valid()) 
            {                   
              var url_edit = $(this).attr('url');
                  t_id = $("input#it_t_id").val();
                  cv_id = $("input#cv_id").val();
                  topic_app = $("input#topic_app").val();
                  it_type = $("#it_type option:selected").val();
                  it_skill_lvl = $("#it_skill_lvl option:selected").val();
                  url_show = $("input#it_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_edit,
                      dataType: 'json',
                      data: {t_id:t_id,topic_app:topic_app,it_type:it_type,it_skill_lvl:it_skill_lvl},
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#it_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('it_show').innerHTML = xmlhttp.responseText;
                              $('html,body').animate({
                              scrollTop: $("#it-"+t_id).parent(".cv-section").offset().top},
                              'slow');
                              $('#it_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
                        
       
                          }
                        }
                    });                                
                }                  
          });

  // Language 

      $('body').on('click','#lang_submit',function(event)
        {   
          event.preventDefault();
          $("#lang_form").validate();
            if ($('#lang_form').valid()) 
            {                   
              var url_insert = $(this).attr('url');
                  cv_id = $("input#cv_id").val();
                  lang = $("#lang option:selected").val();
                  lang_skill = $("#lang_skill option:selected").val();
                  url_show = $("input#lang_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_insert,
                      dataType: 'json',
                      data: {cv_id:cv_id,lang:lang,lang_skill:lang_skill},
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#lang_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('lang_show').innerHTML = xmlhttp.responseText;
                               $('#lang_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
       
                          }
                        }
                  });                                
            }                  
        });

   $('body').on('click','#edit_langskill',function() 
        {
           var id = $(this).attr('name');
               url     = $("input#edit_lang_url").val();
                    $('html,body').animate({
                    scrollTop: $("#lang_section").offset().top},
                    'slow');
          var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function() 
            {
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
              {
                document.getElementById('lang_form_con').innerHTML = xmlhttp.responseText;
                 $('#lang_show_wrapper').remove();
                  $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
              }
            }
              xmlhttp.open('GET',url+'/'+id,true);
              xmlhttp.send();             
              $('#lang_form_con').fadeIn();
        });



      $('body').on('click','#lang_edit_submit',function(event)
        {   
          event.preventDefault();
          $("#lang_form").validate();
            if ($('#lang_form').valid()) 
            {                   
              var url_edit = $(this).attr('url');
                  lang = $("#lang option:selected").val();
                  lang_skill = $("#lang_skill option:selected").val();
                  t_id = $("input#lang_id").val();
                  url_show = $("input#lang_url").val();
                $(this).val('Please wait..');
                $(this).prop('disabled', 'disabled');
                  jQuery.ajax
                  ({
                      type: "POST",
                      url: url_edit,
                      dataType: 'json',
                      data: {t_id:t_id,lang:lang,lang_skill:lang_skill},  
                      success: function(data) 
                        {
                          if (data)
                         {
                            
                            $('#lang_form_con').fadeOut();                        
                            xmlhttp= new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
                            {
                              document.getElementById('lang_show').innerHTML = xmlhttp.responseText;
                              $('html,body').animate({
                              scrollTop: $("#lang-"+t_id).parent(".cv-section").offset().top},
                              'slow');
                              $('#lang_show').fadeIn();
                            }
                            }
                            xmlhttp.open('GET',url_show+'/'+cv_id,true);
                            xmlhttp.send();
                        
       
                          }
                        }
                    });                                
                }                  
          });

     // REF PERSON


     $('body').on('click','#ref_submit',function(event)
     {
         event.preventDefault();
         $("#refperson_form").validate();
         if ($('#refperson_form').valid())
         {
             var url_insert = $(this).attr('url');
             cv_id = $("input#cv_id").val();
             ref_name = $("input#ref_name").val();
             ref_occupation = $("input#ref_occupation").val();
             ref_employer = $("input#ref_employer").val();
             ref_ph_no = $("input#ref_ph_no").val();
             ref_email = $("input#ref_email").val();
                url_show = $("input#ref_url").val();
             $(this).val('Please wait..');
             $(this).prop('disabled', 'disabled');
             jQuery.ajax
             ({
                 type: "POST",
                 url: url_insert,
                 dataType: 'json',
                 data: {cv_id:cv_id,ref_name:ref_name,ref_occupation:ref_occupation,ref_employer:ref_employer,ref_ph_no:ref_ph_no,ref_email:ref_email},
                 success: function(data)
                 {
                     if (data)
                     {

                         $('#refperson_form_con').fadeOut();
                         xmlhttp= new XMLHttpRequest();
                         xmlhttp.onreadystatechange = function(){
                             if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                             {
                                 document.getElementById('refperson_show').innerHTML = xmlhttp.responseText;
                                 $('#refperson_show').fadeIn();
                             }
                         }
                         xmlhttp.open('GET',url_show+'/'+cv_id,true);
                         xmlhttp.send();

                     }
                 }
             });
         }
     });


     $('body').on('click','#edit_ref',function()
     {
         var id = $(this).attr('name');
         url     = $("input#edit_ref_url").val();
         $('html,body').animate({
                 scrollTop: $("#refperson_section").offset().top},
             'slow');
         var xmlhttp= new XMLHttpRequest();

         xmlhttp.onreadystatechange = function()
         {
             if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
             {
                 document.getElementById('refperson_form_con').innerHTML = xmlhttp.responseText;
                 $('#refperson_show_wrapper').remove();
                 $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
             }
         }
         xmlhttp.open('GET',url+'/'+id,true);
         xmlhttp.send();
         $('#refperson_form_con').fadeIn();
     });


     $('body').on('click','#ref_edit_submit',function(event)
     {
         event.preventDefault();
         $("#refperson_form").validate();
         if ($('#refperson_form').valid())
         {
             var url_edit = $(this).attr('url');
             cv_id = $("input#cv_id").val();
             ref_name = $("input#ref_name").val();
             ref_occupation = $("input#ref_occupation").val();
             ref_employer = $("input#ref_employer").val();
             ref_ph_no = $("input#ref_ph_no").val();
             ref_email = $("input#ref_email").val();
             url_show = $("input#ref_url").val();
             t_id = $("input#ref_id").val();

             $(this).val('Please wait..');
             $(this).prop('disabled', 'disabled');
             jQuery.ajax
             ({
                 type: "POST",
                 url: url_edit,
                 dataType: 'json',
                 data: {t_id:t_id,ref_name:ref_name,ref_occupation:ref_occupation,ref_employer:ref_employer,ref_ph_no:ref_ph_no,ref_email:ref_email},
                 success: function(data)
                 {
                     if (data)
                     {
                         $('#refperson_form_con').fadeOut();
                         xmlhttp= new XMLHttpRequest();
                         xmlhttp.onreadystatechange = function(){
                             if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                             {
                                 document.getElementById('refperson_show').innerHTML = xmlhttp.responseText;
                                 $('#refperson_show').fadeIn();
                             }
                         }
                         xmlhttp.open('GET',url_show+'/'+cv_id,true);
                         xmlhttp.send();

                     }
                 }
             });
         }
     });

// Apply Position

     $('body').on('click','#post_submit',function(event)
     {
         event.preventDefault();
         $("#post_form").validate();
         if ($('#post_form').valid())
         {

             var url_insert = $(this).attr('url');
             cv_id = $("input#cv_id").val();
             app_job = $("#app_job option:selected").val();
             app_industry = $("#app_industry option:selected").val();
             job_type = $("#job_type option:selected").val();
             apply_post = $("input#apply_post").val();
             url_show = $("input#post_url").val();
             $(this).val('Please wait..');
             $(this).prop('disabled', 'disabled');
             jQuery.ajax
             ({
                 type: "POST",
                 url: url_insert,
                 dataType: 'json',
                 data: {cv_id:cv_id,app_industry:app_industry,app_job:app_job,job_type:job_type,apply_post:apply_post},
                 success: function(data)
                 {
                     if (data)
                     {

                         $('#post_form_con').fadeOut();
                         xmlhttp= new XMLHttpRequest();
                         xmlhttp.onreadystatechange = function(){
                             if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                             {
                                 document.getElementById('post_show').innerHTML = xmlhttp.responseText;
                                 $('#post_show').fadeIn();
                             }
                         }
                         xmlhttp.open('GET',url_show+'/'+cv_id,true);
                         xmlhttp.send();

                     }
                 }
             });
         }
     });


     $('body').on('click','#edit_post',function()
     {
         var id = $(this).attr('name');
         url     = $("input#edit_post_url").val();
         $('html,body').animate({
                 scrollTop: $("#post_section").offset().top},
             'slow');
         var xmlhttp= new XMLHttpRequest();

         xmlhttp.onreadystatechange = function()
         {
             if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
             {
                 document.getElementById('post_form_con').innerHTML = xmlhttp.responseText;
                 $('#post_show_wrapper').remove();
                 $('.chosen-select').chosen();
                 $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
             }
         }
         xmlhttp.open('GET',url+'/'+id,true);
         xmlhttp.send();
         $('#post_form_con').fadeIn();
     });

     $('body').on('click','#post_edit_submit',function(event)
     {
         event.preventDefault();
         $("#post_form").validate();
         if ($('#post_form').valid())
         {
             var url_edit = $(this).attr('url');
             app_job = $("#app_job option:selected").val();
             cv_id = $("input#cv_id").val();
             app_industry = $("#app_industry option:selected").val();
             job_type = $("#job_type option:selected").val();
             apply_post = $("input#apply_post").val();
             t_id = $("input#post_id").val();
             url_show = $("input#post_url").val();
             $(this).val('Please wait..');
             $(this).prop('disabled', 'disabled');
             jQuery.ajax
             ({
                 type: "POST",
                 url: url_edit,
                 dataType: 'json',
                 data: {t_id:t_id,app_industry:app_industry,app_job:app_job,job_type:job_type,apply_post:apply_post},
                 success: function(data)
                 {
                     if (data)
                     {
                         console.log(job_type);
                         $('#post_form_con').fadeOut();
                         xmlhttp= new XMLHttpRequest();
                         xmlhttp.onreadystatechange = function(){
                             if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                             {
                                 document.getElementById('post_show').innerHTML = xmlhttp.responseText;
                                 $('html,body').animate({
                                         scrollTop: $("#post-"+t_id).parent(".cv-section").offset().top},
                                     'slow');
                                 $('#post_show').fadeIn();
                             }
                         }
                         xmlhttp.open('GET',url_show+'/'+cv_id,true);
                         xmlhttp.send();


                     }
                 }
             });
         }
     });



     /// Dynamically Fucntion

         cancel_btn = function(section,url,container,show)
         {
          $('html,body').animate({
          scrollTop: $("#"+section).offset().top},
          'slow');
          url_show = url;

             cv_id = $("input#cv_id").val();
              xmlhttp= new XMLHttpRequest();
              xmlhttp.onreadystatechange = function(){
              if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                 {
                 document.getElementById(show).innerHTML = xmlhttp.responseText;
                 $('#'+show).fadeIn();
                     $('#'+container).fadeOut();
                 }
                }
              xmlhttp.open('GET',url_show+'/'+cv_id,true);
              xmlhttp.send();
        }

      delete_dynamic = function(t_id,del,show,table_name,focus_id)
      {
          var del_url = del;
              show_url = show;
              cv_id = $("input#cv_id").val();
              $("#delete").attr("tid", t_id);
              $("#delete").attr("del", del_url);
              $("#delete").attr("show", show_url);
              // $("#delete").attr("section", section);
              $("#delete").attr("tname", table_name);
              $("#delete").attr("fid", focus_id); 
          return false;
      }


              $('body').on('click','button[id="delete"]',function(event)
          {                // $('#loading').show();                 
                event.preventDefault(); 
                var id = $(this).attr("tid"); 
                    del_url = $(this).attr("del"); 
                    show_url = $(this).attr("show");
                    table_name = $(this).attr("tname");
                     focus_id = $(this).attr("fid");               
                 var x =new Object();                             
                x = {
                    'id':id,
                    'table_name':table_name,
                    };                                
                $.ajax({
                    url: del_url,
                    cache:false,
                    type: 'POST',
                    data: x,
                    success: function(data) 
                    {         
                        $('html,body').animate({
                        scrollTop: $(focus_id).parent(".cv-section").offset().top},
                        'slow');
                        $(focus_id).parent(".cv-section").fadeOut('slow');
                         return false;
                    },              
                });
              return false;
           });
// Upload Image

     //
     //$('body').on('click','button[id="profileUpload"]',function()
     //{                // $('#loading').show();
     //
     //    var cv_id = $("input#cv_id").val();
     //       img_valz = $("input#avatarInput").val();
     //       url = $("input#profileUploadUrl").val();
     //    console.log(url);
     //    var x =new Object();
     //    x = {
     //        'cv_id':cv_id,
     //        'img_val':img_valz,
     //        };
     //    $.ajax({
     //        url: url,
     //        cache:false,
     //        type: 'POST',
     //        data: x,
     //        success: function(data)
     //        {
     //
     //        },
     //    });
     //
     //});




  // $('#removebtn').on('click', function(event) {
  // event.preventDefault();
  //   if (confirm('Are you sure you want to delete this file?'))
  //   {

  //      var delimgurl = $('#delimgurl').val(); 
  //          userfile =$('#userfile').val();
           
  //     $.ajax({
  //       url     : delimgurl,
  //       type: 'POST',
  //       data: {userfile:userfile},
  //       success: function(data)
  //       {
         
  //         $(".preview").find('#removebtn').fadeOut();
  //         $('#preview').attr('src','#');

          
  //       }
         
  //     });
     
        
  //   }
  // });


     readURL = function (input) {
         if (input.files && input.files[0])
         {
             var reader = new FileReader();

             reader.onload = function (e)
             {
                 $('#cv_preview')
                     .attr('src', e.target.result)
                     .width(175)
                     .height(130);
             };

             reader.readAsDataURL(input.files[0]);
         }
     }

      imguploaddy = function (input,id)
  {

    if (input.files && input.files[0])
    {
        var reader = new FileReader();

        reader.onload = function (e)
        {
            $('.preview'+id).find('#preview'+id)
                .attr('src', e.target.result)
                .width(175)
                .height(130);
        };

        reader.readAsDataURL(input.files[0]);

        $('#preview'+id).fadeIn();
        $('.preview'+id).find('.fileUpload').fadeOut();

    }
  }


  var max_fields      = 9; //maximum input boxes allowed
    var wrapper         = $("#img_field_wrap"); //Fields wrapper
    var add_button      = $("#add_new_img"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(event){ //on add input button click
        event.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment

            $(wrapper).append('<div style="min-height: 360px;"  class="preview'+x+' col-md-3 countdiv" > <input name="attachName[]" type="text" placeholder="Name" class="form-control" style="margin-bottom: 5px; required"><img id="preview'+x+'" src="#" alt="Images Preview" class="img-responsive" /><div class="fileUpload btn btn-info"><span>Upload</span><input type="file" name="userfile[]" class="upload" id="userfile" onchange="imguploaddy(this,'+x+');" multiple/></div><input type="submit" id="removebtn" value="Remove" class="btn btn-danger" ></div>');

            //add input box
        }
var conveniancecount = $("div[class*='countdiv']").length;
    if (conveniancecount == 8 ) {
        $("#add_new_img").fadeOut();

    }
    else{
      $("#add_new_img").fadeIn();
    }
    

    });
   
    $(wrapper).on("click","#removebtn", function(e){ //user click on remove text
        e.preventDefault(); 

        var url = $(this).attr('url'); 
           t_id =$(this).attr('tid');
            imgname =$(this).attr('imgname');
        var r=confirm("Are you sure want to deleted")
        if(r==true) {
            $(this).parent('div').remove();
            x--;
            $.ajax({
                url: url,
                type: 'POST',
                data: {t_id: t_id, imgname: imgname},
                success: function (data) {


                }


            });
        }
    var conveniancecount = $("div[class*='countdiv']").length;
    if (conveniancecount == 8 ) {
        $("#add_new_img").fadeOut();

    }
    else{
      $("#add_new_img").fadeIn();
    }


    });



    var conveniancecount = $("div[class*='countdiv']").length;
    if (conveniancecount == 4 ) {
        $("#add_new_img").fadeOut();

    }
    else{
      $("#add_new_img").fadeIn();
    }



       

});



